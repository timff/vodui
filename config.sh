#!/bin/bash
SSM_READ='../aws/scripts/ssm-get'
AUTH0_CLIENTID=`${SSM_READ} /RfsVod/Auth0/ClientId`
AUTH0_AUDIENCE=`${SSM_READ} /RfsVod/Auth0/Audience`
AUTH0_REDIRECT=`${SSM_READ} /RfsVod/Auth0/Redirect`
AUTH0_DOMAIN=`${SSM_READ} /RfsVod/Auth0/Domain`
API_ENDPOINT=`${SSM_READ} /RfsVod/ApiEndpoint`

echo "$API_ENDPOINT $AUTH0_DOMAIN $AUTH0_CLIENTID"

sed -e "s|AUTH0_CLIENTID|$AUTH0_CLIENTID|g" \
-e "s|AUTH0_DOMAIN|$AUTH0_DOMAIN|g" \
-e "s|AUTH0_AUDIENCE|$AUTH0_AUDIENCE|g" \
-e "s|AUTH0_REDIRECT|$AUTH0_REDIRECT|g" \
-e "s|API_ENDPOINT|$API_ENDPOINT|g" \
src/config/app-setting.template > src/config/app-setting.vod.ts

