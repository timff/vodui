BUCKET ?= nswrfs-artifacts
VERSION ?= 1.0.0
ATF_BUCKET ?= ${BUCKET}/rfsvod

ENVIRONMENT ?= dev
DECOMMISSION ?= False
CFN_SYNC ?= True
DEPLOY = "../aws/scripts/cfn-deploy"
INVALIDATE = ../aws/scripts/cft-invalidate


.ONESHELL:
build:
	./config.sh
	npm install >/dev/null 2>&1
	ng build --prod --no-progress
	cd dist
	zip -r ../rfsvod-ui-${VERSION}.zip *
	cd ..
	aws s3 cp rfsvod-ui-${VERSION}.zip s3://${ATF_BUCKET}/

deploy-local dl: 
	aws s3 sync dist s3://rfsvod-ui-${ENVIRONMENT}
	${INVALIDATE} -a vod

update-ui:
	aws s3 cp s3://rfsvod-ui-${ENVIRONMENT}/release-${VERSION}.zip
	unzip release-${VERSION}.zip
	aws s3 sync dist s3://rfsvod-ui-${ENVIRONMENT}
	${INVALIDATE} -a vod	

clean:
	@-rm -rf dist node_modules release.zip

deploy-ui-infrastructure:
	aws s3 cp RfsVodUiStack.yaml s3://nswrfs-artifacts/rfsvod/ --acl authenticated-read
	${DEPLOY} --stack-name rfsvod-ui \
		--template-url https://s3.amazonaws.com/${ATF_BUCKET}/RfsVodUiStack.yaml \
		--decommission ${DECOMMISSION}

.PONY: webapp clean deploy

all: webapp