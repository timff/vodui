export const setting = {
  production: true,
  logoutRedirect: "https://vod.dev.apps.rfs.nsw.gov.au",
  apiEndPoint: {
        api: 'https://vod.dev.apps.rfs.nsw.gov.au/v1/'
  },
  auth: {
    clientID: 'fsE175TioeJWCFkqxpcTQmaGcw4s06W5',
    domain: 'nswrfs.auth0.com',
    audience: 'https://nswrfs.auth0.com/api/v2/',
    redirect: 'https://vod.dev.apps.rfs.nsw.gov.au',
    scope: ''
  }
};