
// for testing....

export const setting = {

    production: false,
    logoutRedirect: "http://localhost:4111/test",

    apiEndPoint:"https://vod.dev.apps.rfs.nsw.gov.au/v1/",

    auth: {
        clientID: 'D9QY5Y3LVpnGhjsVf2YCilHcApnDcjtT',
        domain: 'romabug331.au.auth0.com', // e.g., you.auth0.com
        // audience not a must parms
        audience: 'https://romabug331.au.auth0.com/api/v2/', // e.g., http://localhost:3001
        redirect: 'http://localhost:4111/callback',
        scope: 'openid profile email'
    }

};


