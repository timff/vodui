export const setting = {

    production: false,
    logoutRedirect: 'http://localhost:4111',


    // auth: {
    //   clientID: 'YOUR-AUTH0-CLIENT-ID',
    //   domain: 'YOUR-AUTH0-DOMAIN', // e.g., you.auth0.com
    //   audience: 'YOUR-AUTH0-API-IDENTIFIER', // e.g., http://localhost:3001
    //   redirect: 'http://localhost:4200/callback',
    //   scope: 'openid profile email'
    // }


    // auth: {
    //     clientID: 'D9QY5Y3LVpnGhjsVf2YCilHcApnDcjtT',
    //     domain: 'romabug331.au.auth0.com', // e.g., you.auth0.com
    //     // audience not a must parms
    //     audience: 'https://romabug331.au.auth0.com/api/v2/', // e.g., http://localhost:3001
    //     redirect: 'http://localhost:4111/callback',
    //     scope: 'openid profile email'
    // }


    auth: {
        clientID: 'fsE175TioeJWCFkqxpcTQmaGcw4s06W5',
        domain: 'nswrfs.auth0.com',
        audience: 'https://nswrfs.auth0.com/api/v2/',
        redirect: 'http://localhost:4111',
        scope: 'openid profile email'
    },

    apiEndPoint: {
        api: 'https://vod.dev.apps.rfs.nsw.gov.au/v1/'
    }


};


