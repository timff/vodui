import {InMemoryDbService} from 'angular-in-memory-web-api';

export class LocalDataService implements InMemoryDbService {
  createDb() {
    const video = [
            {
                "id": 101,
                "title": "first video ",
                "upload_date": "12/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "good"
            },
            {
                "id": 102,
                "title": "forest gum  ",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 103,
                "title": "titan video",
                "upload_date": "21/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "good"
            },
            {
                "id": 104,
                "title": "big bear video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 105,
                "title": "iron man video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "good"
            },
            {
                "id": 106,
                "title": "super man video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 107,
                "title": "the sun video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 108,
                "title": "six friends video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "good"
            },
            {
                "id": 116,
                "title": "my life come true video2",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 104,
                "title": "big bear video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 105,
                "title": "iron man video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "good"
            },
            {
                "id": 106,
                "title": "super man video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 107,
                "title": "the sun video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            },
            {
                "id": 108,
                "title": "six friends video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "good"
            },
            {
                "id": 116,
                "title": "my life come true video",
                "upload_date": "22/11/2018",
                "video_size": 12,
                "area_code": "1345",
                "status": "pass"
            }
        ]

    return {video};
  }


}


