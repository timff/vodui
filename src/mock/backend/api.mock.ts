import {BehaviorSubject} from 'rxjs';
import {BaseMockBackend} from './base.mock';

export class MockApiBackend extends BaseMockBackend {
  private static count = 0;

  protected load() {
    this.loadUserPayments();
    this.loadRoomPayments();
  }

  private loadUserPayments() {
    if (this.model.log) {
      this.model.log('MOCK', 'Loading users Payments and setting up request');
    }
    const Payments: any = {};
    const days = 30;

  }

  private loadRoomPayments() {
    if (this.model.log) {
      this.model.log('MOCK', 'Loading Payments for rooms');
    }
    if (this.model.rooms) {
      for (const room of this.model.rooms) {
        let Payments = [];
        // Create Payments for room
        const date = 1;
        const today = this.createPayments(date.valueOf());
        Payments = Payments.concat(today);
        for (let i = 0; i < 14; i++) {
          const more_Payments = this.createPayments(date.valueOf());
          Payments = Payments.concat(more_Payments);
        }
        Payments.forEach((b) => b.room_id = room.email);
        room.settings.Payments = Payments;
        if (!(window as any).control) {
          (window as any).control = {};
        }
        const control = (window as any).control;
        if (!control.systems) {
          control.systems = {};
        }
        if (!control.systems[room.id]) {
          control.systems[room.id] = {};
        }
        const system = control.systems[room.id];
        if (!system.Payments) {
          system.Payments = [{}];
        }
        system.Payments[0].today = today;
      }
    }
  }

  private createPayments(date: number) {
    const Payments = [];
    const bkn_cnt = Math.floor(Math.random() * 6 + 1);
    for (let i = 0; i < bkn_cnt; i++) {
      Payments.push({
        ConferenceRoomAlias: 'cfsydinx',
        Location: this.model.city,
        BookingUserAlias: null,
        StartTimeZoneName: null,
        EndTimeZoneName: null,

      });
    }

  }
}
