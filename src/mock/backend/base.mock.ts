export class BaseMockBackend {

  protected state_obs: any;

  constructor(protected model) {
    setTimeout(() => this.init(), 100);
  }

  protected init() {
    this.load();
  }

  protected load() {
  }

  get data() {
    return this.model;
  }

  protected request(method, url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open(method, url);
      xhr.onload = function () {
        if ((this as any).status >= 200 && (this as any).status < 300) {
          resolve(xhr.response);
        } else {
          reject({
            status: (this as any).status,
            statusText: xhr.statusText,
          });
        }
      };
      xhr.onerror = function () {
        reject({
          status: (this as any).status,
          statusText: xhr.statusText,
        });
      };
      xhr.send();
    });
  }
}
