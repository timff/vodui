import {BehaviorSubject} from 'rxjs';
import {BaseMockBackend} from './base.mock';


export class CardBackend extends BaseMockBackend {

  constructor(protected model) {
    super(model);
    //   MOCK_REQ_HANDLER.unregister('/control/api/users/current');
  }
}
