import {Component, HostListener, OnInit, Input, ElementRef, AfterViewInit, Inject, ViewChild} from '@angular/core';

import {LocalStorage} from '../lib/local.storage';
import {VideosService} from '../api/videos.service';
import {interval, timer} from 'rxjs';
import {VgDASH} from '../lib/hls/src/streaming/vg-dash/vg-dash';
import {VgHLS} from '../lib/hls/src/streaming/vg-hls/vg-hls';
import {VgAPI} from '../lib/hls/src/core/services/vg-api';
import {BitrateOption} from '../lib/hls/src/core/core';

import {Platform} from '@angular/cdk/platform';
import {Breakpoints, BreakpointObserver} from '@angular/cdk/layout';

import {setting} from '../../config/app-setting';


export interface GuidUrl {
    hlsUrl: string;
}

export interface IMediaStream {
    type: 'vod' | 'dash' | 'hls';
    source: string;
    label: string;
    token?: string;
}


@Component({
    selector: 'app-video-player',
    templateUrl: './video-player.component.html',
    styleUrls: ['./video-player.component.scss']
})
export class VideoPlayerComponent implements OnInit, AfterViewInit {


    @Input()
    public videoInfo;

    @HostListener('document:keydown', ['$event'])
    public keyboardCloseVideoPanel(event: KeyboardEvent): void {
        // console.log(event.key);
        event.stopPropagation();
        if (event.key === 'Escape') {
            if (this.isPopUp) {
                this.closeVideoArea();
            }
        }
    }


    private  urlConfig:any;
    constructor(
        private ls: LocalStorage,
        private video: VideosService,
        private el: ElementRef,
        @Inject('UrlConfig') private signUrl,
        public clientOS: Platform,
        private breakpointObserver: BreakpointObserver,

    ) {
        this.currentStream = this.tmpStream;

        this.urlConfig = setting.apiEndPoint;

    }


    public mobileView: boolean;

    updatePLayerPosition() {
        const isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
        this.breakpointObserver.observe([
            Breakpoints.HandsetPortrait
            // Breakpoints.HandsetLandscape,
        ]).subscribe(result => {

            if (result.matches) {
                this.mobileView = true;

            } else {
                this.mobileView = false;
            }
        });

    }



   public setScrollState(state:boolean) {

        this.mobileView = state;
    }




    // private lastScrollTop: number = 0;
    // private direction: string = '';
    // public scrollDown: boolean = false;
    // private scrollCheck() {
    //
    //     window.onscroll = () => {
    //
    //         let st: number = window.pageYOffset;
    //         let dir: string = '';
    //         if (st > this.lastScrollTop) {
    //             dir = 'down';
    //             if (st > 360)
    //                 this.scrollDown = true;
    //         } else {
    //             dir = 'up';
    //
    //             if (st < 220)
    //                 this.scrollDown = false;
    //         }
    //
    //         this.ngZone.runOutsideAngular(
    //             () => {
    //                 this.direction = dir;
    //                 this.lastScrollTop = st;
    //             }
    //         );
    //
    //
    //     };
    //
    // }







    /** ************************************************************** */


    @ViewChild(VgDASH) vgDash: VgDASH;
    @ViewChild(VgHLS) vgHls: VgHLS;

    currentStream: IMediaStream;
    api: VgAPI;

    bitrates: BitrateOption[];

    tmpStream: IMediaStream =
        {
            type: 'vod',
            label: 'rfs-video',
            source: ''
        };


    onPlayerReady(api: VgAPI) {
        this.api = api;
        console.log(api);

    }


    setBitrate(option: BitrateOption) {
        switch (this.currentStream.type) {
            case 'dash':
                this.vgDash.setBitrate(option);
                break;

            case 'hls':
                this.vgHls.setBitrate(option);
                break;
        }
    }


    autoStream() {

        const doTimer = interval(10);
        const tryTimer = doTimer.subscribe(() => {
            if (this.api.canPlay) {
                this.api.play();
                tryTimer.unsubscribe();
            }
        });

    }


    public leftTopClose() {
        console.log('iam closing lskldfjf');
        this.closeVideoArea();
    }


    launch() {
        this.api.pause();
        this.bitrates = null;

        let delaytime = timer(100)
            .subscribe(() => {
                // this.currentStream = stream;

                this.currentStream = {
                    type: 'vod',
                    label: 'VOD-hero',
                    source: 'https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8'
                };
                delaytime.unsubscribe();
                //   this.autoStream();
            });

    }


    startStreaming() {
        this.bitrates = null;
        this.api.play();
    }

    stopStreaming() {
        this.bitrates = null;
        this.api.pause();
    }


    onClickStream(stream: IMediaStream) {
        this.api.pause();
        this.bitrates = null;

        // this.currentStream = stream;
        let delaytime = timer(500)
            .subscribe(() => {
                this.currentStream = stream;
                delaytime.unsubscribe();

            });
    }


    /** ************************************************************** */


    ngOnInit() {
        this.ls.setObject('name', 'RFS RFS');
        this.updatePLayerPosition();

    }


    handleHtml() {

        let mymedia = this.el.nativeElement.querySelector('video');
        let currentTime = this.el.nativeElement.querySelector('video').currentTime;
        let readyState = this.el.nativeElement.querySelector('video').readyState;
        console.log(currentTime);
        console.log(readyState);
        mymedia.pause();

    }


    public closeVideoArea() {

        if (this.api) {
            this.api.pause();
            this.bitrates = null;
            this.currentStream.source = '';
        }


        this.isPopUp = false;
    }


    public isPopUp: boolean = false;

    public popUp() {
        // if(this.isPopUp) {
        //     this.handleHtml();
        // }

        this.isPopUp = this.isPopUp ? false : true;
    }

    public tmpGUID: string;
    public videoURL: GuidUrl;
    public posterURL: string;


    get clientSide(): boolean {

        if (this.clientOS.ANDROID || this.clientOS.IOS)
            return true;
        else
            return false;
    }


    public systemTest: string;

    public initVideo(video: any) {

        let ApiUrl = this.urlConfig.api + 'signvideo/';

        if (!this.clientSide) {
            ApiUrl = this.urlConfig.api + 'signhdvideo/';
        }


        if (video.guid) {
            this.videoURL = {hlsUrl: 'GuidNotValid'};
            this.posterURL = video.snapshotUrl;

            this.systemTest = ApiUrl;

            this.video.videoGetUrl(ApiUrl + video.guid).subscribe(
                hlsUrl => {
                    this.videoURL = hlsUrl;
                    this.tmpGUID = video.guid;

                    this.currentStream.source = this.videoURL.hlsUrl;

                    // this.api.play();

                    this.autoStream();

                    console.log(hlsUrl);

                },
                errormsg => {
                    console.log('initVideo -- error-->');
                    console.log(errormsg);
                    //https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8
                    this.videoURL.hlsUrl = 'https://d2zihajmogu5jn.cloudfront.net/bipbop-advanced/bipbop_16x9_variant.m3u8';
                    this.currentStream.source = this.videoURL.hlsUrl;

                    this.autoStream();

                }
            );
        }
    }


    private posterImage() {
        if (this.posterURL) {
            return this.posterURL;
        }
    }

    private hlsUrl() {
        console.log(this.videoURL);
        if (this.videoURL) {
            return this.videoURL.hlsUrl;
        }
    }


    public posterUrl: string;

    public initHlsApi(api: VgAPI) {
        this.api = api;
        console.log(api);


        // this.api.getDefaultMedia().subscriptions.ended.subscribe(
        //     () => {
        //         // Set the video to the beginning
        //         this.api.getDefaultMedia().currentTime = 60;
        //     }
        // );

    }

    public runPause() {
    }

    ngAfterViewInit() {
    }


}
