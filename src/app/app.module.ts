// common ng Modules
import {enableProdMode, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
// service
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryDataService} from './api/local/local-data.service';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientXsrfModule} from '@angular/common/http';

import {TestData} from './comm/newcard/test-data';
// import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';


// components
import {AppComponent} from './app.component';
import {HeaderComponent} from './comm/header/header.component';
import {SearchComponent} from './search/search.component';

import {SearchResultComponent} from './list/search-result/search-result.component';
import {MaterialComponent} from './directive/material/material.component';
import {RegisterComponent} from './login/register/register.component';
import {LoginComponent} from './login/login/login.component';


// for home-nav
//import {HomeNavComponent} from './home-nav/home-nav.component';


// Modules
import {UiModule} from './lib/new-ui/ui.module';

// import CDKS from material
// import './polyfills';
import {LayoutModule} from '@angular/cdk/layout';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {TableComponent} from './list/table/table.component';

import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {OverlayModule} from '@angular/cdk/overlay';


// directives
//import {ClickStyleDirective} from './directive/click-style.directive';

import {FilterDirective} from './directive/filter.directive';
import {ScrollTrackerDirective} from './directive/scroll-tracker.directive';

// components
// import {MenuComponent} from './home-nav/menu/menu.component';
import {IconListComponent} from './comm/icon-list/icon-list.component';
 import {NewcardComponent} from './comm/newcard/newcard.component';
import {ItemFilterComponent} from './comm/item-filter/item-filter.component';
import {DyncFilterComponent} from './comm/dync-filter/dync-filter.component';
import {FilterUnitComponent} from './comm/dync-filter/filter-unit/filter-unit.component';
import {FilterElementComponent} from './comm/dync-filter/filter-element/filter-element.component';
import {VideoPlayerComponent} from './video-player/video-player.component';
import {SearchBarComponent} from './comm/header/search-bar/search-bar.component';
import {CallbackComponent} from './auth/callback/callback.component';
import {PreviewComponent} from './comm/newcard/preview/preview.component';


//////////////////////////////////////////////////////////////////////////
import {VgCoreModule} from './lib/hls/src/core/core';
import {VgControlsModule} from './lib/hls/src/controls/controls';
import {VgOverlayPlayModule} from './lib/hls/src/overlay-play/overlay-play';
import {VgBufferingModule} from './lib/hls/src/buffering/buffering';
import {VgStreamingModule} from './lib/hls/src/streaming/streaming';


//lazy load
//import {LoadImageDirective} from './directive/load-image.directive';


//http error handler
import {HttpErrorHandler} from './api/http-error-handler.service';
import {MessageService} from './api/message.service';
import {environment} from '../environments/environment';
import {MonthPipe} from './pipe/month.pipe';


// import { LocationStrategy, HashLocationStrategy } from '@angular/common';


if (environment.production) {
    enableProdMode();
    if (window) {
        window.console.log = function () {
        };
        window.console.warn = function () {
        };
        //   window.location.href = "http://localhost:4111";
    }
}


@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        SearchComponent,
        SearchResultComponent,
        MaterialComponent,
        RegisterComponent,
        LoginComponent,
        //  HomeNavComponent,
        NewcardComponent,
        TableComponent,
        //   MenuComponent,
        IconListComponent,

        ItemFilterComponent,
        DyncFilterComponent,
        FilterUnitComponent,
        FilterElementComponent,
        // ClickStyleDirective,
        FilterDirective,
        ScrollTrackerDirective,
        VideoPlayerComponent,
        SearchBarComponent,
        CallbackComponent,
        PreviewComponent,
        MonthPipe


    ],
    imports: [
        // common ng modules
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,

        // service
        HttpClientXsrfModule.withOptions({
            cookieName: 'My-Xsrf-Cookie',
            headerName: 'My-Xsrf-Header',
        }),
        // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
        // and returns simulated server responses.
        // Remove it when a real server is ready to receive requests.
        HttpClientInMemoryWebApiModule.forRoot(
            InMemoryDataService, {
                dataEncapsulation: false,
                passThruUnknownUrl: true,
                put204: false // return entity after PUT/update
            }
        ),


        // ng sub module
        UiModule,

        // cdk
        LayoutModule,
        CdkTableModule,
        CdkTreeModule,
        ScrollDispatchModule,
        OverlayModule,

        // gular2
        VgCoreModule,
        VgControlsModule,
        VgOverlayPlayModule,
        VgBufferingModule,
        VgStreamingModule,


    ],
    providers: [
        HttpErrorHandler, MessageService,

        {
            provide: TestData,
            useClass: TestData,
        },
        {
            provide: 'UrlConfig', useValue: {
                mock_url: 'http://localhost:3000',
                mock_url2: 'http://localhost:3001',
                swagger_url: 'https://virtserver.swaggerhub.com/datateam1/VideoPlayback/1.0.0/filename?searchString=Widget',
                //  video_url: 'https://3wddwfe1gh.execute-api.ap-southeast-2.amazonaws.com/dev/year/2018',
                json_tmp_url: 'https://jsonplaceholder.typicode.com',
                api: 'https://vod.dev.apps.rfs.nsw.gov.au/v1/'
            }
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
