import {Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {timer} from 'rxjs/internal/observable/timer';
import {interval} from 'rxjs/internal/observable/interval';
import {VideosService} from '../../../api/videos.service';
import {Platform} from '@angular/cdk/platform';
import {Observable} from 'rxjs/internal/Observable';
import {Subscription} from 'rxjs/internal/Subscription';


@Component({
    selector: 'app-preview',
    templateUrl: './preview.component.html',
    styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

    constructor(private frameSource: VideosService,
                @Inject('UrlConfig') private URL,
                public clientSide: Platform
    ) {
        this.startAnim = false;
    }

    ngOnInit() {

    }



    @Input()
    videoGuid: string;

    @Output()
    mouseOut: EventEmitter<boolean> = new EventEmitter();

    @Output()
    loadAnimation: EventEmitter<any> = new EventEmitter();


    showguid() {
        console.log(this.videoGuid);
    }


    public makeImages(num: number) {
        for (let i: number = 0; i < num; i++) {
            this.imageSource[i] = `https://picsum.photos/800/300?image=${i}`;
        }
    }


    public imageSource: string[];
    public imageSourceRam: string[];
    public imageFrame: string;
    public isshow: boolean = false;


    clearPreview() {
        //  console.log('timer clear');
        this.mouseOver = false;
        this.startAnim = false;

        this.delaytime.unsubscribe();

        if (this.playPreview) {
            this.playPreview.unsubscribe();
        }

    }


    private loadALLFrames: number = 0;
    private playedState: boolean = false;

    afterload(i: number) {

        this.loadALLFrames++;


        if (this.imageSource.length > 3) {


            // in case browser does't load  1 or 2 frames, it still play
            if (this.imageSource.length - 1 == this.loadALLFrames) {
                //    console.warn('loading finished...');
                this.playedState = true;

                if (this.mouseOver) {
                    this.preview();
                    // for any case, if 1 or 2 doesn't load, then it won't launch the animation.
                    //playedState  set once here ...


                }

            }
        }
        //  console.log('got ...' + i + ' of ' + this.imageSource.length);

    }


    private delaytime: Subscription;
    public mouseOver: boolean = false;

    initPreview() {
        this.mouseOver = true;
        this.loadALLFrames = 0;

        if (this.playedState) {
            this.preview();
        }
        else {

            if (this.clientSide.ANDROID || this.clientSide.IOS) {
                this.startAnim = false;

            }
            else {
                if (this.videoGuid) {
                    this.delaytime = timer(50)
                        .subscribe(() => {
                            // this.currentStream = stream;
                            //    console.log('preview ->' + this.videoGuid);
                            this.initAnimFrames();

                            //    this.preview();
                            this.delaytime.unsubscribe();
                        });
                } else {
                    //         console.error('not valid videoGuid...');
                }
            }
        }

    }


    public framesURL: string[];

    private initAnimFrames() {

        if (this.mouseOver) {
            //     console.warn(this.videoGuid);

            let ApiUrl: string = this.URL.api + 'preview/';

            if (this.videoGuid) {

                this.frameSource.getPreview(  this.videoGuid).subscribe(
                    val => {
                        this.imageSource = val.previews;
                        //       console.warn('imageSource refresh...playedState->' + this.playedState);

                    },
                    msg => {
                        //          console.log('initAnimFrames -- error-->');

                    }
                );
            }
        }
    }


    public startAnim: boolean;
    private playPreview: Subscription;

    preview() {

        let atFrame: number = 0;
        const period: number = 200;
        let total: number = this.imageSource.length;
        this.startAnim = true;

        let myinterval = interval(period);

        this.playPreview = myinterval.subscribe(() => {
            // console.log("play" + frames);

            this.imageFrame = this.imageSource[atFrame];

            if (atFrame > total) {
                this.imageFrame = 'noFrames';
                this.startAnim = false;    //no shadow
                this.playPreview.unsubscribe();
            }

            if (this.startAnim == false) {
                this.imageFrame = 'noFrames';
                this.startAnim = false;
                this.playPreview.unsubscribe();
            }

            //      console.log('playing...' + atFrame);

            atFrame++;

        });
    }
}
