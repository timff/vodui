import {Component, EventEmitter, HostListener, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
// import {VideoItem} from '/item-filter/item-filter.component';
import {VideoItem} from '../../interface/videos';
import {interval, of, timer} from 'rxjs';
import {VideosService} from '../../api/videos.service';
import {FilterUnitObj} from '../../interface/filters';
import {TestData} from './test-data';
import {AuthJsService} from '../../api/auth/auth-js.service';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';

import {BreakpointObserver, Breakpoints, BreakpointState} from '@angular/cdk/layout';


const totalToShow: number = 28;
const totalToShowSearch: number = 28;
const numberToInsert: number = 10;


@Component({
    selector: 'app-newcard',
    templateUrl: './newcard.component.html',
    styleUrls: ['./newcard.component.scss'],
    providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]

})

export class NewcardComponent implements OnInit, OnChanges, OnDestroy {

    @Input()
    private videoInfo: VideoItem;

    @Output()
    public popVideoInfo: EventEmitter<any> = new EventEmitter();


    @Output()
    public filterUpdate: EventEmitter<any> = new EventEmitter();


    private sourceList: VideoItem[];
    private moreList: VideoItem[];


    public videoItemListRam: VideoItem[];
    public videoItemList: VideoItem[];


    constructor(
        private breakpointObserver: BreakpointObserver,
        private getToken: AuthJsService,
        private testData: TestData,
        private vs: VideosService,
        private location: Location) {
        this.sourceList = this.testData.sourceList;
        //   this.moreList = this.testData.moreList;

        this.viewporttest();
    }


    public twocolumn: boolean;
    public checkHandSet: boolean;

    private viewporttest() {

        this.breakpointObserver
            .observe(['(min-width: 1598px)'])
            .subscribe((state: BreakpointState) => {
                this.twocolumn = state.matches ? true : false;
            });


        this.breakpointObserver.observe([
            Breakpoints.HandsetPortrait
            // Breakpoints.HandsetLandscape,
        ]).subscribe(result => {

            this.checkHandSet = result.matches ? true : false;

        });


    }




    private isTouchBottom: boolean;

    @HostListener('window:scroll', [])
    onScroll(): void {

          console.log(window.innerHeight + window.scrollY);
           console.log(document.body.offsetHeight);

        if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
            // you're at the bottom of the page
            console.warn(' you  at the bottom of the page ');
            //    this.isTouchBottom = true;

             //   this.showMoreVideos();

        }
    }


    ngOnChanges() {
        // this.videoInfo = {};
        // this.videoIndo = {};
    }


    ngOnInit() {
        this.initVideos();
    }


    ngOnDestroy() {
        this.vs.getVideos().subscribe().unsubscribe();
        this.vs.postVideos().subscribe().unsubscribe();
    }


    public loadingMask: boolean;


    private initVideos(): void {

        this.loadingMask = true;

        let testJwtToken = interval(100);

        let subscribeToken = testJwtToken.subscribe(() => {
            console.log('initVideos() -- try fetch token');
            //  this.getToken.reSendParseHash();

            if (this.getToken.accessToken) {

                this.getVideoItems();

                setTimeout(() => {
                    this.loadingMask = false;

                    for (let i = 1; i < 5; i++) {
                        this.location.go('index.html');
                    }

                }, 1400);

                subscribeToken.unsubscribe();

            }
        });
    }


    public isShowMoreSpin = false;

    public showMoreVideos() {
        this.isShowMoreSpin = true;

        let delaytime = timer(500)
            .subscribe(() => {

               this.getNextVideos(numberToInsert);

                this.videoItemList = this.videoItemList.concat(this.moreList);

                console.log(this.videoItemList);
                this.isShowMoreSpin = this.isShowMoreSpin ? false : true;

            });
    }


    public notMoreVideo:boolean = false;
    private getNextVideos(insertNum: number) {

        this.moreList = this.otherVideos.slice(this.counter * insertNum, this.counter * insertNum + insertNum);

        if (this.moreList.length > 0) {
            this.counter++;
        } else {
            console.log(' no more videos to show ');
            this.notMoreVideo = true;
        }

        console.log('---- moreList------');
        console.log(this.moreList);

    }


    private counter: number;
    private otherVideos: VideoItem[];


    private initVideoList(total: number) {

        this.notMoreVideo = false;

        if (total < this.videoItemListRam.length - 1) {
            this.videoItemList = this.videoItemListRam.slice(0, total);
        } else {
            console.log(' must less than total number of videos.');
            this.videoItemList = this.videoItemListRam.slice(0, this.videoItemListRam.length);
        }

        this.counter = 0;
        this.otherVideos = this.videoItemListRam.slice(total);
        this.afterFilter = false;

        console.warn(this.otherVideos);



    }


    public originalVideosNum: number;

    private getVideoItems() {

        this.vs.postVideos().subscribe(data => {


                this.videoItemListRam = data.videos;
                this.originalVideosNum = this.videoItemListRam.length;

                this.videoItemListRam.forEach(
                    item => {
                        item.year = this.yearHandling(item.date).toString();
                        item.month = this.monthHandling(item.date);
                        item.time = this.timeHandling(item.date);
                    }
                );

                console.log(this.videoItemListRam);


                this.filterFactory.filterName = data.indexes;
                this.getFilterOptions();


                this.initVideoList(totalToShow);
                console.warn(this.videoItemList);


                this.videoItemList.forEach(
                    item => {
                        if (!item.thumbnailUrl) {
                            item.thumbnailUrl = './assets/image/no-image.gif';
                            item.snapshotUrl = './assets/image/no-image.gif';
                        }
                    }
                );

            },
            err => {
                console.log('error from getVideoItems()' + err);
                this.vs.getVideos()
                    .subscribe(
                        val => {

                            this.videoItemListRam = val;
                            this.originalVideosNum = this.videoItemListRam.length;

                            this.initVideoList(totalToShow);
                            console.warn(val);
                            console.log(this.videoItemList);

                        },
                        err => {
                            this.dataRefresh();
                            console.warn(err);
                        }
                    );
            }
        );
    }


    public testme: any;

    public tryGetVideo2() {
        this.vs.getVideos2().subscribe(
            json => {
                console.log(json);
                this.testme = json;

            },
            err => {
                console.log('  tryGetVideo2 error ');
            }
        );

    }


    public launchPlayer(video: any) {
        // console.warn(video);
        // if (!this.isSearch)x
        this.popVideoInfo.emit(video);
    }





    tmp: number = 1;

    doSomething(str: boolean) {
        //  console.warn('-----> images loaded ---->');
        console.warn('img loaded-' + this.tmp++);
        console.log(str);

    }


    public isLoading: boolean = false;

    public popLoading(bl?: boolean) {
        this.isLoading = bl;
    }


    public newFilter(filter: FilterUnitObj[]) {
        if (filter) {
            filter.forEach(item => {
                console.log(item.name + '--' + item.option);


            });
        }
    }


    public isSearch: boolean;

    public handleSearch(str: string) {

        this.popLoading(true);

        let resultDelay = timer(1000).subscribe(
            () => {
                this.sendSearch(str);
                //  this.videoItemList = this.videoItemListRam;
                this.afterFilter = false;
                this.isSearch = true;
                this.popLoading(false);

            });
    }


    public filterItems: any;

    private sendSearch(str: string) {

        this.vs.doSearch(str).subscribe(data => {
                console.log('search result-->');


                console.warn(data);


                this.videoItemListRam = data.videos;

                // this.filterItems = data.indexes;

                this.initVideoList(totalToShowSearch);
                this.originalVideosNum = this.videoItemListRam.length;


                //handle filter
                this.filterFactory.filterName = data.indexes;
                this.getFilterOptions();


                //  console.warn(this.filterItems);

            },
            err => {
                console.log('error from postVideos()' + err);
                this.vs.getVideos()
                    .subscribe(
                        result => {
                            this.videoItemList = result;
                            this.videoItemListRam = result;
                            //  console.log(this.videoItemList);
                        },
                        errorResult => {
                            //  this.dataRefresh();
                            console.log('error from testSearch()');
                            console.warn(errorResult);
                        }
                    );
            }
        );
    }


    private arrayHandle(array: any) {
        let n = [];
        for (let i = 0; i < array.length; i++) {
            if (n.indexOf(array[i]) == -1) n.push(array[i]);
        }
        return n;
    }


    private filterFactory = {
        filterName: [], optionName: [], filterArr: [], filterGroup: []
    };


    //## ISSUES  does not garranty the order of the propery
    private getFilterOptions() {

        let FilterOptions = [];
        let arrNum: number;

        arrNum = this.filterFactory.filterName.length;

        //init array
        if (this.videoItemListRam.length > 0) {
            //   arrNum = Object.keys(this.filterName).length;

            this.filterFactory.filterGroup = [];

            for (let e = 0; e < arrNum; e++) {

                // init the options name
                this.filterFactory.optionName[e] = Object.keys(this.filterFactory.filterName[e])[0];

                //all Arr in the indexes with order
                this.filterFactory.filterArr[e] = this.filterFactory.filterName[e][Object.keys(this.filterFactory.filterName[e])[0]];

                // init the options array and set as null
                FilterOptions[e] = [];

                //showing name
                this.filterFactory.filterGroup.push({name: this.filterFactory.optionName[e], options: []});

            }
        }


        this.videoItemListRam.forEach(
            (item, i) => {
                for (let g = 0; g < arrNum; g++) {
                    // get the value for unique arr
                    FilterOptions[g].push(item[this.filterFactory.filterArr[g]]);
                }
            }
        );


        let pickOptions = [];
        FilterOptions.forEach(
            (item, i) => {
                pickOptions[i] = this.arrayHandle(item);

                this.filterFactory.filterGroup[i].options = pickOptions[i];

            }
        );


        this.filterFactory.filterGroup.forEach(
            filter => {
                if (filter.name == 'Year') {
                    //  console.log(filter.options)
                    for (let i = 0; i < filter.options.length; i++) {
                        filter.options[i] = this.yearHandling(filter.options[i]);
                    }

                    filter.options = this.arrayHandle(filter.options);

                }

                if (filter.name == 'Month') {
                    //  console.log(filter.options)
                    for (let i = 0; i < filter.options.length; i++) {
                        filter.options[i] = this.monthHandling(filter.options[i], '---');

                    }
                    filter.options = this.arrayHandle(filter.options);

                }

            }
        );


        // the filter Group
        console.warn('------GROUP--------');
        console.log(this.filterFactory.filterGroup);


        this.filterUpdate.emit(this.filterFactory.filterGroup);

    }


    private timeHandling(date: string) {

        let newDate = new Date(date);
        return newDate.getTime();

    }


    private yearHandling(date: string) {

        let newDate = new Date(date);
        return newDate.getFullYear();

    }


    private monthHandling(date: string, sign?: string) {

        let newDate = new Date(date);
        let month: string;

        return sign ? sign + (newDate.getMonth() + 1).toString() : (newDate.getMonth() + 1).toString();

    }


    private convertFilterObj(obj: any) {

        //  console.log(tmpObj);
        //  this.filterFactory.filterName[e][Object.keys(this.filterFactory.filterName[e])[0]];

        obj.forEach(s => {

            this.filterFactory.filterName.forEach(
                f => {
                    if (s.name == Object.keys(f)[0]) {
                        s.name = f[Object.keys(f)[0]];
                    }
                }
            );

        });

        return obj;
    }


    public afterFilter:boolean = false;

    public mutiFilterObjPrehandle(filterObj: any) {

        this.notMoreVideo = false;
        this.afterFilter = true;

        let myArray = [];

        for (let i = 0; i < filterObj.length; i++) {

            //  console.log( filterObj[i].name  )   ;

            if (filterObj[i].name == 'Aircraft Model') {
                myArray.push({name: 'aircraftModel', option: filterObj[i].option});

            } else if (filterObj[i].name == 'Fire Name') {
                myArray.push({name: 'fireName', option: filterObj[i].option});

            }
            else if (filterObj[i].name == 'Vendor') {
                myArray.push({name: 'vendor', option: filterObj[i].option});

            }
            else if (filterObj[i].name == 'Year') {
                myArray.push({name: 'year', option: filterObj[i].option});

            }
            else if (filterObj[i].name == 'Month') {

                let str = filterObj[i].option.replace(/---/, '');
                myArray.push({name: 'month', option: str});

            }
            else if (filterObj[i].name == 'empty') {
                myArray.push({name: 'empty', option: 'empty'});

            }
        }
        this.mutiFilterObj(myArray);

        console.warn(myArray);

    }


    public mutiFilterObj(filterObj: any[]) {

        this.notMoreVideo = false;


        let result: VideoItem[] = this.videoItemListRam;
        this.popLoading(true);


        if (filterObj[0].name == 'empty') {

            setTimeout(() => {
                this.initVideoList(totalToShow);
                this.popLoading(false);
            }, 600);


        } else {
            let delaytime = timer(700)
                .subscribe(() => {
                    //      console.warn(filterObj) ;

                    filterObj.forEach(
                        item => {

                            if (item.option && (result.length)) {
                                result = result.filter(e =>
                                    e[item.name] == item.option
                                );
                                console.log(result);
                            }
                        }
                    );

                    delaytime.unsubscribe();
                    this.videoItemList = result;
                    this.popLoading(false);


                });
        }
    }


    public tryFilter(FTmodel?: string, FTtype?: string) {

        this.popLoading(true);

        let delaytime = timer(900)
            .subscribe(() => {

                this.videoItemList = this.videoItemListRam;
                if (FTmodel && (this.videoItemList.length)) {
                    let listRam: any = this.videoItemList.filter(e => e.aircraftModel === FTmodel);
                    this.videoItemList = listRam;
                }


                if (FTtype && (this.videoItemList.length)) {
                    let listRam: any = this.videoItemList.filter(e => e.aircraftType === FTtype);
                    this.videoItemList = listRam;
                }

                this.popLoading(false);
            });
    }


    private dataRefresh() {

        const tmpSource = of(this.sourceList);
        tmpSource.subscribe(
            val => {
                this.videoItemList = val;
                this.videoItemListRam = val;
            }, error => {
                console.warn('error from tmpSource');
            });

        const mytimer = timer(2000).subscribe(() => {
        });
    }


    public trackByFn(index) {
        return index; // or item.id
    }


    public updateList() {
        this.sourceList[0].fireName = 'tony cai';

    }


    public sortingByDate(event) {

    }


    public latestVideos(): void {

        this.popLoading(true);

        let resultDelay = timer(1000).subscribe(
            () => {

                this.getVideoItems();
                //  this.videoItemList = this.videoItemListRam;
                this.afterFilter =  false;
                this.isSearch = false;
                this.popLoading(false);
            });

    }


}
