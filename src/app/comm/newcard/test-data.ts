// import {VideoItem} from '../item-filter/item-filter.component';
import {VideoItem} from '../../interface/videos'
import {Injectable} from '@angular/core';


// @Injectable({
//     providedIn: 'root'
// }) 
 
export class TestData {

    public sourceList: any[];
    public moreList: any[];


    constructor() {

        this.sourceList = [
            {
                guid: "294ab456-678a-4a30-9708-9c661c589d6a",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328C",
                vendor: "Cineflex",
                duration: "00:47",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/294ab456-678a-4a30-9708-9c661c589d6a/preview/PIX_001_328C_rfs_thum.0000010.jpg",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/294ab456-678a-4a30-9708-9c661c589d6a/preview/PIX_001_328C_rfs_snap.0000000.jpg"
            },
            {
                guid: "d55268a8-e5b1-4377-ba2a-61bac321522f",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328X",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/d55268a8-e5b1-4377-ba2a-61bac321522f/preview/PIX_001_328X_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZDU1MjY4YTgtZTViMS00Mzc3LWJhMmEtNjFiYWMzMjE1MjJmL3ByZXZpZXcvUElYXzAwMV8zMjhYX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=MDHONTRw8-GmYyckSu~QlZhaCjeQbexlbv3FyEBwikmHe~iZKXKtPGqfhmc1wV57DW2wVxpVb-Pt525~TM2puNSKTmWggZUya3iVqhuA83GJSfCC3p~6A0uY4zodRx99QJAAZ5NZN152Pub5mdYKTMv0S0Uxtv705Qe6M-WP7cM2Ldli06bJs6JdCMv1VEOneAntoAijFs27rieFmd9ny1EQ6C~lYrWO0CHUXrGCLXPBUw2G6-kUNFSaeTQIsdQUwm8ItNqLU2OoKkuWiRBhe1WPKigr8SJw-3WwTtfB4O4R4DNP040Rs7l2cJM1LqnZVhTGuXE4uM0o8Z4Y8PRpVg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/d55268a8-e5b1-4377-ba2a-61bac321522f/preview/PIX_001_328X_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZDU1MjY4YTgtZTViMS00Mzc3LWJhMmEtNjFiYWMzMjE1MjJmL3ByZXZpZXcvUElYXzAwMV8zMjhYX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=qq4bkdrgAjxXsXy9PuxhBfUb8c7juQ4uJj4U0gHM1GUQgbzohmM91VAwOcTJYNnkkhmmelQTAGchz9~iiUlRaS~XI31UjcI5mOUz3iyB8cY~uvoQKVo7rE~vzfqjnlyezvnDhwXAfMpATAg1mMElQq-uLr~ZUsGENxMOiJRtvtiRdnyi2JnJ-Wsgm8Tml-WqgEbF0ELOg-6CzYMxF7cQkUK3PSjxttEZzhPFiX6XNtVJLyYBeI~Hl7RXPWN5DYfvIg0N0slTvDBvMXlq~DjkZiR91DAu4VhUg4CjvLUA--h937VMJII2Wu1WSbWf-8tlcY~pVj5C5MJywk9XMIF47g__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "90ac69fc-2221-4bda-8317-e9a86e76eab1",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329R",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/90ac69fc-2221-4bda-8317-e9a86e76eab1/preview/PIX_001_329R_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTBhYzY5ZmMtMjIyMS00YmRhLTgzMTctZTlhODZlNzZlYWIxL3ByZXZpZXcvUElYXzAwMV8zMjlSX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=i7q~CKXzPqFxa-5oyixn8kQZjg4bYJrjrA9urAeI-fSpopMtM6QDfGHNgisHKfYvnbAe7fLFQ2-3zf1OfmXEhXSlHkRGoxQK~Z1uvx0~cLELGIGF7k12aR1Qbr6f5Cm99JjUX6xpfd5wD~sLAdbF~SpVeBAuRx~kP1jH799Awk6MULpm0F-VmRbj02OiIv0hc-4C3WRqOKhv04Z5gILRM3WfIRJwuPNGf0nC5AmYeAhonxfv58ARyEEp-iGOikb46r2EHQPcqDthoylMaeF2fSQT6R-XIQa5clLNHP6nYSshELPMjHNw63ga7reAOO6OyURuTOzSX7dI1~cG12NT9w__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/90ac69fc-2221-4bda-8317-e9a86e76eab1/preview/PIX_001_329R_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTBhYzY5ZmMtMjIyMS00YmRhLTgzMTctZTlhODZlNzZlYWIxL3ByZXZpZXcvUElYXzAwMV8zMjlSX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=QA2pbdb5z25fBQeFGwtwBFm1pctcO02HxlrRACykDEtK~xa52pV2GmbS~QJkpKY3Y3TVOeNeZZYj-TovK1LRONcvLTs0XvxZJi5tBQP3N-nml7VgXQTUnbdZl63S8vl5eyobT17VvzNMkYXHVUuSPtW5uomDnpCsRPdATGSg6698jugLObqKz1wDGc7atI9j6PO7~-q6mqxLnFcVbKLqoigNA0UZLsKz-Nkk7wDtrqDT0Psq0GaWd-Gmb3CO55QMcc2ESbPC4tmzeA2oOzHIN~xG2GsRbY3kVlmCRrixNFhto3lUKFho7QcAFxl2JF5dfh6aOD7R2DZdHHJ2Gfdacg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "0b917d13-7cb8-46ff-a48b-a5fc2181cf96",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_322",
                vendor: "Cineflex",
                duration: "03:03",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/0b917d13-7cb8-46ff-a48b-a5fc2181cf96/preview/PIX_001_322_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMGI5MTdkMTMtN2NiOC00NmZmLWE0OGItYTVmYzIxODFjZjk2L3ByZXZpZXcvUElYXzAwMV8zMjJfcmZzX3RodW0uMDAwMDAxMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=uEy4gWke6mpVP2zhT4AFi7kxLCsJgMzgJeWugBe7cylFhXclCs3s2lmR-V1ay3zlOChRJ6gQc6HZ1sBsbbnFWJuesgMB6iP9UY4IMHd81MFt6N~nOw8xn7GOeqwEYuYwp4XxVID6BRL7QduLhyiv~eRngbxjioGc~RsKrqc~XKq3FTopGb-bcILC00YHH7gFM1a-jAkP5PynQNWq2w-POnzqEpLmzS67oS55S6FX5691-Wem8JbCfJQ-nk581TGBefE-15eI6nVN83jWRxvUXoFm9hBr0-9eJQfItnF3lSV~~egPk5VOJRpV0pSFj8MrNT9RJTS~UQi4Ysfm3pJKMw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/0b917d13-7cb8-46ff-a48b-a5fc2181cf96/preview/PIX_001_322_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMGI5MTdkMTMtN2NiOC00NmZmLWE0OGItYTVmYzIxODFjZjk2L3ByZXZpZXcvUElYXzAwMV8zMjJfcmZzX3NuYXAuMDAwMDAwMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=xmEZz70NztdDCpE~s6NRsJY5a5nypPe1PY80-X3ZQi3FLlqn8aiK8ud5z23HYY~szdqU430qdidns~KzN0JeuGumro3CKhW5DZvtjUjpU8XQ2A4p9LGVW2~wVBUPD7Hgnu8LAXEF6Q~x38khIJX9pHINYFnbQYOypNjG~VvPPRf07buCO5U1fXgE9jf-fCrEKBhIz2Dis6Keyrglg~K~mDUQ1l1DwTjuZ8gT1zh7BjNOKnxY51Go4HGeSG9uiBHKVfXkc28s3Dk7p9-qraNWGA45xM1BnPixcFy8UYUuWgWN7iadjagnXnTloS5yVaIiCMFgkxr9XBYJGOd5AqeE5Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "f34f7d4d-5284-4524-8c0f-59519dff317e",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328AE",
                vendor: "Cineflex",
                duration: "00:06",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/f34f7d4d-5284-4524-8c0f-59519dff317e/preview/PIX_001_328AE_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZjM0ZjdkNGQtNTI4NC00NTI0LThjMGYtNTk1MTlkZmYzMTdlL3ByZXZpZXcvUElYXzAwMV8zMjhBRV9yZnNfdGh1bS4wMDAwMDEwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=gxW0E7zv8u9LfqpLDPVg17tRr6EpwEHM-7grx98oNVOHJA-pTsRoaqGHeH~qYZwpKLdBnUTIwGwMuR4mC~xRBdepLJ3MEXS4hDSzSgp0DzaPXo4xump2eRwrGj6bXis5zZq~kbwkyBHlflwfB33sZNoQkZGc0krwz2RvJzje8gBPEaRk~rf~Vo3ArgGEFJIQT~g5Y1QAqSdRE-1iQTfeTg5y8iBNS4PxulfshkCPP36Hkl2jkAUkNdHr508yJrVbQTau4th5hej-RNdlVjead52-8w6jBXbtjO6tsIRPZnesk-dkUNqYaHkd8HY5n1Lmqh78bHOA15WSlR1xvEBQpw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/f34f7d4d-5284-4524-8c0f-59519dff317e/preview/PIX_001_328AE_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZjM0ZjdkNGQtNTI4NC00NTI0LThjMGYtNTk1MTlkZmYzMTdlL3ByZXZpZXcvUElYXzAwMV8zMjhBRV9yZnNfc25hcC4wMDAwMDAwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=rp1RCYqvX6kfkS7GO8sCNmWdz66CPqFcrmYWX0t2lKhxu5ZfacVPVkEW~2Y~DFe2vx9yq~eoNm~powK7mgEN8PCRHu0F0Pzae6sh2Z0xq~vpK~xzLRw2D25gIQa9RHcOCuI6lc88bh6q96c9ra23z9L361YrGt1146YMzHr32J225c2XnJGPl8DlQTIXwTRpTIbcuTSnuvJRAt~gWbnefzkrer4-WhuCqOef0-~oGSvYm6hjwniO3vDfbhVrdcQLl869gwurr~CV1NRGm1sVIR9r5~5BAbk54twj~26oP3HbAiSL8nppXyCerbOL7o2KCtB-i4uo7TRtk3DQUYjVLw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "829f1c1f-3a6b-4be2-a226-6214948bf179",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328L",
                vendor: "Cineflex",
                duration: "00:05",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/829f1c1f-3a6b-4be2-a226-6214948bf179/preview/PIX_001_328L_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODI5ZjFjMWYtM2E2Yi00YmUyLWEyMjYtNjIxNDk0OGJmMTc5L3ByZXZpZXcvUElYXzAwMV8zMjhMX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=WQkUOMRqaacze6Ke2Nkkp9RYMTT0werkuvdFX7vCLzxagK3tVumWsu2h~P9JOJN6A4xvO-O2QcRgt50yKieW89PVM3rUxwAoIfYjLT6vyS~FvtpxJkEt7kMfhijKRzh0xJtHNgMeOhAspCtEz7SYOw-~bdOXLOObIKaH3k6r-9O0Uu78Lz1jcJ5D88QvdTtrd8GDtsH~gXcBfms8Xc5RmbCGUbN85I1OGkTo12UKbKmxa~cF86e6KWenVT6BlEnXaOnsGCGe8DCOuPkq8FoobgY8gcpldzvBPZFlzp2bK9kKguHD4pl6ipijdRA4L1tpCyArQtyKKAbuEFnyM3g~QQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/829f1c1f-3a6b-4be2-a226-6214948bf179/preview/PIX_001_328L_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODI5ZjFjMWYtM2E2Yi00YmUyLWEyMjYtNjIxNDk0OGJmMTc5L3ByZXZpZXcvUElYXzAwMV8zMjhMX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=AMkUV-nopRRkHOHUiSoKuORMTf5EJ7QOMg7MQJ55nLgcsC7z3d39bkjs3JMVdMqcW2e4bfXtSRyyCEZXgRunBsurM1-iud34~oHkXShcbBHXsHsPMnj-4BfJ39wBdbCBksXfdczlzCsUJzosmijwUbl~Nv2M06b~vRDaVNP6HC3-YL3vipXSz~djNAwhOURgH8Vik21YJRqZ~Q6zld~MCJeQ6ZE4ztjV0qrVf6-HJYwN1yWhznfxx1P9QTPutxekHpEsmOTPBur5nwQZ5T9g2XNFAojv1Xn57ogq4u9hYhAZ7W31Dnm69r5~8aBzVf-pfs7GJn4yqtBWPag8jvk79w__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "5b93c37b-f0e0-428d-ae0d-1e13186e66c2",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_322E",
                vendor: "Cineflex",
                duration: "01:20",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/5b93c37b-f0e0-428d-ae0d-1e13186e66c2/preview/PIX_001_322E_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNWI5M2MzN2ItZjBlMC00MjhkLWFlMGQtMWUxMzE4NmU2NmMyL3ByZXZpZXcvUElYXzAwMV8zMjJFX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=tP5HFMYkzwEbaMRnkiULm3I~9-bkB7Tm6xW0td7A1rBVXxfc3NzHkaO3exrFivVUBC40s4UoGzlfaTJ9neiUAeUkB3~P-kjnvpgpBrnZIPOkf4MvRsufgGvRIM62Pa9VkFDNB8uHndy~9RaVvU~6QyqbGTlP1Y-od5id5HGn9z4awpR9bIeIB--Ol8tKLCdPLUrLMtWGAZKAe1CTfs61s1Pz-9kYZItWQwWcqRm4QDOQs2HEw0~91i9ku0BhCG4xsX0NtseT-VXcL1zC48Zd6TriJh~z24mT2VBlQ6PWmbYuvU-k0eA9D1KWjJg0c2GxC355oHBUC49gTZiHu4ipiQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/5b93c37b-f0e0-428d-ae0d-1e13186e66c2/preview/PIX_001_322E_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNWI5M2MzN2ItZjBlMC00MjhkLWFlMGQtMWUxMzE4NmU2NmMyL3ByZXZpZXcvUElYXzAwMV8zMjJFX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=slbQTwVtLBCjbzoD1ucRj2XkN-3RhOa6xuHThanj03qjBf5Ts-6gzKOvsHObw2nJMwAyFghVVpAEteKf80wWf6oaqAFxuPnGBgj~UHppPClZb1kYpgqQls0gsog7KQZ~g9izD8hzVISGjEgfkav29GhO2vNkz199OMZXJXu1RmjPT9o~nwflMqUmrssIMGJC2d7uyUgk4OYIiZzdw49LvOeXyawnfYU8I1X588lfBIVuMKHXcyYSUqDlI8MU4OtyG4MbjMX7Agnl8LKptQNIfhQNcovBgcTCChdZdCyxG85y6EEx9gX62N5C~BG63vHWnIVnFpo6k7f4Cf79b2UIRQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "36a89a35-2dca-433d-b49d-7b8392c6b60d",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328T",
                vendor: "Cineflex",
                duration: "00:01",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/36a89a35-2dca-433d-b49d-7b8392c6b60d/preview/PIX_001_328T_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMzZhODlhMzUtMmRjYS00MzNkLWI0OWQtN2I4MzkyYzZiNjBkL3ByZXZpZXcvUElYXzAwMV8zMjhUX3Jmc190aHVtLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=TJ2LgfWsDHjlSzbKLB7u21PWygTDBt8HGAA9zbE299wjzHr2JIcgIrWhThWYfGRxIhRsjOY~PqmlYOZhEWgwefdnfGoPSlbVgQ1HldmVdBTJ4L9PWiu~zbp5FJxH3ayU2ApYdLOAZGrzIKM0175hX7KygAlzypn22VwDIBhBF651ukw2HLkKGMG3jQ3R-tYYJj4iZ14OVzeUkwbQ8HkZ4oxqLTEVjW2-AymO2HW64EJRAPqHiyO8sfmh-1UhGdIB0x9sAaa0LyJb0sC8KttsQOdzwiPdF6CUSZuPOZlzC9iGpv3CbyCJXitX1dqHMXrWFvm99kks2mSLWKX6MMJBvQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/36a89a35-2dca-433d-b49d-7b8392c6b60d/preview/PIX_001_328T_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMzZhODlhMzUtMmRjYS00MzNkLWI0OWQtN2I4MzkyYzZiNjBkL3ByZXZpZXcvUElYXzAwMV8zMjhUX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=DTimz14khvYMluccQ0RXgbukEhjcJ4cayvcfIpYgNbn6Ex76JHRfbcxiIsaVHRfy2P5fimd148ndq5eNgccO4FI5JK3BIIIdywf5RR7K7jLl1JbF5qKY6ioU3Jm51palswMFZusEU613URnDcOQ0Fg9HjvxqmyRRRu~4xwmEQZlGpn1rdd1zG-FMvm6380zwZRl-BHDT5fArGWLq-ydje7IKDsgq0zTdYxfOvG37hnsCWOfWeRjx-04aD7I6KiScTjm3afJjruG6Kk0ihe2ye~FlaUuuBmflXeNkkFxX0EGkyPYDh0cmRZvdYkz5bjmiKtrC5znqabV0NmBZA3UYjQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "33bd249e-ad38-462c-add8-5958e35c6c22",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_322F",
                vendor: "Cineflex",
                duration: "00:41",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/33bd249e-ad38-462c-add8-5958e35c6c22/preview/PIX_001_322F_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMzNiZDI0OWUtYWQzOC00NjJjLWFkZDgtNTk1OGUzNWM2YzIyL3ByZXZpZXcvUElYXzAwMV8zMjJGX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=BKBNW-FF5KLNfFKSr6fePQN0EGa4N9N0c4R3u1-hbQZaNz2d7hKNFcHU0nmDOrtbsFC1spPOJvZVv7~ojRIeYgTHF8xcIKHLUauxiQ5gnCTzVkDvwmf6oc70QcEfRkqgxVrCwinUZoI~z7lNBxZaGBQKUKVLcPFBgMUPfDgz3A0V40Ony~g1gZ3a6wg9kVn4YLVzoDj3D2II57sH71ofJR7v9UCCxoMSLg2be2vyVxABCasM5hRgWFEts3kLtWK5yC1UhVflFoAy-LPb7mOgJnyJ4NPUgTGsIimbQf3ntBHJvt7asjYLcUA0zkP5ikc-AFLKUoNSE7V7ARWYmglrrg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/33bd249e-ad38-462c-add8-5958e35c6c22/preview/PIX_001_322F_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMzNiZDI0OWUtYWQzOC00NjJjLWFkZDgtNTk1OGUzNWM2YzIyL3ByZXZpZXcvUElYXzAwMV8zMjJGX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=OGX1GfO1k26A0Mvg6eve-E9DScxbfsyYeTn7wagHa0PV5WToT0nX5TLWh8bpZvysh5lpVPvhzdeNZcWEjR7yifE9Npgy3tB6i2locAY6hCV25UHAY77P0Ywz7hN9~RF1rj1R~va9k3a35kDfpUKSLRijpAz0yriidDCuNHktdfc9cQSItg-j3LemcpJOmeTau4ll8mRIC3W7IOkVqgWdMJcsvgz6yfrMUyHzC4R56kO54sSwMCPTYCSMy2Q2-Jy3j4zUF1cMtEouBUt60Qh03T7i7PAifBd-g32eA6hIhMJpxsX0JFiLCSN~j22JqtzEGORgPH~aYnVCYDyo8NjlNA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "4aabc311-98d1-410b-a10a-5ce0fca6a092",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_320",
                vendor: "Cineflex",
                duration: "00:36",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/4aabc311-98d1-410b-a10a-5ce0fca6a092/preview/PIX_001_320_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNGFhYmMzMTEtOThkMS00MTBiLWExMGEtNWNlMGZjYTZhMDkyL3ByZXZpZXcvUElYXzAwMV8zMjBfcmZzX3RodW0uMDAwMDAxMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=tywUAURpWCmSiPER-c-A~IYfsgZIBmsbX3EftTP88rFyARUd6RJbIcMEuSfs9OJWu-4htCoLO0CNDSz4NwRNqr5DCpQMHGHgv-r3oj--HF480qmHESCRJfHH5V8OSCurDSudBza~pf5E6Adbq~ZcucJt2S2lR0Rv1l49ayK4DxHJnaoRHyw6b~yudrPC0hsTCJz85yB8mygQFcuab6HF1D22gTAdr839NxNK8BP6gid4k5bgf0R5TkUFHO0GQuTCLBtKmXf7Jc2AYpX3DKd4nZ14~9ndDNxZYR5suDa0NLEWAwx5FWnml~6Z0dhHV~gY~QHrWEC1YW4~Mwk1zWxZsA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/4aabc311-98d1-410b-a10a-5ce0fca6a092/preview/PIX_001_320_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNGFhYmMzMTEtOThkMS00MTBiLWExMGEtNWNlMGZjYTZhMDkyL3ByZXZpZXcvUElYXzAwMV8zMjBfcmZzX3NuYXAuMDAwMDAwMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=eiuS4BZoPeCyzaNedS1ttyCIpgFdhjOrv48rpLCBXf9NBjkWkfvWGyTAQBL8nCnnG9oy7cmrsnWlVONvZQL4O3aJ58d4h80EzQl9-dcAktzprD99sv7mPyt9IMuSFlq3mfqTbB-6eLGFr5Gost6~XCVDAdioK4XhWhGR9Lt9RgG3eXyPliVmWjOLj8l-mq6c5ZMzHS4er7J-sca8hQsekveMjAf7e6Ot4h2CiimgjMYdWhJXiluDU~pu1myvowFLQKm1Hob1XVLY-Dsm7MgypXptwmMPPpGLN9IgbyVu8Wy05KQzDXOQYZoNMcwXlBoREi0QAoMflsnNhRnTx207mA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "1a859c47-3572-429e-859b-97a0fa64938b",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329G",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/1a859c47-3572-429e-859b-97a0fa64938b/preview/PIX_001_329G_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMWE4NTljNDctMzU3Mi00MjllLTg1OWItOTdhMGZhNjQ5MzhiL3ByZXZpZXcvUElYXzAwMV8zMjlHX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=M5JIm9H7t6upaINZHmD5YL6R80U8z3UqXCRMICmc0ds5JI~FxZ9NDLXSZYR4dttktnMOv3nIRhcxJ~MYyErm0NqmLnN7s-YNjSqX5CPA~HV49ijANvw4IVRVkT2kG8Wb~8Skrm0VHTdFK-66tvVoBrhuUNoPG-D-Bm7pvXVpDxcesAH87ER97GvHKGp5JgZPwPvpGg1IM5BkRkpVijwGUch5xayITIO6sJXaHKWo-4ZpKPTrZUydWMlkTj0Ux5ufjhALBjkI8tAmL9xnNWDoSIVK4FqMlrA~4Z1cxfFtHmz~WSTx3PnHua9kcwIeWipT9swUg8I4kaS4VMJ7xdbpXA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/1a859c47-3572-429e-859b-97a0fa64938b/preview/PIX_001_329G_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMWE4NTljNDctMzU3Mi00MjllLTg1OWItOTdhMGZhNjQ5MzhiL3ByZXZpZXcvUElYXzAwMV8zMjlHX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=FECa0v4TbVRCTYlukr-Pl9gx7om4mwtYKYyBXoLqn7CcHikk1CwMntjXUY26pu8mPVKCczED4V9G~1Lq4-0BqsbWGmw~wIYeKWXp~D4phZNGhsfU0~OKzGrpUFJz6NjmOeyEw4YRwH5JHqJc9HPaAtFaQnU9ntw~3PFHzCRaJuaZA6xqsjT4a-b4mNrTrMh0-OuUq4mVdHQpD1j5PTMWsOhK7QNasIS2y-p7MBEbZBuFnkcGZo9OZ~yRiCeVWujzka-lsF3RHY0Oo-9j8taxSi0zIz-yqrqs2koMXH6~eg4CjNM-k53yS5kVFUjOGJQTiypKUo5tkFKiScu2o7kgAQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "a4b0e7df-59eb-4bdf-bd48-7ea70ed1bd4e",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328F",
                vendor: "Cineflex",
                duration: "03:51",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/a4b0e7df-59eb-4bdf-bd48-7ea70ed1bd4e/preview/PIX_001_328F_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYTRiMGU3ZGYtNTllYi00YmRmLWJkNDgtN2VhNzBlZDFiZDRlL3ByZXZpZXcvUElYXzAwMV8zMjhGX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=QpHifJVF~iDnVjt4rLbolCx1LvE4D~ryW4UFKRXH~KKQFg3Skt6esSUZM4vOqHtyrto0ApinuxMEL3W-lH72A~iCPhSHIO68V-XWpTI---XPAHTjDNLMi-TaTVNhNsD4iowSoGk4IpkV8AU~oDl3cfeSWf7s8D3eiuxWO4E2S5vckGW~Fznt~pXb1BU7jOr-k0rcnMYnLCH2466L~OpzQmp02VVaw0HXOJA18QJyEbl3buXOdH88Bzb08xMrOUddGuxshwXkrx7W-D3yj5g5UWBc9m-OL-6qsgrMPK03~veriTxKSCiJyBS9DzT4DBsXGXNph7fx4~OMu5nS5kieNg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/a4b0e7df-59eb-4bdf-bd48-7ea70ed1bd4e/preview/PIX_001_328F_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYTRiMGU3ZGYtNTllYi00YmRmLWJkNDgtN2VhNzBlZDFiZDRlL3ByZXZpZXcvUElYXzAwMV8zMjhGX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=xPw8jqZlHn5PbkKmllwcQv9OpNh6HNHZOIsNnCMh~U5T953wAtf602NYzV6F92nPVeKG3x51IBLaGEdFoovXNzBZoc8PM~75RZZCeFzvjKosPsxdIFoAaiYm9J~PMSu61J8QT2M14U9WP7Dp1Pv-DsoQlwSN33HvwH0-aQzog-P4UhqXGv3JWFnh7l9sl5MSON~ZvGKMbVFIesHcZLTa-~ItTTSVFLaBYU2fCLSNJ~eiSUAHaXGogXZ91PVtxNaWptGbIwFplXECIXs-dw-zAFrqvfJhEIzzwbUpUfTbmVONkV6ZDqRgb-zvdeqgRGnS6wE8aghn7tYHWreo0q12vA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "423efdb3-11a7-4648-80e2-1fd301e949bc",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329AE",
                vendor: "Cineflex",
                duration: "00:06",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/423efdb3-11a7-4648-80e2-1fd301e949bc/preview/PIX_001_329AE_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNDIzZWZkYjMtMTFhNy00NjQ4LTgwZTItMWZkMzAxZTk0OWJjL3ByZXZpZXcvUElYXzAwMV8zMjlBRV9yZnNfdGh1bS4wMDAwMDEwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=Sci81wMZHtYXWUn4UKLcCrIv7~PWyjJzrrb73zlnaPzysDB~IZzyKBcpbdR~NQ11h0buWF574MHCBQtSuOVueep9~~3DhuIRguuQ52kM3Qj3oiC~ICF4DmdxFMy--O2RJNHen6prOKKjxCKPpw-Ptzlae-PbcwhMzYz92uK~gVg27XMDiREpDxlCAnIEE~DoGNb~TcD~SWjkjDSrg0YWYKzeJMKj0JuylgwajgHcaNpQs2pHpX5zBTaD9OVujAAL70YkOzJfZ-44egqZYhtgpGs7QeGwYlpOze5xq~npyY52NFy~ZhMSbpaS12O-caFYxLTiDrE35oYfOnzDlLLKBA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/423efdb3-11a7-4648-80e2-1fd301e949bc/preview/PIX_001_329AE_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNDIzZWZkYjMtMTFhNy00NjQ4LTgwZTItMWZkMzAxZTk0OWJjL3ByZXZpZXcvUElYXzAwMV8zMjlBRV9yZnNfc25hcC4wMDAwMDAwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=diHzq2vQMYjN0dspMWj3Z4Hr3Qjz0TZfxd~QPSqfAv7Tvp2F7BMLOGN3iEsIHjMB~tQzbhY0VRbyIvbJhh0iv4af8RDrt4TY8PMlMNZNVZ8shn8BP26pRHqrkcyJ~p1UelNn3Ldvzk1~16Yz57EtaVcC2RwNGHmCK7t-9pPGILCbgmYCg8i4kLcOYnCJQ9QCLcgmXBwPkF18S-2y5vUUVfKGjVArMXylxZaNJsT3IcfKooJd5VlhD1K4jt4V-LgESvgu4902vd2lx9xFe9nH5Ze~TorEHWnHq1qa3VOzv5RlcNOG04~aYxQg8kcwhBteYhGXclokNL2UoY9oM5eoSg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "3afd6cae-6894-4fac-ae7d-b714fa53d9f6",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329W",
                vendor: "Cineflex",
                duration: "00:03",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/3afd6cae-6894-4fac-ae7d-b714fa53d9f6/preview/PIX_001_329W_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvM2FmZDZjYWUtNjg5NC00ZmFjLWFlN2QtYjcxNGZhNTNkOWY2L3ByZXZpZXcvUElYXzAwMV8zMjlXX3Jmc190aHVtLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=uRxcpJT711hpe5tuY0kXnZFrDH1RYy9q0tffYAZXTmISx-OtM1twCU7E17jxlT3Sr-GHiX7zH8TixlYXW0cY5jcg~dNzdwtOSH-zjLhz4f8KhaeXiJoOAWsYV5-PBLwR4-1~qR9MGByr-Ze14dOCIzeSwtiULssLRGi1MUxAsGS45Nma3i5ySQKLmCK0UlaDH6cMjaqe89N~3mgnqjxx7KATdBw22mG-1riR0z-06qLuy6gbvwly1TK5oI3ewLHvCPF0XwfibyNaZ2n34yEPKBkdmE7hjS51xGw5QYvm5Y7cPthit-fXcTnznqMkz5U353dXt7OEUSPMm9tu8ZljwQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/3afd6cae-6894-4fac-ae7d-b714fa53d9f6/preview/PIX_001_329W_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvM2FmZDZjYWUtNjg5NC00ZmFjLWFlN2QtYjcxNGZhNTNkOWY2L3ByZXZpZXcvUElYXzAwMV8zMjlXX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=zdBZrcBAAXYionbWqnE~qHkfkSWZrg9-XYdRKYZ-AHbc0eZop8o66AjsuLoQ~yIO42lAkjU2~ZBCgG-Km0EaEZSwuu55bgTZkvi9C~BElOTiv-bGvawJCi61nyi1rOw28-tFCx4QlmC39GOHVl~B4mnCKJA2Q3xA14ZwUn7GsSQCoXABKqL42rVG-0vxU5sSnha12MjTNsv0sjnq-PjVxGWZHdRCzWUa2xkpWv63qhHGj5~gpPkwKembarsmFH1pwGtSToF-85RBqBQprHTJfLrU1inaxwp9siuK1nfCmRef96wLqp57a7GMeycTqvuU-zXO~4qki9PsBOnE~Ze11w__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "2a1a0610-e904-401e-801d-f054a4e79ba1",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328AB",
                vendor: "Cineflex",
                duration: "00:03",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/2a1a0610-e904-401e-801d-f054a4e79ba1/preview/PIX_001_328AB_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMmExYTA2MTAtZTkwNC00MDFlLTgwMWQtZjA1NGE0ZTc5YmExL3ByZXZpZXcvUElYXzAwMV8zMjhBQl9yZnNfdGh1bS4wMDAwMDEwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=c3WyxRSV0EqFPPnc0HG5QcoqBB2zzMgO0NUzYRbkEvH0HbuGs8Ib9SJvDbpmcMrN0YjoHDNyArTEQokjklhiExMRe5rPiIUJ0GgPgMowZpcxN8BY-0v4SP1MqxKAKTl0VAxSb7WVPWmTtypBs21HjnZycYxLeA~P0rgMGXIivK6B2GI6Hlw~qDEFuxNxgE~lWdVYD23plVTF9GG9Hg6H2ne3objPVzC6vH6ynaiZ3JuQ99HprvwYR2oe7hfyfLJp~WKr~8hgWYRVLd2ujNq8RKkz27~Y5In2jIswiCPRot2A~rTsJDLhYWM8wu08fn-R5IzxLHY9Jq18XApx0Hbw3A__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/2a1a0610-e904-401e-801d-f054a4e79ba1/preview/PIX_001_328AB_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMmExYTA2MTAtZTkwNC00MDFlLTgwMWQtZjA1NGE0ZTc5YmExL3ByZXZpZXcvUElYXzAwMV8zMjhBQl9yZnNfc25hcC4wMDAwMDAwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=jdAaDmJ8AhCykxSDY31njwEs1xb4q5N6XoEPw3RwLJghb3QQLvT~~-swM9Co6bsjUogfQX2sPVpl8ZEWzvZhldE1DfwtT8GFTsghE2Q1cRsKLWG2i4MYEviTYbX72JBdn4gFkxHX3qXzG-JrWuiKdDo4zdaQ9PlTBKbrGXuSWRnbSIZxsmneri6mYosE8OhyeTcnv7BZ9cR7d5FvG9yJVTqKMy-iWh61JnnHf~5qIBcnvKTPpKgu0lIDHzpcn7-Bpd9y-bsBSPlNFRl1Me6vZr1oJaZMYAwpfIXdPkI~ywwAr~BvmqaSeYTBShamuCA84BBVtLTrICm6g1iuCXi0HA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "9657486c-bce1-4821-9a1f-0b7aaf7557eb",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329K",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/9657486c-bce1-4821-9a1f-0b7aaf7557eb/preview/PIX_001_329K_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTY1NzQ4NmMtYmNlMS00ODIxLTlhMWYtMGI3YWFmNzU1N2ViL3ByZXZpZXcvUElYXzAwMV8zMjlLX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=KJMVdGpibDv-~MqYi3zlIpG3VScyyHxTPPMz~bgAmyHBS~umUs9Pjjix8dvxdFBPR2HpVr0MPw6RhXDT075NnVQw-GJyguf4ifUjzegA~v5gH0YBKnOFYMLX8zyQqzUm6J1YbMk2w16u0XJTg3pa0rDTwI1nbAT3flrThEhQYIcVkIepeebLZZXF16zfyFj4Ri6mDpfkvT8qV0QM-hcHXa1l8~wXiB3w6zurSwB2IoOC2ihtd1YnLsxtE-RdLO1krUmsH6p25xGXpsg0UzZEE1Tgiox6aEMLqbNYuHoWIRF8d~ziBP2OoSmBTOr6lG7Qg9beF3mRY5-z3ZtfwKVVmA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/9657486c-bce1-4821-9a1f-0b7aaf7557eb/preview/PIX_001_329K_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTY1NzQ4NmMtYmNlMS00ODIxLTlhMWYtMGI3YWFmNzU1N2ViL3ByZXZpZXcvUElYXzAwMV8zMjlLX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=ExCXNiSCmHGxE4KgdtL-3y-79ypTzAUdnC6~OV2ErhQh0YlMmCM6kUbVJhKgr4pRurEHV9cqRoH84P35V7R~ewwaITW~SmZpTyZt-ANuDLraS5qHOnYplB1d-HTZU-g9rcICXlQjZw2lkz-offpPQr~M1uZ8UA55iOA3gbiDPLvoIzZpAdgraUK3N-21luPgs4P9EG~6rmH5cLo4JKnBebQxEIpnSkN~pt671APpBCQOx3pY2hdBkUsEcqsp7IZf1ldwqRAsB~XV~b2cODl07kMKwndDjAqnUtLzBVJA7b36kMadodNNTyWlNka7qnDWiGfavgNQtZ89mLCc2eLziw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "34437e80-e77c-40d4-8e0e-706710e129c8",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329Q",
                vendor: "Cineflex",
                duration: "00:05",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/34437e80-e77c-40d4-8e0e-706710e129c8/preview/PIX_001_329Q_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMzQ0MzdlODAtZTc3Yy00MGQ0LThlMGUtNzA2NzEwZTEyOWM4L3ByZXZpZXcvUElYXzAwMV8zMjlRX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=PPggDEChH5xLKcmLcLZMtPhH3X08nkyirZAZ9CiyxhFr4Gscwk7SLiDVS-LHjiJ2pX6si3g79rHVdb56574sbuQp56GYL7mUo2b4icEKR9~OkHPym3tqhYQY6uxrpWHnyTyt57i~nQpkFcsnKzseKfhBXwSuwdgvy30ycJJf8aBl0GTnPYUsmEb4dY0B6F3ZdeWAu9Hy7hEPIaJQ-FNgyrgc1jaj9L4P29kF7SIjsutjmCJzfPXsIXCEb1SJczah-RXaLiBTI87WZtAZ2IN1DE5rcrrlaE-g4iHIfqjmI8Z~yZ36UVtojcIvaNDZ9wcTNjBB19QjQupzZTvailXs8Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/34437e80-e77c-40d4-8e0e-706710e129c8/preview/PIX_001_329Q_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMzQ0MzdlODAtZTc3Yy00MGQ0LThlMGUtNzA2NzEwZTEyOWM4L3ByZXZpZXcvUElYXzAwMV8zMjlRX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=K~GYeczDVTyAZHYY6P1Iclpo9V5kxnNAiMWPaNwTyyADncQ8dQKbPcEFz1bbkUxZ7qdsi~PB0uqFtnnIa4XW59b7hRPb~q0J7kkjsYWW6xNPVjtWgsXoNRzpOd-MVIODtXsA9O~Yikww~BWctYiFTeYva5qngDFx8yebQZAjBUVOxpiznZoHchiOXG6EAVEPtNlxPQMMef7M1qBOPgKNKZpVVeP7a-yWVXK-IHnfdQvp07cMJDq8wgPJyqL0Pn3zV6a2KpWCkUfdxnDur5Rm25dgZ~sYKEfwax5NgFn6ObrY751W7Y03zTHO8AGfZ5gOIqjJ5DQkxT-QPrE0~7pJ-A__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "6e392589-5b5a-4d60-81a1-f5e74eee1b89",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_331",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/6e392589-5b5a-4d60-81a1-f5e74eee1b89/preview/PIX_001_331_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNmUzOTI1ODktNWI1YS00ZDYwLTgxYTEtZjVlNzRlZWUxYjg5L3ByZXZpZXcvUElYXzAwMV8zMzFfcmZzX3RodW0uMDAwMDA0MC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=jwR79hW7sbPzvDCS6TB5XiGb9GwonYy9dRsPBWAIK75wzIaX0j8F~aAo3eYezCBQkGhg77uhBmlJSZjIQ9dMuXsHcu7Y14FHnZ8u0Jbt1WfhWyHOjyEyLEmaNmUJ64ODDYdQBUqnD6EqgOshvj0eqc7BK0Hb5-zw8Gag8jIfZs-K9cmf9mZTdDKFj0uQGgQr0stqhAZhECN0fvZ0X3utgv8hn5NALcjHTA67g3EsaX6u5bE4Z4RLgVf~AYCAD4pctUiJ68nKmhraKN5rFyAkzsxDliFyT3z-ofXbO2PmTV-n8DMuS308BHWVPwzTioRYfl3PtdKifWliyxgJp7~-Iw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/6e392589-5b5a-4d60-81a1-f5e74eee1b89/preview/PIX_001_331_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNmUzOTI1ODktNWI1YS00ZDYwLTgxYTEtZjVlNzRlZWUxYjg5L3ByZXZpZXcvUElYXzAwMV8zMzFfcmZzX3NuYXAuMDAwMDAwMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=clDY7W2wYN~-L-~mzUgUJsSLXCybU8LnUj8Fj6pZsZ77WDCZQSmv2aEN5a3NcpPCrxzuVovOO101L2gLralX2OL-HXDwlBe23JahQxPcCOCxPMgjcKsLE9az9xZV9Oyjo2ght7aG6AHUWr6Nyq77g~kV-M~d4pzhYzSpWV8-ye~~jU-CPjCvKvRT1Gzkb~VfXboHNL2-paE~30nx3AJ5G5OfEwZkdpukY-wzPbjR-jZF9QW-DQm8fE9Rp3aaCECX~rGgg4D6oenstkW~rM8w17w9FzWUmscPIcFx6WAZWK~VujwoqUddP7GbU7NAlOVk58ogrj7FeB2d-7NTl~uxHQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "4e66801d-95f2-4063-b841-07ab8ccf904b",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328Y",
                vendor: "Cineflex",
                duration: "03:51",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/4e66801d-95f2-4063-b841-07ab8ccf904b/preview/PIX_001_328Y_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNGU2NjgwMWQtOTVmMi00MDYzLWI4NDEtMDdhYjhjY2Y5MDRiL3ByZXZpZXcvUElYXzAwMV8zMjhZX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=ylisr7L75oySNXrI4I1EM4WfQxa1HCTuCJ99Ggb9vX~2-w7fLZBwkmq5UnW2FUIBv3aOYY~GI8pn4hq6KllwDfwkIn-9NrEKJgznZ0k72TRpU11aOHFQk9QMCrSYH9LoG8lUgYSL8zrO2LjpogNh4PEYsPLeczkl~eAFWbo~ZlmZEibIYw-seirP06L8q2e2KfsBbB1cOmh425f25dUEAgb5N3LJxfIccvOqAvh9zJPutq~TGzx0F9Cg1Rg27GRVy5UbciHR-k8nfCjkjA3~WSEX1enFm-gRLyZ88U5ByIR35pSe3lLbn6Ba-yWesrAv6503g8PikHkvEmxhbLmHxw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/4e66801d-95f2-4063-b841-07ab8ccf904b/preview/PIX_001_328Y_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNGU2NjgwMWQtOTVmMi00MDYzLWI4NDEtMDdhYjhjY2Y5MDRiL3ByZXZpZXcvUElYXzAwMV8zMjhZX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=PBVpJvuWc8dEmYrw-SrTLawrXJ5dCGdIi7IyWyq-TMbrqAmdsUv~gdyvHy~7ByhbnSLSvDRKys5FiyIU-7Rn7YXNuKYypgimb1gE8cheLZrncGotz~zZWxY18AAUEjbw6t2p1HbkxYA43jSQIZSVAAJ~p5W5oiJUKbeOUGLxmemr2jG~X17dgim13tNem8k4D70oTEgLUvxKBnlhucAjq-sKYEEe-ROBxXw66fS-J0cjCtQ9ps1V-BptvP1GTRNGHSP0vfMSFkvLxNd7P1uOUNdDy5UuZGVCeKQdCVYT8ILlBpeLrBjf4TwD4F3-okRlGa1q1vki~PFVRYycqjit3Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "aba37d7b-2be4-441d-9738-2d544112b2b0",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_321B",
                vendor: "Cineflex",
                duration: "03:51",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/aba37d7b-2be4-441d-9738-2d544112b2b0/preview/PIX_001_321B_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYWJhMzdkN2ItMmJlNC00NDFkLTk3MzgtMmQ1NDQxMTJiMmIwL3ByZXZpZXcvUElYXzAwMV8zMjFCX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=Ow3pDV-DQOX7Jiub6FeK8L3l~qzzOKDspQmhhNsFU6QI61VPYO10zQlz15dwl3oy42trcf6OWS6x15pve~zR3SOBzjR7-FXFiPw291i1LHTaJPoFm5lVhxiKSSHX2UXFKktBnKHtgTc~EEaUCkqV9z~q8yTzTCjUNC5PUN-0aagaTtP0SqBJcRUDcKsoO-KSZrjVeGGdrX332shVoyByBoFQudB2EGKIRBxLb87KMNVuubebu7hRXTU~YQ-LmPvw~9sGi4j3T7y9skxcifKTIYNmGFTx6m6PNno0OnCY0Z1u2a8kU~1-xYU8PZdgs08sYFHBYmQsczDZsuAhxNFQzg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/aba37d7b-2be4-441d-9738-2d544112b2b0/preview/PIX_001_321B_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYWJhMzdkN2ItMmJlNC00NDFkLTk3MzgtMmQ1NDQxMTJiMmIwL3ByZXZpZXcvUElYXzAwMV8zMjFCX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=eHu4MLDkhThnjkl00ckX5mY-zkEmfOx9uinUOpTJz9KcPc0rZykXgvPWfr~LB~ZiOxIfrzP1pyNmdKSUNFHEgOzbBLUrHTZZf2Im7DXUXAoW4XTOHdYzEdsijJgFI3r2Vie6c91kE4089yXOU8DxQzMJawC--HnHrrbOjP~OOXCOTlCTYHeoFSE2Y0sw448gYR2Y3vXS~d7g6fsTv1BMD6mABl7mSvnqCRGKB9U5~1Fsh9tQCYjpVZyrGg9lp0c-akWtCBG0Wnf6LGq3r2mvR2ttb1FIQZZVs0GgDYtv~suC0PDSNUTT7rSXs6GkWdELU-MuMx42q4d65vtcU4pdkQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "133c1f91-f98e-4186-b0de-9590adee07d3",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_330",
                vendor: "Cineflex",
                duration: "00:41",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/133c1f91-f98e-4186-b0de-9590adee07d3/preview/PIX_001_330_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMTMzYzFmOTEtZjk4ZS00MTg2LWIwZGUtOTU5MGFkZWUwN2QzL3ByZXZpZXcvUElYXzAwMV8zMzBfcmZzX3RodW0uMDAwMDA0MC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=q2hLx2g3kvn7c6YlA9cwIUjQ6tIcM69VvEJHdAudtK7-nRcs8oMF5KCm4ihmkoROLTbPRonlJaWcmtF~e6dnw~Id-puz839jH793bXUAyvAIf5ee0WQnRSQOjBD~~pp~svS2o2n5g3KsLYCHtabsJnFwEAOooUoxWtkpLvhg9h4~~EFz8-gm-zxPg0Dk--ePL2E3P7s6AlRSgDuLCBIvcQ1gZT4ONvN-3r3E9v9c47nBGHcNZOcVPD~CbzsTqgauDR67uHXhKQ5f2mbVWlKxIf7dDe-bVA6KqXuMBSyuEXrf0PQPXu5RFKBSkHggxMsoKLSfbyZarM3GUhN-urfQjA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/133c1f91-f98e-4186-b0de-9590adee07d3/preview/PIX_001_330_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMTMzYzFmOTEtZjk4ZS00MTg2LWIwZGUtOTU5MGFkZWUwN2QzL3ByZXZpZXcvUElYXzAwMV8zMzBfcmZzX3NuYXAuMDAwMDAwMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=qStx8~qWxR462KMNqqzYBHs8nNyBWG6mXI2ofYiNUAwaz2w2R4UjJb~DWW-RpmxHg22TlFf1CzBafDISy2cBBdDppVmG2SCya1uv6o7Ueas7Yafdj5y6yyjgi24qeWy80HrHOak6J5Tlil0f3LDeAq3RR5r2KXVYVgsOLwwLuVSHy09HXheWDndvqKpz4HvccYlQA7-NkHmUcoLhpZ~rEHsl3m2HUA4C9HVHzHT1NjycA-CGsvPWRt9ewjBMHTa8ASevA0-Zt3Yxsf5o9do09e2QXDESq-lQwTS8ZQBu2rnTfhoE8eIDfNPPf--1HCHA~NooAMPF1dw8vino2qri3A__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "6be2186a-66d3-42b8-badb-e870fe4b3192",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328D",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/6be2186a-66d3-42b8-badb-e870fe4b3192/preview/PIX_001_328D_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNmJlMjE4NmEtNjZkMy00MmI4LWJhZGItZTg3MGZlNGIzMTkyL3ByZXZpZXcvUElYXzAwMV8zMjhEX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=Xv5TaUgkF9-IpfdwPaXYaBHSND4iNCJQqxiYoYtNIkqZAkk-u7cX0jSVxWCjHZ8J5344TqsWpBUjyEvx1NyPPslPQn0TCUQJZZHUBQLuOhiPeytBpL8Nil4j6FDj2RTjvHInY2VDkIt4OB6hOzqaL4BdhP2G3gjUZ5C6i2k7e7L3wQ9KXfROKTTqeXHmPsiHn5Z3VNEuMwzoQUGosAr0Wr1yJAjptYLS1iCLiXmb0bYDPadOI1tMU650pqszbmeo8PrXbswHqZfjQb0884tv5Hj0BkVrzyuK3~Id3w7nLei5xOt6pp1gPb6kSgWhO0V7ED7Fanx2iqMKkMSnJ8zNfA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/6be2186a-66d3-42b8-badb-e870fe4b3192/preview/PIX_001_328D_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNmJlMjE4NmEtNjZkMy00MmI4LWJhZGItZTg3MGZlNGIzMTkyL3ByZXZpZXcvUElYXzAwMV8zMjhEX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=bMUKYsnJxIydse7~UtU2zmaAGOROp8mKQRfXEPeOZsApCTsAKL-RDs0DyoNQSxTZt91tgLrBGQOGwkoU82sQ3GSd7g06fYdtLc41757arqEVhpyVFU0jwG20xRouAASN3qCVsC8vc0-JgaN0mIXaf8itR1l4SBH-yfc-Z4cHaF1fsGx-piEIYQ0Ol~gusWe1phZ1otx~ghzCCGr3i~atOK~u-mNtUddH8Tv52c1ZYYad7~eXan0zCYFIzm6IvOjgokbfSl0YUUJMjNhHT6RpFh~9lZbhrb5DM1fTh0sUdy8KDcDpOHWZqh5dwjvLO3MsZOaiznyWLu-Q9n1yOMamMg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "fb486da9-fd72-4bf5-aa22-f8695fc7ad95",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_321",
                vendor: "Cineflex",
                duration: "00:10",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/fb486da9-fd72-4bf5-aa22-f8695fc7ad95/preview/PIX_001_321_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZmI0ODZkYTktZmQ3Mi00YmY1LWFhMjItZjg2OTVmYzdhZDk1L3ByZXZpZXcvUElYXzAwMV8zMjFfcmZzX3RodW0uMDAwMDAxMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=zpv2q9pNw4DnE8OEbBXkg6HJMrS3b8d5xgjrV8u3xjtr40TuCIp3vGUD79EIjduQ~hT9MhfodyVHl8L7fXb7s7oB45DpBpLBuQ7ya9fSWaiSTueai0eiLa2kkTcXPmKh4-rYfoqgIbaseEOR8Q6EqqbaoibuK548NxOtwiZW-JzHbgz6mtyrnsgek-UvgoFokdWvmr8hwuk9pjX2Y5vl8Zl-LhCELxS0l08sUk-iXXxvevZWaU0GZP6V5SejCGSWbhHQOW3hjga7oUwa3bEE9ltPMc~zGoa2BSegzXMuYVAtfk2GY3cXsxkanaHH8VlRu74T9vRoBOIQuZklTbcOdw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/fb486da9-fd72-4bf5-aa22-f8695fc7ad95/preview/PIX_001_321_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZmI0ODZkYTktZmQ3Mi00YmY1LWFhMjItZjg2OTVmYzdhZDk1L3ByZXZpZXcvUElYXzAwMV8zMjFfcmZzX3NuYXAuMDAwMDAwMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=XLxaNLQ4MJIzHmhPv5nRfUoL~d2TEclN0UpPy03jO5fn~R5ckoyUUwK3b9x~hZoTy5QrLT5WoAQOJ8XcrpGpezwELBo-uvWf~wlC5koSvdmJTUuptOXY~phTyWBYocTRvuqiU5H8j2br4yrV9RWKrd6QN5v0hXEFYCmkgDgLgOQuILqoi4uPEUTdPZaz5QPCifaOnY6Akg6nyA6ovdpIQVhEmuSeSeuP7xUPAQMxjfBO2RlmOtxEx63trVj76TlSISr7f0w5SBaDLvyE953Jhn1tJ79M5OaxY07b1q9rR-6uWR07~wA6vkFGFjVJkQtcdcnFnqc5MPIJBmEErGeH0Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "9b97f338-c986-4ee9-ae68-72dbd3f3dc07",
                aircraftModel: "FB200",
                date: "2017-09-13",
                fireName: "Black Hill Tomalpin Fire",
                fileName: "PIX_001_332P",
                vendor: "Cineflex",
                duration: "00:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/9b97f338-c986-4ee9-ae68-72dbd3f3dc07/preview/PIX_001_332P_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOWI5N2YzMzgtYzk4Ni00ZWU5LWFlNjgtNzJkYmQzZjNkYzA3L3ByZXZpZXcvUElYXzAwMV8zMzJQX3Jmc190aHVtLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=hcq5AyKI1EQMTnF~szDW3eyiLAjqoOGVaXmiLg51gvNHMLPy-ayBV6FoqWlhkC3tbmoeL7FuPDzcc~uRI791MQ9hWRqIu1qC9G5HAxdx-ACC3dh~g2Hp0ojWV9l5Xr7S5WvQHpxhMaApibDS109WLSvR6h0uXhbaUYbe0lH2-aAS6HuR7V-T-ojuLC-BhpTb-oGwG5jcOe~ydbsDa0G7CYdGkc8nQumAKHacXDXiE-wNGbeyH-nA2blsuCi7QB1Ds~7LNHzxxWmGNXvXzdkEdyLJ6SQf4RtSvl2TkwerchQUwvUk1cnkrwKtLk~jSMcV7zm7ZmpC3HT8eGQuod2T0Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/9b97f338-c986-4ee9-ae68-72dbd3f3dc07/preview/PIX_001_332P_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOWI5N2YzMzgtYzk4Ni00ZWU5LWFlNjgtNzJkYmQzZjNkYzA3L3ByZXZpZXcvUElYXzAwMV8zMzJQX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=fxArozM1Akv65U1eGKAuQ~Z6DfsijGlZ6f~l3PXXMM-U8SXECYUYAYPGAzlKVVBsHYa6-j5i0P3daI1xuJ4l6yB1bStF9hUi1uGEii8BSnXxk9mIlVBD7OKRGhE0a5-WU~gHz1GGwvIITZ5QsDw6JO90KJ9z9nQMxEYJP~h4ssL6tYy4dE3ivq9FWgDCjHHmaIuC8TU10PasFo1etIqQke3Df9UkO2Ou-k6mZfe2bsKRJdWE0A7QLJfuchzHozKKDNo6V5YI5mqkou7e67~7daEObMo~EQpbYFd-59g7rj3MvrT6GR3ZlWjJaVg0RQVyXYNpWZ1QF7yRWA9aPztRDw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "22bfa2df-5fe8-4eb8-9ddb-55dcb014d009",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328A",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/22bfa2df-5fe8-4eb8-9ddb-55dcb014d009/preview/PIX_001_328A_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMjJiZmEyZGYtNWZlOC00ZWI4LTlkZGItNTVkY2IwMTRkMDA5L3ByZXZpZXcvUElYXzAwMV8zMjhBX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=bkWIC1ZI83crnREQga5br66t47yIX4Zs88ANNGJ2vlEL6W1wQPLtsJGur3UlEKEcN8EIlagHE8aKzQRozZFOVi0mEG7RT9s~RAT7IV2zbOdlBJzdgJzZvYceHtgDzkCsl9vB~awzWardqLYihT7n0PPvdWomBGA2Q~YasSxGWxGB2NJJjDqqeuyZn8XsaSPTGyAmBvCuKydDFw6~gVfFtuaup-Ge36t6jDuqwblCJW4bWxkIAeo25sWyFUgu7eTG5O13R6fd-yGjHPyoqmLHtOgA96xyg0e6jWV3mSb09sWMq0OPZOIdnYtoJGaCTKZ2~N3zYvreWmyyBf~MBp71JQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/22bfa2df-5fe8-4eb8-9ddb-55dcb014d009/preview/PIX_001_328A_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMjJiZmEyZGYtNWZlOC00ZWI4LTlkZGItNTVkY2IwMTRkMDA5L3ByZXZpZXcvUElYXzAwMV8zMjhBX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=gf5ueG4p4yIzSNKI~TM37EX-PGN2SRbBNTCeVgyCTR5E6z45PLSh37k0NAJtTsKPFp11Gv1Zxgs~qg~pqf~UOwRW4Uj0phKt9LHC3dSZa9QT8L5P~NVjSl2HjXlC9LrwNIIZRsjdftzdt79ai8kGRTmmca6lAKjTjcJ5v1H451QQ2IsB3Ehhg~GnVok8IdH9zmlc29NeFjjEEvc9Ca2Mbc5yDE7v85U0Tvz78wzRt11TF4AVQsTDFwuJXq15UP4iMAQxIgTXQF5eh4wvE5NuDlcXViBcxRSMRnCysk7f0Zm9ChUyBIsLLUivTXfI-t6NPnonzPJNbt~UMcs1LPeDoA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "0d77a66d-5e66-4b48-8c4b-ac43b40157fd",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328U",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/0d77a66d-5e66-4b48-8c4b-ac43b40157fd/preview/PIX_001_328U_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMGQ3N2E2NmQtNWU2Ni00YjQ4LThjNGItYWM0M2I0MDE1N2ZkL3ByZXZpZXcvUElYXzAwMV8zMjhVX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=eMgt11VcQ1JJEgvwjVF~eu4qA5ReXZ1wX17LnWJS2~DBRMIo1GFhQfnfXj-nOPqRlI8Car73glcRXx0PXweHZBr0B7XTM~fFCpe9hcr9h-e-noY66qkOctIuqznokKiXkRSSc1Dbg4JbvdbJMAZbmRsqhHCQFjrYzr8LRj0lidflKYllTjKkFKm4OZ3y539l0dOLEdwNqWRKGYhU0IdcO6-zg6CFDGRoZkJff3-3HWMvV51PQ6iNYXpN6XQu6Ce4RnbfAewBwqJAX8-ixZsR~SfEmBydxoebX9NuvRqU7yssfs--T~oN-dSkyqUrL0gMXh1857Uz8-5-1YEsmli6fQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/0d77a66d-5e66-4b48-8c4b-ac43b40157fd/preview/PIX_001_328U_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMGQ3N2E2NmQtNWU2Ni00YjQ4LThjNGItYWM0M2I0MDE1N2ZkL3ByZXZpZXcvUElYXzAwMV8zMjhVX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=qHUmGw1u2NxUDU86VN6oAePwmHiiajIGHp73DL56S3CPDIG-4THblBszb2vyOYEl-p4je5sePUDeSB0VEywuVdnTqYC8cUpJFmzq5S7Od23ofofeTdt68xXwD3DFAx77~U4CbOlhNmGAC-qDWO0VnDhtQoA-p52XA~D0LnLEP-Cw99hIfiSrJSr7QXQxpkF9GmlV7XmFxylkFhhMGJ25u92aG97J5Z4s~KydpnFdFIpMlqjcHbjJvTis80M5CZanGsEkH5MZ9yx58XsRVwlY0N2sue~V3KpBXpqEIs3GGrY2lievZXrsna~y~ex5XDmfSWdjBwe84XhhyOSaiuMyRw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "0feb7152-4773-46aa-9603-b836376a5372",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328N",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/0feb7152-4773-46aa-9603-b836376a5372/preview/PIX_001_328N_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMGZlYjcxNTItNDc3My00NmFhLTk2MDMtYjgzNjM3NmE1MzcyL3ByZXZpZXcvUElYXzAwMV8zMjhOX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=L32r6-l5rA7GOWbkkJjf1OjPswGHqUtNuuMj5t1aFX3SGyxUI8PXNVsP03Au4Ps40DmvlAESNyBndYPwoMDQMl-oADCZ05QVXw5qU9gzsFZpbf0CXGiFVKoz4F5dz9rMItk9DRpvmjSHkUGatO~wcto0CB2HAJ4MZAvSp01bvmRr4CmKw6LgWOTjEZR0sAdX39HACva1Pfi9mwKFlOYWBqYCknjg~CsUV2QBi6VlvewpCjdB2GiG39yZGeTi3pwhn0Ta2LzEYzu0SKPY-H3tnIWYjdvIMHcXEVuXMACl55Zn8lxUkeC83Cat82gyqiqJUP5QSJnvX2O-3gQQvPzeRQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/0feb7152-4773-46aa-9603-b836376a5372/preview/PIX_001_328N_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMGZlYjcxNTItNDc3My00NmFhLTk2MDMtYjgzNjM3NmE1MzcyL3ByZXZpZXcvUElYXzAwMV8zMjhOX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=xEeGjlmiQgVQooHgjx1JUF9AE3fsJVv~GhWwg9ScdckeXNuqRgHxDJVea1CChZiC~kA35LkLzIBq29Qga5zxf1Qsht3FIcjWg6tHb0SSvebNixWXMrvxUqIP7Mf4Tfv9j9zTQQQqXJ25dOEADDdvNdIP2arCWf3fyPgjfBrPakY5WK~KJyaarIg1IYFj1y1l8O0cdb5wSoc8s-FckJ~3reX-i-oWcGVIWFHN-PMmfs-Uwe2SVDJer7H71FTUBcqfMOXGGjsh2~ye63szyCviHhvc3wWL9qGQmQWrVMisxgAa6YJulwkYGGnsSIR6X49KIA-M7RFyHo3hEQA2gOMEwQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "6f74a49d-a3e2-4d5d-9b93-f260b8e705f8",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328H",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/6f74a49d-a3e2-4d5d-9b93-f260b8e705f8/preview/PIX_001_328H_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNmY3NGE0OWQtYTNlMi00ZDVkLTliOTMtZjI2MGI4ZTcwNWY4L3ByZXZpZXcvUElYXzAwMV8zMjhIX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=v6T92Sz7CZRZxptbB4U3RccMfWb12F~S0azDZTkRJUiUyLuoJSjd4hc~N-z1rAnIMtEZbwwote1VMnoNSdDqcMfeXCK3H7M~KBRfMmSA26G9Y2fsAglh5yyo~qlqA-fqE7S7PtOCZKrVKL94c~JUmCSk5IWENc0TWPsh0Od1h4D0PEUQoMm3Sx7MCCV-UuYW5H5fVN~9Ol08GRDLlu~oDDcZbaLbhz95ik4zp8kc79XQ98ubYic0lWQC4ll8Ve544KmvZKMjmQZVoPWMIRUqmssvAQPEU3PV-S7qX6oyk-Los-Hgzb5pPOpvHMO34uLJ2MyIbnQG5xEamhhBBui6tQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/6f74a49d-a3e2-4d5d-9b93-f260b8e705f8/preview/PIX_001_328H_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNmY3NGE0OWQtYTNlMi00ZDVkLTliOTMtZjI2MGI4ZTcwNWY4L3ByZXZpZXcvUElYXzAwMV8zMjhIX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=C-AwzoHsrkyzpy~N9-CccxTHiIUQxV23yj3fJ5GAaTixLBqNREbwz5giIeHPsPr2EbYXdUEPTE2smMpgUKwc4Yt~R0w2FaxsJb57Rtc-fNnFa3MUu4T1z8IjrHVV43aFLkKtl4bqAgkTW7vPBOATfUoucoSpaPM10q0an5EPAgAsKbe6jez3d76sRESQOAtAABpirZF97U5K1HPb9~Rgr-LDVMRKEOTfyqWEBXV46IHFoqIC31o8wUh3O1WqCvpyapAmGLZhFAffQAHBjProxO48H~wiYfncqKsERrcWJ~FIxJ6O2gFFtl4IXojN111qNJ8y7Ta6xhM-HFiV6pF3Yg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "b0e28ea0-0ce3-4889-a8da-3d78f4bd3f99",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329I",
                vendor: "Cineflex",
                duration: "01:17",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/b0e28ea0-0ce3-4889-a8da-3d78f4bd3f99/preview/PIX_001_329I_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYjBlMjhlYTAtMGNlMy00ODg5LWE4ZGEtM2Q3OGY0YmQzZjk5L3ByZXZpZXcvUElYXzAwMV8zMjlJX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=RcyXoS8eoiMeKhGwj-B4HtHDmPQCbzyMCj2Iz5CTlTqCLBRsruhdp56e6SgSDxeT4ktSQ8X2Dx4T9Nnguz4hz231sBuS2Tl3RnsuSTY4WUqtNwKIcuyptYtcK3JgrgLKuDN95FfsmT3rKxETYhdaDOSg9ALWc32--B0yTtFBcF8SS-eiZywcctEA-viovH1KSh9OBWUnACzjYmZIW1hpRRqCltRq~Vb7GmFAXWNmBKlK3u84C24rJBVo7Y7Tb5cyj~-PYe5U7t8E59KaYuEsZHxovLkZucYsHa8QFI43VhGVbeeNHbWTHWHfa2PmBNmVUkleJBF4H5o2IhVDhfdJHw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/b0e28ea0-0ce3-4889-a8da-3d78f4bd3f99/preview/PIX_001_329I_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYjBlMjhlYTAtMGNlMy00ODg5LWE4ZGEtM2Q3OGY0YmQzZjk5L3ByZXZpZXcvUElYXzAwMV8zMjlJX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=mYaI9tra-6uOnxarmpSXMTzLXHw9uUJOfUqa5HQ2YBjZNEzVxUW9oVn~0X3r-lSBeNV-dLW1-UZwclJvlcTHvB4Qfl7NeQhvWywe9K1Dh0BEzdjndWUzsmxilt36h~gCxVPZhFiqG8DYE62VOgaPlue9MbWvUnJYA4r-I-UQj-gvE4NSfVuFSPjMdqaNqY7iCV-TYV4BgGwagEbG6VhwucdjXvZLxblh0UkPyA2CvVXre0-qXEVHoUyavEFkfE-YjUubVsqNUafbRT-oh-ZwtrGo1919fDIDyCKfkALjqIaU2l7VRHgGbkrnaJypIIdVkn4X8DNwtQ7bn65VSyA--Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "8686d970-a8cc-4dbb-a117-c94f648423e3",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_322A",
                vendor: "Cineflex",
                duration: "00:04",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/8686d970-a8cc-4dbb-a117-c94f648423e3/preview/PIX_001_322A_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODY4NmQ5NzAtYThjYy00ZGJiLWExMTctYzk0ZjY0ODQyM2UzL3ByZXZpZXcvUElYXzAwMV8zMjJBX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=OIwmHTtICtk5cX8bCiDs1iFrVi1~UW-5Jijvlqrq7mUMJNQO~O9-zjP8nmFNKJPTKTynrF1PXeoGO-rqb8-bKbx1TQEBgvrN57ltcE0RhXyzBXyLvmbXgIM15EG1cG68Ze255aZlKft1UfChnsbz1UGN4dPyZRY8Ph43fI1JWAY3vZiZzfyYIzEGem3i0eqA2Va9Vmsokgt8vbr9s1OaoBi1Xg~qy9Ve8hZhJT1vMiAHJjgNdC3EthyUFTvwMHOMW6xP-U3VUFoXNHr~zjOTfGiFh8yMxcYUSLDm3E2BVfTJt22-IH9Aivlg5yvNvpm1X~9Re1wFKvJXsJ~BmkDtCg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/8686d970-a8cc-4dbb-a117-c94f648423e3/preview/PIX_001_322A_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODY4NmQ5NzAtYThjYy00ZGJiLWExMTctYzk0ZjY0ODQyM2UzL3ByZXZpZXcvUElYXzAwMV8zMjJBX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=qHvZiGyTpnM1r5GQIzEL5q-w3t~FTPI9ODvObpqNT-YhbcrZQCPlR2N3e0oX7cLVp~IMiPCdak3LJMOOcWHuj~iL1itMt1lC143Ahl3cY6IW2bl9clcZUji8RPckqV5G0KI42caxqs-n0MLMBAbtqIQXcHaTtHvNuzCPltJo9cgDzkvQNO~3OzAvvFeNlyQecmkvehya0hE5U0PEwEEFHeGxz7P9X0J~YlYAkb1Uy7hVVea5RUuDmvvpqEpizU5W0J56BKOvCMaaurffvhbzTvHoc8oUczeq4BOrs6kQGHYaI~1QatqYMXlBR~INwMSQh6yfl-NCxAerNixAUG2g8g__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "eeef148e-2d13-49bc-9fb7-9742805e0390",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329Z",
                vendor: "Cineflex",
                duration: "00:03",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/eeef148e-2d13-49bc-9fb7-9742805e0390/preview/PIX_001_329Z_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZWVlZjE0OGUtMmQxMy00OWJjLTlmYjctOTc0MjgwNWUwMzkwL3ByZXZpZXcvUElYXzAwMV8zMjlaX3Jmc190aHVtLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=Id0N~uFUjBnEk0DrTmrV08u20DexA-7u45a2DdrNCSyLnvzs~LJUcD44I0z6XhQTTbbFG~EQq~bbA0ZuIv4iKrIjtIdO3LjkpSmlgQIspvGWSI9xiT5H1hQ42NYxhrAzgL7LRef8w9ADk8guc1SqsOMag6~pJC~GokME86mtVdgLW5ZGNw-68YCuEV8I7tf5Yh1nUu1fMNfN~Ram7wmO8bLb6eYVFEZrb-GhlGy-ibRQSDMncpGJxXrAo5yNxfukbA3i6nWc~R4WDqXihUoVXmG9Gr69jXsWmLfLnJMTWSPaUMI53LNaJSMHq6HMjHy1LtERT8AWuGveaHJYiaCOxQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/eeef148e-2d13-49bc-9fb7-9742805e0390/preview/PIX_001_329Z_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvZWVlZjE0OGUtMmQxMy00OWJjLTlmYjctOTc0MjgwNWUwMzkwL3ByZXZpZXcvUElYXzAwMV8zMjlaX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=qnVuUwptISxARRC8G335Bj-hyZBnFzvPLcjEwkmceSEk8vxheIQuJ2QNxitgH7SCWHsocPUGUEvQiMvuZBIoyq51Q3fTpClqt05HRafdxivRoo6ak-6m6Sw45RZGzvoKJx6e5VOMIETkPLZ0osIteXvFDHv8oKYuvX~9aJc12tHcVgYk620ZZn8KguR74FqH9QA3fOyJsT-351Sazf8NAqNq2PYZEwpn8msbvZf44rZv57LNvTbqUycMlpGb26XOpcZQk~2v3QJx74WG6k5OubHxXG2S54ZdypQp54hvI2RgCdZCW4n68zt17aUVk61-y-mWLHXM4VX~Ke6H1icd~Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "96821ac1-a81d-4cdc-a6b8-a5ac0fcfbee3",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_321A",
                vendor: "Cineflex",
                duration: "01:45",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/96821ac1-a81d-4cdc-a6b8-a5ac0fcfbee3/preview/PIX_001_321A_rfs_thum.0000019.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTY4MjFhYzEtYTgxZC00Y2RjLWE2YjgtYTVhYzBmY2ZiZWUzL3ByZXZpZXcvUElYXzAwMV8zMjFBX3Jmc190aHVtLjAwMDAwMTkuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=MiZELBDFolhstbzJbvqmDBcC4spRpy0pMKGYvdlqKeFBqzZKH8t6~XTlFS~ATsjjhRY1GAHcvocoPr~fZXzUE9PyYdHR6uYxpGerajZ6pfPpw2vGutbqaFXZDCh8UiFpXGbzgd13iY-AC25z0knSRjYTeCu5EMgncqw1kW8Iab6Av1igBbPAZ97es9PfQbWbtZ~qfmLvOcj2nowbDhPpkHacGnuMevRuFQ2kKfbc-W3BUxF-iPUbNEW97GJhG-DUhl65sEok-Leu2SjN0DaGZuSLpA6JWRtDw9A2f1Y0tj8~VijjOUQ~T~fx6DhaGZb5nH9KexOI9B9hT6mohkbUTQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/96821ac1-a81d-4cdc-a6b8-a5ac0fcfbee3/preview/PIX_001_321A_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTY4MjFhYzEtYTgxZC00Y2RjLWE2YjgtYTVhYzBmY2ZiZWUzL3ByZXZpZXcvUElYXzAwMV8zMjFBX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=CRR~WZCk8ZF32qxGVKljTjJ30qmS2Lqf-6bDdhum8Y6p0D49bYqIPPS8MPvmzO7D9-D99YJbSCJvBMOvT~JB9Gi-elb8fQ~~jBmdqShRFtZ15MhFqFsUCJ1a~hnraAywdWEY4HScWsYJs8gkyASQ9L9kbXc6dODGsQ5yWokBHRhb1D5Bq9tS4-Tr8ucF~xJYZybrqUuOdmhSq4yVHnh9vXfEbMRvwZFQlnY2aBpYWMS28oT4wO9pF5JqDkmh00hQPMvZDXyje3n~4CEjoG1E9hWr7GWC4UwKl6d1qJdEiSkyN0K6kJehM2noFFaxP9kNsUwdjSVqIjsqWO8tB9A-9g__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "1fdcb2e6-fc0f-420f-883a-f414b013dd46",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_330O",
                vendor: "Cineflex",
                duration: "00:02",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/1fdcb2e6-fc0f-420f-883a-f414b013dd46/preview/PIX_001_330O_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMWZkY2IyZTYtZmMwZi00MjBmLTg4M2EtZjQxNGIwMTNkZDQ2L3ByZXZpZXcvUElYXzAwMV8zMzBPX3Jmc190aHVtLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=ANE~4AesvZW-xkl6xBawoLHJDT8-6u8A2TZoUIZfKyZ095U8ZHIlhgS69FpGWhPK4yQJr1sS07JVLxH7sBjljNG7DMXID2KBnYdST4iLdYWlKbBhzgMlelH64pZNSYuIZWmP95IzJV2UAgIbtjfEZSKTbNjQysac3aUkTqIoxFUu-nmp7GKRY~mnJpBAzrRyu55DqrXN9eqogph0Wym~AdRXUjzb4ju-fYmyzpkBSodmgIzomkkCQoSWoQdCrssVx8Amiwx1DaqUcKNhHhEZ6kIGWMd5ltZyWNQdApIfG79vsbFNLCa820eTEGG0FOwyzNxPRNmC-wRzsLgqkVuomQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/1fdcb2e6-fc0f-420f-883a-f414b013dd46/preview/PIX_001_330O_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMWZkY2IyZTYtZmMwZi00MjBmLTg4M2EtZjQxNGIwMTNkZDQ2L3ByZXZpZXcvUElYXzAwMV8zMzBPX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=Y8H1ULdleJw~EY51FT1umghCaDzpBtsw8sYjuLF~-lynf5uq3xVIGpf0d4vBDR-Duz6d7WRhz2SsMt4G7-g-jBtArJEh5iJKee1-OBcTlazNfujJx6qYPqs2Sp1ThWuxkBOjttKFH63wj~XwSFXI4X5MfHtVoqVO6sPIqENQ~Ji4uh5e7RoIFKM4MMVV5rmtD9maSrC7umrXVAwuEf0RQJ-GstxjKMNEkbGw9yWP4d3WT-4vc8kjJ4OxGsC7kfwqdCNn17Fvhjdd1bLN1QmvZnaS-6mzWUMIxBAG2ldgvVsbM~4Krw~jl-aO4JuGfmNFXvrKkO171zRq0ishHDNjhg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "3fc681a2-e3c2-4070-b4f3-a220baac459b",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328AC",
                vendor: "Cineflex",
                duration: "00:04",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/3fc681a2-e3c2-4070-b4f3-a220baac459b/preview/PIX_001_328AC_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvM2ZjNjgxYTItZTNjMi00MDcwLWI0ZjMtYTIyMGJhYWM0NTliL3ByZXZpZXcvUElYXzAwMV8zMjhBQ19yZnNfdGh1bS4wMDAwMDAwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=qqg6oCf5xkARq20puNE2G7KpvOwZPUg0qqloKN5-dnfUvUd~axZTr0DUCTcQBkINYU4GE1~Wt5dlcg-e77-T1OSQlT-YAOzjqEZU5hZsRTS1qWHwAMUOQ8~ecp8pq-GZz7i6v83aP-OHqEqranu8~ReUGo8i4aNZDmxRlGcmh9GYcFP3epkQcco~fXEaU-Gs0sDAHSvjxH5HIJVgPOJSpXa9sZfYaz6HPmKzwyWeGLFalkMiUgtuU1~wEOsXB3fNGt8BldCXeCWGUfbxGaz0w4v8sbB~-kfXC64nz0fOiP~cNhO6nUJFxckxwypm39PIevks5iHP21UrWGai1keCRQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/3fc681a2-e3c2-4070-b4f3-a220baac459b/preview/PIX_001_328AC_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvM2ZjNjgxYTItZTNjMi00MDcwLWI0ZjMtYTIyMGJhYWM0NTliL3ByZXZpZXcvUElYXzAwMV8zMjhBQ19yZnNfc25hcC4wMDAwMDAwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=TUV5iktohbbWpM7wd6cEYmxg6NmTkrV1tunw-pwtkRMqOErCIHBK0VX1NJYDd9fTJcyJepoehsTlgYnJ6sap4K30G4-d8nBdMwY7KS5hPQS4f1apQe6PWdJlmHfRK7lxGKT-PXs9qSWcdauMuQikpMLke~M6aRweV0SzEjdvHF09C-S1HPY2oXjJDd64qiuYgBBZNRUGx6mD4qlroBcwtVwxJAudNfXsw7YHTwlvJ-Wr8bD5Nf85OHtCdv5pUitkv7as-Gg6Es4CegfZ~5wA-sc9W0Rqm9xblOkxvKeKMjdgEkAFDKUg-0uRtMrk4bpGJtnC~8O-Fs9dWX4eT6VkIg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "af3276f7-bfdb-4aad-a701-7be78e157930",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_331C",
                vendor: "Cineflex",
                duration: "00:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/af3276f7-bfdb-4aad-a701-7be78e157930/preview/PIX_001_331C_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYWYzMjc2ZjctYmZkYi00YWFkLWE3MDEtN2JlNzhlMTU3OTMwL3ByZXZpZXcvUElYXzAwMV8zMzFDX3Jmc190aHVtLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=ydB5nCmx-jQEIxLwW9R-8nwLp2VCBfPxI4bhBZmGHW756g3smxN740zTZMZVnIvcT7lst5G6ktYwvaKTXj-qPGe0fpfXu1bGG6DpcIK9fJnCu8coYkuEoENiObExj-c4JrGP~QrKkef9X3jZfJd3uo34iATB~AdJnxbazGAD8U-etU9oc3-fnJwkKyaxa7Q9oUwysbbnxCApw3wmTuTcYwzzx9kvTMh9Z8MJWcDH07hh9oie84LrouMIQFqs0Zg7fztBpZKMYkk4CqID68~fG8ymcdkSKSO54~jxR9g0ycGc8uL-366x3ovdAohMxfJaDHFESW3btcAsmatwjd-A1g__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/af3276f7-bfdb-4aad-a701-7be78e157930/preview/PIX_001_331C_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYWYzMjc2ZjctYmZkYi00YWFkLWE3MDEtN2JlNzhlMTU3OTMwL3ByZXZpZXcvUElYXzAwMV8zMzFDX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=YLx4cZzYMIX1753I0LnwNFlGh80sbJ2~aenjJ66HBl-4oLGYA3Mk6O-sB0s3jlISH~tLyIGWVIOKAnJhXpyh8XTcaj9UZEI8cPT199atRFoxqUKxfUKaeFy479H8MAykKtAdh234sLbq7xxecyKVVJ48IxjeHLDLNPFmUhjUCsN4c-RcRg7is7T9OiKSugsMgAx7X-Gj80nnLqcF~FPs8-Fem2aOJ7fM~7cAPflBeriK-2p89C74NHH-qq5UXmaeTLkO4E6v-VoNCmXsis6OOeQ5coumr-Q93G3IrwCgf6dzKtPW7tYwx6MSy~rArl~CER5ZB2Km13fVzUteJEkJAg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "04cd18c0-05cc-41f9-818a-ddeae5665b7c",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328W",
                vendor: "Cineflex",
                duration: "00:04",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/04cd18c0-05cc-41f9-818a-ddeae5665b7c/preview/PIX_001_328W_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMDRjZDE4YzAtMDVjYy00MWY5LTgxOGEtZGRlYWU1NjY1YjdjL3ByZXZpZXcvUElYXzAwMV8zMjhXX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=oFCZerg2ZmKrJSYVfuJH-7MDOE4d6Zl86bzn7JX71IosZ00nnH40bOyEIsJ1s8sFst1WZ4JnI1PrszB6PqzLoip537OK2mY9d0SRjJ7coatxcENg-gMHMGB43uWUZ9PfeUxbuJtxbWtBDU1TiKGS~kOjHamSZSuA4QdrUmA55NTVDF5D3xy23E-ezzsjNzJxztUfcpi9AM7QZv1GBJvY~bg1iiRrVhid6rpDeqV9RXdJRykqY993JfPYPIzhIQ82bWiWYr9~zJOq6jeUv6i5d65lB54YfWTkRSfpUc6no6X8b0Pq9v9rSgvdlztx6yXsGgOQMCVdagWGAklFUKRRNg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/04cd18c0-05cc-41f9-818a-ddeae5665b7c/preview/PIX_001_328W_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMDRjZDE4YzAtMDVjYy00MWY5LTgxOGEtZGRlYWU1NjY1YjdjL3ByZXZpZXcvUElYXzAwMV8zMjhXX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=eHtYfSgVWtu29CHyPa2sosod5v3S03PL3uRrayqoPJpAvPtWqTH2JXMAUGYYhhVpZ7mQQfD5oCasMgtX3-1hRsDEseQNUwTvrlhkY6Jg2K7A-EymkJi~7QTAE8I-E1VUaxsMmxIb8c0BEzMZRP4p6EjqzFrwcPwDRtos93tjyP2iyodoPu15bi4ZgMuE0q3wngZqfQNjaOVNmEH8CzR7NQfx1wgSSHlF91ASWuYCHTRg0xJltwlNGCKb3TvgZ1Y24n-CYoWmTfwU-aObd8ytS~yalrBD0~41ykehA19sz3t5ld87iz2jwFsFVCyIPSq9-V~SD4L4PAU9GH31tsk3wQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "6359525a-301f-40b0-9576-5b8e10d842f8",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_322D",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/6359525a-301f-40b0-9576-5b8e10d842f8/preview/PIX_001_322D_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNjM1OTUyNWEtMzAxZi00MGIwLTk1NzYtNWI4ZTEwZDg0MmY4L3ByZXZpZXcvUElYXzAwMV8zMjJEX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=j3bw0yGlQCqKFCp0ryWB2AY3Iy2ZhIurpTTZE-x9H71VjJacDIAyFq4zSG7iHLLIbby-YszWPoqya-cjF-oiRvI02DmvZfbpgebPIj08FL33DH7VOdWa4FvrryEsYqtdN5CSQ6GJikLKiTsVpvrBtRUpbpCbgXcyJry4kFG2d5oW34harWt1Rm9K~HqYURVrKd~44Mmfd2Tm7LQ4wc2xbgQsIYzjfd8bPhZvcxq6tp61SWKVVsb4xOu7Cfu0q6OWsNy63iT9xZkroQlgjRznLubLVo5oCg-occcA~TnaNwVIeF1zBfk9T1lWlDmpaicbW0gvX1YXJo07aXKSsry3BA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/6359525a-301f-40b0-9576-5b8e10d842f8/preview/PIX_001_322D_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNjM1OTUyNWEtMzAxZi00MGIwLTk1NzYtNWI4ZTEwZDg0MmY4L3ByZXZpZXcvUElYXzAwMV8zMjJEX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=i4VjKCunBIy2waaHzAEOzbSEygFYD0e7x4WvGNBaxFLPWfb8U1zTJTXeKqV8ATkpx78N1qE4QZkr~nR71VXWmP8t5M1ZK-nmsxHfAHNd2uX4nafeY2nFK4F~b~GwLAh3Ryx96CnhN8-k7tABKKaT~~~bJ4~A92tnZDk4dyGM8EM4jNJrPwgor4-Kkzx4s-GqRg-Gy21JgnqrVrsSwwBiZZHxLQdrUQkU8UvxLJ6NbcapfVTQjKyoQlaDdq0ODjrqfYmhXzknMeUkUIw7m3QPZUWmwPGYwJJDB-QNYuW2I68QW5f5gr7046smKXBAZ2RhqxWvgKGnwIHveLua0sCzhA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "8a5cdb5f-4673-4588-9535-80ae0f168caf",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329AB",
                vendor: "Cineflex",
                duration: "00:16",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/8a5cdb5f-4673-4588-9535-80ae0f168caf/preview/PIX_001_329AB_rfs_thum.0000030.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOGE1Y2RiNWYtNDY3My00NTg4LTk1MzUtODBhZTBmMTY4Y2FmL3ByZXZpZXcvUElYXzAwMV8zMjlBQl9yZnNfdGh1bS4wMDAwMDMwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=k3ZQ2qWR5WOiRutaDx6wfciRymqlSOBkqAh0G2CcgWSfNBelV~muADzZjM2GeDJNr~NTPEAtp4FF94-Xi4N5Jkh1OOzgQXBZFpLEDp9N~EIgKNFECR2XaydaFYKrJDUQjI0~7gYWWJFaBY4BCht3onfqIdmRVWqDfg8xlBBHOnP2q4J9-D~CJgTATtdAmPVYLJLFssp3MHOjyjXaUeBQo4d5eaMi7frWwwFqxvVuHSTfxQccjYI55qExGLGyz~nJV5KtM5BPTXD~FXRCg1HL7WonAWOCIippz-hgQqBX~5NfSNHR-4G9COSeO-YFMTureDogFg7qUzMW-Sb~FjfmNQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/8a5cdb5f-4673-4588-9535-80ae0f168caf/preview/PIX_001_329AB_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOGE1Y2RiNWYtNDY3My00NTg4LTk1MzUtODBhZTBmMTY4Y2FmL3ByZXZpZXcvUElYXzAwMV8zMjlBQl9yZnNfc25hcC4wMDAwMDAwLmpwZyIsICJDb25kaXRpb24iOiB7IkRhdGVMZXNzVGhhbiI6IHsiQVdTOkVwb2NoVGltZSI6IDE1NDQ5OTk4Mjl9fX1dfQ__&Signature=VN10VXYlktEsP3TafQnX3GLx-kRcfImqwzElvbshPkjqrKmqBQSmGf4oRaoyqg463~DPeHENOH4a~mBqs50iPCDIKFJoPo~a-mUHkuweydFXNIf2jTvOU~Z2jYIxnX5-bHSRGlrE3RLCc8dyRacU87EsBkTbc5YXv3cc21lkh8EZqW5s~8ZExucyM6xYsDTnH9n9zlTzAOb6hrMRCthxpGw~lLNS~EbAXAcBNQ5D3dyBKPSPZEXzdYOf2Dhya7u2tUosln~3u8VR0etZXTavHI72B2frYNpkBwiPNrorOEDTY3W7UN9OflrnkseXRmRVUq3Z9nWaq~tXR3lwdYuP0A__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "cf1be611-ce0f-4aa3-b8c7-e5a6e96df9b0",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329F",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/cf1be611-ce0f-4aa3-b8c7-e5a6e96df9b0/preview/PIX_001_329F_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvY2YxYmU2MTEtY2UwZi00YWEzLWI4YzctZTVhNmU5NmRmOWIwL3ByZXZpZXcvUElYXzAwMV8zMjlGX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=w5Hy4z1TzFJb4pJSuOCN5zBWiSxnGYB2aQDmH~f9dE1luyOtvaQ9sYwchf3nF415fUDtY~rwQxcE30KeVfLKQ1sZsVg72xarKu4shakmgvLIi4bvf8W4Yjcz~g8FOwRwqYXS1okX7u0v~YOzyKqitIfpUp9zQOubBLhI4OvZTwM2KFcQ~O6M85f0MH48cdOvfh67g3fQJ3OEs6q3ogRFjxi-ks2aO2ybkLSjVT~VAqB~AKp7QsO9D0X3DqnPG6Lc60wyih8jdeoCcbDBA~LXQEMNZyp0qbLJYDP01aHURCwhN-FhxI4WkjrZSegDA8T4pPblieEB5ZVeZXTOciuVpQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/cf1be611-ce0f-4aa3-b8c7-e5a6e96df9b0/preview/PIX_001_329F_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvY2YxYmU2MTEtY2UwZi00YWEzLWI4YzctZTVhNmU5NmRmOWIwL3ByZXZpZXcvUElYXzAwMV8zMjlGX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=ntt2imrUPoS1Y1oKFtQY4sMRnJrvcqjKeKohLjhCskNrDoA50zfc0C2kbrdS-rYw9r3BuvwWfbJfb8wjtm8eYDLb-2cO696wrSANO4pfov1bxmxHggSXkBr9LlL2Kik58VIHGmbwOT7kRIspJeH~R9HEdPs3v8okw5trl2mesyfKjXgnx1Pr435MSjlGVcpIZ4w~Cb5obmjKwl0UDVPIEatT3A-GWPCE-VCvgtN8ihIABGrKiPUdNgjblJBTaYTvG9EpP6jf5juAEuyCETtyrQ37FLQevXw3urlzivkbWpsZTzuiVMvcLwukUH1i05-ez~xkvih8W6Hjnyfx~qzwmA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "9a9e0303-4d1a-4b65-96af-9e5e1411ff65",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328",
                vendor: "Cineflex",
                duration: "04:33",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/9a9e0303-4d1a-4b65-96af-9e5e1411ff65/preview/PIX_001_328_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOWE5ZTAzMDMtNGQxYS00YjY1LTk2YWYtOWU1ZTE0MTFmZjY1L3ByZXZpZXcvUElYXzAwMV8zMjhfcmZzX3RodW0uMDAwMDAxMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=CuRRobCqVl59QmP2UylPXFyVo8LuXnavpuMwApEwc9ME-UMIXtoFNhC0nnYfndpf0vqG5IBC2dlIZS6ESFWtJwLLA7BrXqpW0cXVly-BX8qCY1fXKr1dfVeko0nSboyW72cJwn4ujuImHUBlGWJAd6yfJyWcGPXJIGnMgTRcwvXYiQSNHAmw6Z5Sw1SMHo~LnKY3RX-iwfQLXgqruggSY3HzY0Ybvnd1fmDA~CBjyUxm7pcsKdTO7VdbFQZL~sgjpK4jyfxRUXv~IepK~LdXbf1Nq9eTc1d3SszeDnJe5i3K5yQ3~Lq2YqdOS9ShyfQalq0uOWPYELrJyp9kxBN-bw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/9a9e0303-4d1a-4b65-96af-9e5e1411ff65/preview/PIX_001_328_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOWE5ZTAzMDMtNGQxYS00YjY1LTk2YWYtOWU1ZTE0MTFmZjY1L3ByZXZpZXcvUElYXzAwMV8zMjhfcmZzX3NuYXAuMDAwMDAwMC5qcGciLCAiQ29uZGl0aW9uIjogeyJEYXRlTGVzc1RoYW4iOiB7IkFXUzpFcG9jaFRpbWUiOiAxNTQ0OTk5ODI5fX19XX0_&Signature=CusLm5Z19dRQD994c-DY6oc1kUvXGiCSn8iv-QJFpqswmXIwF3M5F3609NReL9WtayRJmemcTAQWHOlmQSZ9v-e7genPpdyGjKya9ccfOBtBOtYq4SWtScla8K-39-C4G~dQ4MckUnl5G2vxaQKc8lboKcicfPvMrX~2K5jKJ4s0qw9IW6b1vgF8X0elXHyDPl5pOP3cDAYBZaD-jkTV7D89dKMY1UNnhUvk8LyT1I4-NwDycNSo8zukrLAc1Vf~Qx~4ULkoTmq1M4XARS5cUxA4~4hIv59T0It2TUYYjQ62sWiJbPSJ65~sDmRY7MOLghNq46WYQuuwU2wsvgyPnw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "9548ff6f-92e5-4588-a8a3-2b3a4821093d",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_331B",
                vendor: "Cineflex",
                duration: "00:10",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/9548ff6f-92e5-4588-a8a3-2b3a4821093d/preview/PIX_001_331B_rfs_thum.0000020.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTU0OGZmNmYtOTJlNS00NTg4LWE4YTMtMmIzYTQ4MjEwOTNkL3ByZXZpZXcvUElYXzAwMV8zMzFCX3Jmc190aHVtLjAwMDAwMjAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=A5Y0KZruHEoxZ4G~M0kX5sp88lPaxZWHKzHxVZ5Fowtf0EuS4o3H0Bhhk04VLvDnX~7AkjIrPUJMgXCe26oe815T96EV1DUfXedaXJQVAp2qsnAu0XAUVY7Ds491cJfAg3-wD0-wz~p~anQCB7FnB4OQE4jeThUVnIHe14Ny304P-Y7ulo4kil6TB5dwL88C8C0TwFHM98CwJRM4rs27JD-mvqG4f6DijUVfXrI5pgA4jI90qaJpL0HbxS4BJermd3EwkvZQBkZYrFC1mVvNs2LIW9~R-wjI754-1ngvDtYM40Oe0truDWYAvhjxdHXyruYrbef40UZ~IdgN3s1Dww__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/9548ff6f-92e5-4588-a8a3-2b3a4821093d/preview/PIX_001_331B_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvOTU0OGZmNmYtOTJlNS00NTg4LWE4YTMtMmIzYTQ4MjEwOTNkL3ByZXZpZXcvUElYXzAwMV8zMzFCX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=aR4Fv0sQay1CVutRwud2dwE-Od~yBQJO-JiAMit~N3M3iI514p3ZOIY~L4uADUCCHe-l3QVKjnP7EpZ9A2zbV78iMNWORF9ie1CvBv70BNZHkYVwf7UYkXlnwcqAq5jYtSPxOo4ukAnge8DgfJvoHJ4lC814qmM2sDGucGbUoiIFUqh2dtN5-562RAKKHDpOKbPVou6YXAWPmIdlXRSw7UANthC5n24mnsmofJG0Nss7DZg4n5cLinj66Ol9QSIs7GD1WSFHXcSPPIz-G6mA6x2x4D8FC51VC5h~FFYGAxl3rFJCrjp3Sx-C7hNUUAsUncCHS-AdiO2w-GNOZftvwg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "806f6123-da6d-4074-ab78-160635eab232",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_321D",
                vendor: "Cineflex",
                duration: "00:03",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/806f6123-da6d-4074-ab78-160635eab232/preview/PIX_001_321D_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODA2ZjYxMjMtZGE2ZC00MDc0LWFiNzgtMTYwNjM1ZWFiMjMyL3ByZXZpZXcvUElYXzAwMV8zMjFEX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=l0ONLee2VZOkU0bFXPuan9Sj2-EVrGkGbhxQT8dyvaBS54S5XmgG4Yi-8Ej8bXqc9Xwci4hASFe0tiEUUhCuDjwYoXQxlMZ1ttLI0bqdQougTU6GANM9E8Zq0LA3VA-cabMLCNUQrRY6E9q1huveOLKm~~xh6fRo4TRD7EOk89HDaSJ~1N3jkVCH5mcZL8kKqMTdqza~cOKgGszJ7psvCGJAsBqBZX0lsjBf8thC7cOb05ZKa-qQzjzO93JHo5gEqlgIgVO1iwvG7A~Z2CHZ5aBKPzhulOG-51b9bKs9ut5JzOkqhFBWhf~J55ZLwVuSv2vcsyHXHSOIoJRYN-A8Gg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/806f6123-da6d-4074-ab78-160635eab232/preview/PIX_001_321D_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODA2ZjYxMjMtZGE2ZC00MDc0LWFiNzgtMTYwNjM1ZWFiMjMyL3ByZXZpZXcvUElYXzAwMV8zMjFEX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=0i50~DdjCBuLCkb0i8P-RehKDMaRJBzTDywnPsQZ3nTWvP1k-8QVbGJ6jzZawbUw89alGyQ~o2z~gXKFw2IlnYwttgjtR235jLVbZoJfjjkiJGCkjT5bPnPgRGdL3xjim1IOwH60-Cauin-BiAqCe2001Qp2InL462m3y7AaYdBTSiH77TLWpCz6tf~nCoI8LNDpCyRMHjq5sOrO0uDAbnKHfEdXzskhsZmS8CvNRUXtawDoddHZSw1MMDOqRYvf4p93E-jP4sCE6GSIZRiYp0WpRaJtGNfyrqmR1gUqjvbroJqGDwA82hisqyk81x5ByBZobriYhP~hgRRZxupSrw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "489f1d54-8f0a-45b5-806f-30e781dce450",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_321E",
                vendor: "Cineflex",
                duration: "00:01",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/489f1d54-8f0a-45b5-806f-30e781dce450/preview/PIX_001_321E_rfs_thum.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNDg5ZjFkNTQtOGYwYS00NWI1LTgwNmYtMzBlNzgxZGNlNDUwL3ByZXZpZXcvUElYXzAwMV8zMjFFX3Jmc190aHVtLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=WRYzXlSuMfrdZKs8Rq5xLvY~~kReh0~9Pz7N~HQ5Ip-2pzAdtKuA7vYukYmzFQLRPiU6DCsuSbGPSIltWAhK7QP1ymNCFSoLkFjPExcsIZSi~A9K8C2abo4f8lZ-J4Qcvg0hdf83nQg1sWQS64xztz1NuwNRYTNBDOnHKqnF5ylKwDNXKDsPyZvoH0CuOdfXrTRh2R3jWOzhugQR3eLalXQwE-IhPKAth-bw4aOJTSeX32HWtXdz7PzQXyxrIN0vXqvVPin3WphwvNksGi8zSIRlfq~h~PPzjqRuRwTux1ohEzH0nMioESZhWsMmixsZKaFNt5ClE38EntLIeBlohQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/489f1d54-8f0a-45b5-806f-30e781dce450/preview/PIX_001_321E_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNDg5ZjFkNTQtOGYwYS00NWI1LTgwNmYtMzBlNzgxZGNlNDUwL3ByZXZpZXcvUElYXzAwMV8zMjFFX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=YZnD6tdBRqUnId88LaREOjG6QWUfpTezmi6lC051UHRXGL3Ho2YAK1XeTGoRhRKcJYhF~41L3FoVfN5BIp8qvVdQ0xqlZCVoU3rHbBHSHb6oUXPko6vfvo322dEJU4pTc78jCEf15-MFblVHB7SxCjN-8z5qy3PPh0E1BKyHQWOekIDUp~pM5tK1tiu3xO4MafeUEXGcBflKLZWfAFDB4xCRFuBJW2MFD0z7v~vtie2Zyhw1Peu3Zy-GtxLbnW8pqKNVlcC3L97WROcbUE2gozQk9Icq5vhE6RyXXPNRsYc1SvJxhzUXlHm5HVod-d-iO9HFzg2Mc3bXzdAcqObyZw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "5f189d5e-69f0-4f77-b28e-b44dd96a0d95",
                aircraftModel: "FB200",
                date: "2017-09-13",
                fireName: "Black Hill Tomalpin Fire",
                fileName: "PIX_001_332A",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/5f189d5e-69f0-4f77-b28e-b44dd96a0d95/preview/PIX_001_332A_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNWYxODlkNWUtNjlmMC00Zjc3LWIyOGUtYjQ0ZGQ5NmEwZDk1L3ByZXZpZXcvUElYXzAwMV8zMzJBX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=LfjJAeY~sXPTuAV5DeBhwA3JcRshFXQ8J1nKj7NFNf5su2Vajor7ihUr5FWY~RWUIwsZQWrMfG1NnH5iecYPjG~vQJktiEzSiVeL-wUHl7N~H-SpS~b~XrgxV4~RsjUwy59-eDh7h2FPuIOpJMVMMObbghd7HTnNkm0WDOSaAYBkaK0bINndyshIb8gwNZWAcl2W0cPhujWC8nbeyR3OlpIeJYB1-RgPTNzACOUNkaER5Ti0n3ZhBexoEd9RmS4Vx~HqON5eKbmsSAHG7NVOITZBqtVOMmmX8f4HKt8Qr7EMxjKC3bP8OYWI9YWsF8qU7NyEflZdy95J6~y73XxrKw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/5f189d5e-69f0-4f77-b28e-b44dd96a0d95/preview/PIX_001_332A_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNWYxODlkNWUtNjlmMC00Zjc3LWIyOGUtYjQ0ZGQ5NmEwZDk1L3ByZXZpZXcvUElYXzAwMV8zMzJBX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=CWaNWeRwujUrgdMYvEr37MvYhBLAqzyd-YAoOmFcMA6UHTE-x2QpfGzDNvUzEriwEOa90HrfZkl2eLDDUl3GscD5pFxbxH1zLdazEwFPfgM8r5KY3bcEYwOLuCmE2byMYM8LytZaUNhn6RObpLZCyS9anhHAgYyXMX1podNswUu4M9pJaZKMLCE~nIad5coAYlvS9X2g3KbZmHgv6Ka5psfnqE3NTip5z8QA-aNPg16rcb4l09l0p5DergariEPezGOSqCByaJOfnHoCpQZHemHp81~fQQhX14IuEGKN8s2k22GwAaazciDhKeGqBT9RtbUw7HbrPpAnPBvPU13a2Q__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "7809055e-bb41-424f-bc17-6cf6ebab151e",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_321G",
                vendor: "Cineflex",
                duration: "00:12",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/7809055e-bb41-424f-bc17-6cf6ebab151e/preview/PIX_001_321G_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNzgwOTA1NWUtYmI0MS00MjRmLWJjMTctNmNmNmViYWIxNTFlL3ByZXZpZXcvUElYXzAwMV8zMjFHX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=R9mbqpni71x-PWN7Q-7JJedWpxE9OBC0miC2qHcD1MBoBvyLC01PhZwSiU1ImBBJRIuXqPS0hvanApg0uMPDF53A-NUgJ0r2yyLCtmN01wN7FWql4jf42snZRWP2uE~9~ziw~MdmcgrTtPNPjtElUO9I8UsiqLZtSfzVqAXti4NytNS8Db3VrJDAOKzq0aCZKO7AjnHBztTgvOL~OCJgrslXYX1aPePcEZLc1sMu5FHxKtV-VQiwJqEj3FpWxm54PvKw2xSR-S9M~7fC4urGi6K7Sfl0Ubq~wRUZTzmU03yDHTDrGYervBApKxbskB3BNzwRtEm4pcB714H9CvDhnQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/7809055e-bb41-424f-bc17-6cf6ebab151e/preview/PIX_001_321G_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNzgwOTA1NWUtYmI0MS00MjRmLWJjMTctNmNmNmViYWIxNTFlL3ByZXZpZXcvUElYXzAwMV8zMjFHX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=tZsCmTo1kvWqeyIsPZwZCg7rpTGeSqy6nmnuh-rD9T2aSho7KcR29o5t26T89-gWKbCmuRwXFCW7a71yr8CsRU1Xx7flo0DzX553jgBTzPuAvlhfKEKzhsq9TbWcHy9U3J8Y28FJ5JctfenCa6hlNYP2aljCZqQiaGLAccAi40EFbKkVlnHbLsIuBRYdJrWwlyv5YjPjgSeE0~n7SK-FmBvScFSmlA21EtUXDjcjrjCeq0JZJYOiiQaQbj9DODnk9gfdwSM6xzfC6RsNeg9wc9F0uciWc35fvELg1LJw22EmldC25AuD2~FIUE5sMxFUlbA3OeHRPyApWmbzdENzcQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "7cb837ab-ce6c-453c-85a9-58a84343ee8a",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_328Z",
                vendor: "Cineflex",
                duration: "00:03",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/7cb837ab-ce6c-453c-85a9-58a84343ee8a/preview/PIX_001_328Z_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvN2NiODM3YWItY2U2Yy00NTNjLTg1YTktNThhODQzNDNlZThhL3ByZXZpZXcvUElYXzAwMV8zMjhaX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=kubf4-O3wOgOydovu7WzDfd0x9q-sMloAavoH0LJ94Wi6VaDmZlubTqSvCxBh-YjUkbF60FpPsreDCbeY8KgzITZImljVcljQrgFF1zpwGRBzs7Qf~25hs2r~8aMduZxQ3QcwxJJtZUi0OToHoI-nU7jTNK4iDva9ZGBEbCC9oo5zGGVkZDys2~O~rSYFrbiyWrocFhurVRHIJ9bILrWsDq3ZjqnKtD0hGlwfBuzdNlksEBofaTZKWQWM6pLgzNaWng7dy9XN~QKtehYe7JjnVNo667od4daiJddDnnOiolLfoQhQyzVc3X8tZQweLa-Le4CG9IZ-nj9f28NC8Iqew__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/7cb837ab-ce6c-453c-85a9-58a84343ee8a/preview/PIX_001_328Z_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvN2NiODM3YWItY2U2Yy00NTNjLTg1YTktNThhODQzNDNlZThhL3ByZXZpZXcvUElYXzAwMV8zMjhaX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=Lnzyturf8gWzRmA1xSXcQ6eLOZ2wosFejRfnc9r8Q2cCYBnUakGrLeH8hF8YQuKK4UUZOK1LMltbFR7PrQGwa9SH-mvBg4gEliJevfqj069psJjG~U8Erk54-SgiB7G-E3V4Y2w4MRwP~LXM2HT1HonSmvjdIzHSlSGyUSSeprXYxoHv9~bx3kKX~bIOYrZ0IB9gxjrcyJVFC~z~WGk~o1srkQinOHk6j85qy-9xooixBcPqdvfsMSlWMw96KorGPTlbNx6AYNVQOr~vhVFu-TpoDjuP8BIGC4fWBSyMTkc4O86Fl8LOcoWnnVh4iJ4yF54ohVXi8c-fnj-SdEUamg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "c3b813ca-2ce5-4af1-be94-4ebd9bbcc998",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_330A",
                vendor: "Cineflex",
                duration: "00:11",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/c3b813ca-2ce5-4af1-be94-4ebd9bbcc998/preview/PIX_001_330A_rfs_thum.0000020.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYzNiODEzY2EtMmNlNS00YWYxLWJlOTQtNGViZDliYmNjOTk4L3ByZXZpZXcvUElYXzAwMV8zMzBBX3Jmc190aHVtLjAwMDAwMjAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=OGqH4k3l2p5QlYCobI7Wog2emilbZUyvXD2cXCABVRw-zxF1~jQB0yuhsrLjMRNyHAzDhtJ2jvwcWshMGzlwXJK6rOERq3LJyfO7OR-PBGyImAKB8kVo9ESwDUH8iF4ZltJTay~rpXQa5LQkUSNQbA300u96yI6N5BZOm04Q4nvZgeTsQbOQPeTIrmJYA5T12kPxqWbAY1csEb3yW7t233d~oFNU3~UV0lsESN67ijbQLMVVpA4mIc4AHXFXjk2170hxH8dYUb4eJz6X8uC92Y-4P1SrUf7aYmLFNgU9ZPvT~Uov3UG09JtFphQNfvPZY4lX0Pe~Vk6Jv6okgh59TQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/c3b813ca-2ce5-4af1-be94-4ebd9bbcc998/preview/PIX_001_330A_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYzNiODEzY2EtMmNlNS00YWYxLWJlOTQtNGViZDliYmNjOTk4L3ByZXZpZXcvUElYXzAwMV8zMzBBX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=EgCkVZs5rnutTiRXwnKLhhjUbrHaGbTAzUIHZRk7aD-mFwbSaFS3KBQY1MhANNNS8bDpC~IotHqCr9NmqzWUCfiptqqo4PPkckphiciFae79R6M~0fe57wK4cuA4xNnGyQv2APTjwxyRuQJRz7-1sUSISimTyBEEfrHwoVfv8ZcppnxHbi5Gm1VUjOCq60y56Vn5G8uCjQfvT15UN9WlxJt23ifoWHgs0EvoAVRfHSwFUR3Fo26mhKImutRsrkVb4l39pF2qc9haeAmhlj9NF-qGKInd780CIhXiezU7nZjhccMd3mhdM0r1ArCEwRU7nnZPx2DUqoYxtB8U1HSZ~A__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "769a1b77-3ec0-4507-bd46-ca8455fa9c52",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329L",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/769a1b77-3ec0-4507-bd46-ca8455fa9c52/preview/PIX_001_329L_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNzY5YTFiNzctM2VjMC00NTA3LWJkNDYtY2E4NDU1ZmE5YzUyL3ByZXZpZXcvUElYXzAwMV8zMjlMX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=JEJBx4PJugqAXBhYa55VtOaGbj6WQmMWNqAKbs2qsawJx6LjgW86QS62AwsWKFtqev0mQcZk3q1k94O-5ZGoSb7fF9sCRnD3QAJMen5Hz6cPgn8O2nhwDmH2x~D8An7~I4rmasGDFdamfoyDRj6qRWR46~q8cRigFKQ2AhE7xSi69-xPOy8wCFatZGyN3zTZwGGyxDLJHCGmNfDkuadHr4wHK-W4j8su-l7wujE5C~J6Osh~0XfHFkfXtGBDFuuX1QAIH6tH5FbjvrJer~qnqeRk85T4SRWsf0EM30l7NjUI7dNgnvNIpg0bFg1jykNOsQL9x3H2vkHaeNUWmqcpvQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/769a1b77-3ec0-4507-bd46-ca8455fa9c52/preview/PIX_001_329L_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNzY5YTFiNzctM2VjMC00NTA3LWJkNDYtY2E4NDU1ZmE5YzUyL3ByZXZpZXcvUElYXzAwMV8zMjlMX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=IkHBcXNlKPvyaguNaGSoZOdzUHKaREMjG3ibnkzMz0rXBf4V4bGexJ1vpAV8~QYoljbV58nlsUsAzRO4wrUSoDaCby9tV-nvo9uhy7H4vC7IQ028EjIVjpQrGm5B2mxli91orlNnpBHGDfjqYBMH0TGrUPLcZBUip4WQh1k3YFZ3Da5GJIAjh2J020HM0iULghSLPyyT4ueLbTLYdedVD9QDUE3zXJdgu3iybmwW7-hnPiGRMqUAjbQwJGe3yiDYh2NnJ6XOdPWL1v9dB3j~M21SZnTp5lTFTc~0v8qTC81Be98RU42uwcY1MO1Q6HwcOw~mZZZVvcfz7UbG4nXsPQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "a7ad8fcf-eb92-4695-a9ae-08361ee03cc7",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_322C",
                vendor: "Cineflex",
                duration: "00:19",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/a7ad8fcf-eb92-4695-a9ae-08361ee03cc7/preview/PIX_001_322C_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYTdhZDhmY2YtZWI5Mi00Njk1LWE5YWUtMDgzNjFlZTAzY2M3L3ByZXZpZXcvUElYXzAwMV8zMjJDX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=ueKol3ZVVA9p4aZQc9W6EhVyI3npBMZEtEd0pQl0fX82LJVALuUxBG4Z3KVhjIUoWJemTTejEL7NoZKOlVjqYpwwZe-uckjSzNeub-0Pb9yciHWQL-waFRwGwz7MPdXNIkg-1bTCvlJMmv~oyZm50LTEtC-M~oJBYBlouHP7sXzirHG6YRU~s7T-ljjpiGQaGGLAuKKHl~pTo9x0b6obRLm3QjTiMKALHN-O0FsuLBVPm-T61-BthqmG3tZkeJ3ITbHWK63nf6m-cct00CSkvQ5tSjegGe6V3ZxS3Kox0yYK1bFxEdxkYVsXybJW9CPzJKSZ45wu0jILhoIp7c0EBg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/a7ad8fcf-eb92-4695-a9ae-08361ee03cc7/preview/PIX_001_322C_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYTdhZDhmY2YtZWI5Mi00Njk1LWE5YWUtMDgzNjFlZTAzY2M3L3ByZXZpZXcvUElYXzAwMV8zMjJDX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=cRp08kp4-Fi81DANqJ9nobxoFC4siHHrpKWOrc3sJkiIp6vHe9GOJSM3bcThHOejTksB196kxGva446vfr4tIuOTzzU2hjoXGlOEBXr-6SoK4VGkIEdDobj3TTc~sB~VjqxPTVI9qqevp88Q6aiew6~fzeGhw~AmBM5dHqINhj4C37NZmyfuCtCTlia9rXG6B-Sl8XG2HLCJn8Eeph3yfVSm6UcJwYs0qk5-H39MrOgEtr0eUI-jHylVcXpj6J5EXY-Mdoq52~78E40it5njAcc~Q~FfCompcBVsHWdfNlVhzW5JJXzHwMHoEK47vPXBCiFXB3rCaob7Wxs5PnQvOw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "279efa4f-01aa-4e28-a1df-9434062e8fcf",
                aircraftModel: "FB200",
                date: "2017-09-13",
                fireName: "Black Hill Tomalpin Fire",
                fileName: "PIX_001_332S",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/279efa4f-01aa-4e28-a1df-9434062e8fcf/preview/PIX_001_332S_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMjc5ZWZhNGYtMDFhYS00ZTI4LWExZGYtOTQzNDA2MmU4ZmNmL3ByZXZpZXcvUElYXzAwMV8zMzJTX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=FY9KaVKocPzCoFz6xLiRANxGVpPSJ1FHenoUSET4Nn3Ut0SkZwaY3U7xv0S8yNDWWEssAKpvSeUvXQcL1l0UfJUFf66WSv0yM6MlKhb52FmBoLhgCgNycztgt28TLmiZW~hJxgYlBUKRmubLyJsjBHh-GzFM49tHwWJ0K8DKIwCM6anDw9tmQXXBxy5YX8xlplW5Lu04LnbdyIhQLzWrAF5Rtp8U-b3USupgUp3PcOLEPWmpx55elsT6jxWcqRdR6xJzz-pejEiDp6VCTSTqYtQx0jK160RNDaRA5a8IAEsSlTok9ipFngzzWO7vsIqxzH33daZRT1FFom7ItQ8DoA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/279efa4f-01aa-4e28-a1df-9434062e8fcf/preview/PIX_001_332S_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMjc5ZWZhNGYtMDFhYS00ZTI4LWExZGYtOTQzNDA2MmU4ZmNmL3ByZXZpZXcvUElYXzAwMV8zMzJTX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=QLVes8CtJlkhB7Rwo8DQSJ8IbKeogOIx2wZkViA2wSxAIvg~0StoR8OjoGj~YKxixgZk-QQKoxKu-BcdiXmPFbY2RP4JczGKHL14L03zd0-Hgr3LkdhgTyuvrjuoh2qLhSX6EAJzgXHEO4iXjZW4lHXItbW3pmIFSuT-k7ClhqjqIMqX3ttekwDuUoccZ~XCx3vY2~UgTGR10JddvmOkF4wl9eDasZmxMDRAu14M7yFIBVqSeMP~4mCkMNvdojpdA3BgAd9eon58DcUiH32xEGDLZdQn7KWsHeI-elnfTSaiXoF9eEVUhFoAnRvRqa9Xv6pblYCRcXiqdvXTqZCJrg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "8570e391-a789-4c48-a873-48470578d2da",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_330C",
                vendor: "Cineflex",
                duration: "00:19",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/8570e391-a789-4c48-a873-48470578d2da/preview/PIX_001_330C_rfs_thum.0000030.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODU3MGUzOTEtYTc4OS00YzQ4LWE4NzMtNDg0NzA1NzhkMmRhL3ByZXZpZXcvUElYXzAwMV8zMzBDX3Jmc190aHVtLjAwMDAwMzAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=x-cuTpegyAeO75U~j13kAGtNlUcfi4XcU0fHxJnMYeNc5TIWm32Y2kz5c6bf~IgJ4C4cRP1dJS0h2dXfadth9RInK51i4dns3DRp9QKGMv1klDqBCWNioBHfUJ6-V-fAq-AVVb6QUFrpRzzNq80hcc0t7-oPf8XFDzQ~KRAmSyivLcabKhXoT5ATlJOuneSkCKV1zSFbgjFF0UxZskoeD00uVd~SpP460H5~s8d8fvMjWAzjuARN8BIqVM3QOQqs8TTm5ZkZ6ETEwKxEvESdRXGdGRmcVXEhf6hnH1baPI7kMwJ8ad-inlj2dmOHsfL19--BIPq62uejN5vb-SJxgw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/8570e391-a789-4c48-a873-48470578d2da/preview/PIX_001_330C_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODU3MGUzOTEtYTc4OS00YzQ4LWE4NzMtNDg0NzA1NzhkMmRhL3ByZXZpZXcvUElYXzAwMV8zMzBDX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=SNMdKJJ3ujycVYB8WGqKabbKzSZFgMTecOs2F6Zzw8oBmrCIXmC4f09n0avHJPKgg5GGowJ6hoIYHfmcICMexkAebiteIqJKJ5xHQqfg01wXHsY5cQLlk0YYF0VksDnzu0LpvmGTDP55Byp20s-j20gaRLy7IVm0SZ6EalpA~L1EeMRm-JViI8zOWbRksKvx-pNbrQ6dWOAQL4Mfs4uZAqzdmYli90x73M9FYVa2JdxpIBwx0W4Tb6RKTHwKX6T8D7LQ4J94Qjme~xm8MbtDGvzU6RtPKRyNoEDbb68UlyCenQ3G0P0eXTyXD9p36RGD~cxx1NDm~8HWR3edpjmSrg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            }
        ];



        this.moreList = [
            {
                guid: "c3b813ca-2ce5-4af1-be94-4ebd9bbcc998",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_330A",
                vendor: "Cineflex",
                duration: "00:11",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/c3b813ca-2ce5-4af1-be94-4ebd9bbcc998/preview/PIX_001_330A_rfs_thum.0000020.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYzNiODEzY2EtMmNlNS00YWYxLWJlOTQtNGViZDliYmNjOTk4L3ByZXZpZXcvUElYXzAwMV8zMzBBX3Jmc190aHVtLjAwMDAwMjAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=OGqH4k3l2p5QlYCobI7Wog2emilbZUyvXD2cXCABVRw-zxF1~jQB0yuhsrLjMRNyHAzDhtJ2jvwcWshMGzlwXJK6rOERq3LJyfO7OR-PBGyImAKB8kVo9ESwDUH8iF4ZltJTay~rpXQa5LQkUSNQbA300u96yI6N5BZOm04Q4nvZgeTsQbOQPeTIrmJYA5T12kPxqWbAY1csEb3yW7t233d~oFNU3~UV0lsESN67ijbQLMVVpA4mIc4AHXFXjk2170hxH8dYUb4eJz6X8uC92Y-4P1SrUf7aYmLFNgU9ZPvT~Uov3UG09JtFphQNfvPZY4lX0Pe~Vk6Jv6okgh59TQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/c3b813ca-2ce5-4af1-be94-4ebd9bbcc998/preview/PIX_001_330A_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYzNiODEzY2EtMmNlNS00YWYxLWJlOTQtNGViZDliYmNjOTk4L3ByZXZpZXcvUElYXzAwMV8zMzBBX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=EgCkVZs5rnutTiRXwnKLhhjUbrHaGbTAzUIHZRk7aD-mFwbSaFS3KBQY1MhANNNS8bDpC~IotHqCr9NmqzWUCfiptqqo4PPkckphiciFae79R6M~0fe57wK4cuA4xNnGyQv2APTjwxyRuQJRz7-1sUSISimTyBEEfrHwoVfv8ZcppnxHbi5Gm1VUjOCq60y56Vn5G8uCjQfvT15UN9WlxJt23ifoWHgs0EvoAVRfHSwFUR3Fo26mhKImutRsrkVb4l39pF2qc9haeAmhlj9NF-qGKInd780CIhXiezU7nZjhccMd3mhdM0r1ArCEwRU7nnZPx2DUqoYxtB8U1HSZ~A__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "769a1b77-3ec0-4507-bd46-ca8455fa9c52",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_329L",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/769a1b77-3ec0-4507-bd46-ca8455fa9c52/preview/PIX_001_329L_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNzY5YTFiNzctM2VjMC00NTA3LWJkNDYtY2E4NDU1ZmE5YzUyL3ByZXZpZXcvUElYXzAwMV8zMjlMX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=JEJBx4PJugqAXBhYa55VtOaGbj6WQmMWNqAKbs2qsawJx6LjgW86QS62AwsWKFtqev0mQcZk3q1k94O-5ZGoSb7fF9sCRnD3QAJMen5Hz6cPgn8O2nhwDmH2x~D8An7~I4rmasGDFdamfoyDRj6qRWR46~q8cRigFKQ2AhE7xSi69-xPOy8wCFatZGyN3zTZwGGyxDLJHCGmNfDkuadHr4wHK-W4j8su-l7wujE5C~J6Osh~0XfHFkfXtGBDFuuX1QAIH6tH5FbjvrJer~qnqeRk85T4SRWsf0EM30l7NjUI7dNgnvNIpg0bFg1jykNOsQL9x3H2vkHaeNUWmqcpvQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/769a1b77-3ec0-4507-bd46-ca8455fa9c52/preview/PIX_001_329L_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvNzY5YTFiNzctM2VjMC00NTA3LWJkNDYtY2E4NDU1ZmE5YzUyL3ByZXZpZXcvUElYXzAwMV8zMjlMX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=IkHBcXNlKPvyaguNaGSoZOdzUHKaREMjG3ibnkzMz0rXBf4V4bGexJ1vpAV8~QYoljbV58nlsUsAzRO4wrUSoDaCby9tV-nvo9uhy7H4vC7IQ028EjIVjpQrGm5B2mxli91orlNnpBHGDfjqYBMH0TGrUPLcZBUip4WQh1k3YFZ3Da5GJIAjh2J020HM0iULghSLPyyT4ueLbTLYdedVD9QDUE3zXJdgu3iybmwW7-hnPiGRMqUAjbQwJGe3yiDYh2NnJ6XOdPWL1v9dB3j~M21SZnTp5lTFTc~0v8qTC81Be98RU42uwcY1MO1Q6HwcOw~mZZZVvcfz7UbG4nXsPQ__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "a7ad8fcf-eb92-4695-a9ae-08361ee03cc7",
                aircraftModel: "FB200",
                date: "2017-08-05",
                fireName: "Bells Line of Road",
                fileName: "PIX_001_322C",
                vendor: "Cineflex",
                duration: "00:19",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/a7ad8fcf-eb92-4695-a9ae-08361ee03cc7/preview/PIX_001_322C_rfs_thum.0000010.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYTdhZDhmY2YtZWI5Mi00Njk1LWE5YWUtMDgzNjFlZTAzY2M3L3ByZXZpZXcvUElYXzAwMV8zMjJDX3Jmc190aHVtLjAwMDAwMTAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=ueKol3ZVVA9p4aZQc9W6EhVyI3npBMZEtEd0pQl0fX82LJVALuUxBG4Z3KVhjIUoWJemTTejEL7NoZKOlVjqYpwwZe-uckjSzNeub-0Pb9yciHWQL-waFRwGwz7MPdXNIkg-1bTCvlJMmv~oyZm50LTEtC-M~oJBYBlouHP7sXzirHG6YRU~s7T-ljjpiGQaGGLAuKKHl~pTo9x0b6obRLm3QjTiMKALHN-O0FsuLBVPm-T61-BthqmG3tZkeJ3ITbHWK63nf6m-cct00CSkvQ5tSjegGe6V3ZxS3Kox0yYK1bFxEdxkYVsXybJW9CPzJKSZ45wu0jILhoIp7c0EBg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/a7ad8fcf-eb92-4695-a9ae-08361ee03cc7/preview/PIX_001_322C_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvYTdhZDhmY2YtZWI5Mi00Njk1LWE5YWUtMDgzNjFlZTAzY2M3L3ByZXZpZXcvUElYXzAwMV8zMjJDX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=cRp08kp4-Fi81DANqJ9nobxoFC4siHHrpKWOrc3sJkiIp6vHe9GOJSM3bcThHOejTksB196kxGva446vfr4tIuOTzzU2hjoXGlOEBXr-6SoK4VGkIEdDobj3TTc~sB~VjqxPTVI9qqevp88Q6aiew6~fzeGhw~AmBM5dHqINhj4C37NZmyfuCtCTlia9rXG6B-Sl8XG2HLCJn8Eeph3yfVSm6UcJwYs0qk5-H39MrOgEtr0eUI-jHylVcXpj6J5EXY-Mdoq52~78E40it5njAcc~Q~FfCompcBVsHWdfNlVhzW5JJXzHwMHoEK47vPXBCiFXB3rCaob7Wxs5PnQvOw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "279efa4f-01aa-4e28-a1df-9434062e8fcf",
                aircraftModel: "FB200",
                date: "2017-09-13",
                fireName: "Black Hill Tomalpin Fire",
                fileName: "PIX_001_332S",
                vendor: "Cineflex",
                duration: "05:00",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/279efa4f-01aa-4e28-a1df-9434062e8fcf/preview/PIX_001_332S_rfs_thum.0000040.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMjc5ZWZhNGYtMDFhYS00ZTI4LWExZGYtOTQzNDA2MmU4ZmNmL3ByZXZpZXcvUElYXzAwMV8zMzJTX3Jmc190aHVtLjAwMDAwNDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=FY9KaVKocPzCoFz6xLiRANxGVpPSJ1FHenoUSET4Nn3Ut0SkZwaY3U7xv0S8yNDWWEssAKpvSeUvXQcL1l0UfJUFf66WSv0yM6MlKhb52FmBoLhgCgNycztgt28TLmiZW~hJxgYlBUKRmubLyJsjBHh-GzFM49tHwWJ0K8DKIwCM6anDw9tmQXXBxy5YX8xlplW5Lu04LnbdyIhQLzWrAF5Rtp8U-b3USupgUp3PcOLEPWmpx55elsT6jxWcqRdR6xJzz-pejEiDp6VCTSTqYtQx0jK160RNDaRA5a8IAEsSlTok9ipFngzzWO7vsIqxzH33daZRT1FFom7ItQ8DoA__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/279efa4f-01aa-4e28-a1df-9434062e8fcf/preview/PIX_001_332S_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvMjc5ZWZhNGYtMDFhYS00ZTI4LWExZGYtOTQzNDA2MmU4ZmNmL3ByZXZpZXcvUElYXzAwMV8zMzJTX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=QLVes8CtJlkhB7Rwo8DQSJ8IbKeogOIx2wZkViA2wSxAIvg~0StoR8OjoGj~YKxixgZk-QQKoxKu-BcdiXmPFbY2RP4JczGKHL14L03zd0-Hgr3LkdhgTyuvrjuoh2qLhSX6EAJzgXHEO4iXjZW4lHXItbW3pmIFSuT-k7ClhqjqIMqX3ttekwDuUoccZ~XCx3vY2~UgTGR10JddvmOkF4wl9eDasZmxMDRAu14M7yFIBVqSeMP~4mCkMNvdojpdA3BgAd9eon58DcUiH32xEGDLZdQn7KWsHeI-elnfTSaiXoF9eEVUhFoAnRvRqa9Xv6pblYCRcXiqdvXTqZCJrg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            },
            {
                guid: "8570e391-a789-4c48-a873-48470578d2da",
                aircraftModel: "FB200",
                date: "2017-09-12",
                fireName: "Beacon Hill HR",
                fileName: "PIX_001_330C",
                vendor: "Cineflex",
                duration: "00:19",
                thumbnailUrl: "https://dy20mk379uhbi.cloudfront.net/8570e391-a789-4c48-a873-48470578d2da/preview/PIX_001_330C_rfs_thum.0000030.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODU3MGUzOTEtYTc4OS00YzQ4LWE4NzMtNDg0NzA1NzhkMmRhL3ByZXZpZXcvUElYXzAwMV8zMzBDX3Jmc190aHVtLjAwMDAwMzAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=x-cuTpegyAeO75U~j13kAGtNlUcfi4XcU0fHxJnMYeNc5TIWm32Y2kz5c6bf~IgJ4C4cRP1dJS0h2dXfadth9RInK51i4dns3DRp9QKGMv1klDqBCWNioBHfUJ6-V-fAq-AVVb6QUFrpRzzNq80hcc0t7-oPf8XFDzQ~KRAmSyivLcabKhXoT5ATlJOuneSkCKV1zSFbgjFF0UxZskoeD00uVd~SpP460H5~s8d8fvMjWAzjuARN8BIqVM3QOQqs8TTm5ZkZ6ETEwKxEvESdRXGdGRmcVXEhf6hnH1baPI7kMwJ8ad-inlj2dmOHsfL19--BIPq62uejN5vb-SJxgw__&Key-Pair-Id=APKAJV5DZFV4TMUME64A",
                snapshotUrl: "https://dy20mk379uhbi.cloudfront.net/8570e391-a789-4c48-a873-48470578d2da/preview/PIX_001_330C_rfs_snap.0000000.jpg?Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6ICJodHRwczovL2R5MjBtazM3OXVoYmkuY2xvdWRmcm9udC5uZXQvODU3MGUzOTEtYTc4OS00YzQ4LWE4NzMtNDg0NzA1NzhkMmRhL3ByZXZpZXcvUElYXzAwMV8zMzBDX3Jmc19zbmFwLjAwMDAwMDAuanBnIiwgIkNvbmRpdGlvbiI6IHsiRGF0ZUxlc3NUaGFuIjogeyJBV1M6RXBvY2hUaW1lIjogMTU0NDk5OTgyOX19fV19&Signature=SNMdKJJ3ujycVYB8WGqKabbKzSZFgMTecOs2F6Zzw8oBmrCIXmC4f09n0avHJPKgg5GGowJ6hoIYHfmcICMexkAebiteIqJKJ5xHQqfg01wXHsY5cQLlk0YYF0VksDnzu0LpvmGTDP55Byp20s-j20gaRLy7IVm0SZ6EalpA~L1EeMRm-JViI8zOWbRksKvx-pNbrQ6dWOAQL4Mfs4uZAqzdmYli90x73M9FYVa2JdxpIBwx0W4Tb6RKTHwKX6T8D7LQ4J94Qjme~xm8MbtDGvzU6RtPKRyNoEDbb68UlyCenQ3G0P0eXTyXD9p36RGD~cxx1NDm~8HWR3edpjmSrg__&Key-Pair-Id=APKAJV5DZFV4TMUME64A"
            }
        ];

    }


}
