import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DyncFilterComponent } from './dync-filter.component';

describe('DyncFilterComponent', () => {
  let component: DyncFilterComponent;
  let fixture: ComponentFixture<DyncFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DyncFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DyncFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
