import {
    AfterViewInit,
    Component, ElementRef, EventEmitter, Input,
    OnInit, Output, QueryList, ViewChildren
} from '@angular/core';
import {FilterUnitObj} from '../../../lib/class';


@Component({
    selector: 'app-filter-unit',
    templateUrl: './filter-unit.component.html',
    styleUrls: ['./filter-unit.component.scss']
})
export class FilterUnitComponent implements OnInit, AfterViewInit {

    @Input()
    public filterName: string;
    @Input()
    public filterItems: string[];

    @Input()
    public checkMobile: boolean;


    @Output() filterEmitter: EventEmitter<FilterUnitObj> = new EventEmitter();
    @Output() closeEmitter: EventEmitter<FilterUnitObj> = new EventEmitter();


    @ViewChildren('ListSpans')
    private todoSpans: QueryList<ElementRef>;

    @ViewChildren('ListIs')
    private todoListIs: QueryList<ElementRef>;


    ngAfterViewInit() {
    }


    private isPicked: boolean = false;
    private isCloseShow: boolean = false;

    private filterUnitObj: FilterUnitObj;
    private closeFilterUnitObj: FilterUnitObj;


    public emmitPickItem(i) {

        this.handlePickStyle(i);
        this.isPicked = !this.isPicked;

        let filterItem: string = this.filterItems[parseInt(i)];
        if (filterItem) {
            //    console.log('emmitClickItem---' + filterItem);
            this.filterUnitObj.name = this.filterName;
            this.filterUnitObj.option = filterItem;
            //  console.warn(this.filterUnitObj);
            this.filterEmitter.emit(this.filterUnitObj);

            return filterItem;
        }
    }


    public emmitCloseItem(i) {

        this.handleCrossHitStyle();
        this.isCloseShow = !this.isCloseShow;

        let closeItem: string = this.filterItems[parseInt(i)];

        if (closeItem) {
            this.closeFilterUnitObj.name = this.filterName;
            this.closeFilterUnitObj.option = closeItem;

            this.closeEmitter.emit(this.closeFilterUnitObj);
        }
    }


    constructor() {

        this.filterUnitObj = {
            name: '', option: ''
        };

        this.closeFilterUnitObj = {
            name: '', option: ''
        };


    }

    ngOnInit() {
    }


    private handlePickStyle(i: number) {
        this.todoSpans.forEach(
            e => {
                e.nativeElement.style = 'color: gray; font-weight:initial';
                //  console.log(e.nativeElement.innerText);
            }
        );

        this.todoListIs.forEach(
            e => {
                e.nativeElement.style = 'display: none';
            }
        );


        this.todoSpans.find(
            (item, index) => {
                return index === i;
            }
        ).nativeElement.style = 'color: black; font-weight:bold';


        this.todoListIs.find(
            (item, index) => {
                return index === i;
            }
        ).nativeElement.style = 'display: inline-block';

    }


    private handleCrossHitStyle() {
        this.todoSpans.forEach(
            e => {
                e.nativeElement.style = 'color: gray; font-weight:initial';
            }
        );

        this.todoListIs.forEach(
            e => {
                e.nativeElement.style = 'display: none';
            }
        );
    }


}
