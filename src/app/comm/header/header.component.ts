import {Component, OnInit, Output, EventEmitter, ViewChild, NgZone} from '@angular/core';
import {Router} from '@angular/router';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';

export interface Tile {
    color: string;
    cols: number;
    rows: number;
    text: string;
}

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    constructor(private router: Router,
                private breakpointObserver: BreakpointObserver,
                public ngZone: NgZone
    ) {

        this.scrollDetection();
    }

    @Output()
    private passSearchEmitter: EventEmitter<string> = new EventEmitter();
    @Output()
    private latestEmitter: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public logon: EventEmitter<any> = new EventEmitter();

    @Output()
    private scrollMessage: EventEmitter<boolean> = new EventEmitter();


    @ViewChild('searchBar')
    private searchBar;


    public isMobile: boolean;

    public debug: boolean = false;


    private lastScrollTop: number = 0;
    private direction: string = '';

    public scrollDown: boolean = false;




    private scrollDetection() {

        window.onscroll = () => {

            let st: number = window.pageYOffset;
            let dir: string = '';
            if (st > this.lastScrollTop) {
                dir = 'down';
                if (st > 360)
                    this.scrollDown = true;

            } else {
                dir = 'up';

                if (st < 220)
                    this.scrollDown = false;

            }
            // this.lastScrollTop = st;
            // run outside angular
            this.ngZone.runOutsideAngular(
                () => {
                    this.direction = dir;
                    this.lastScrollTop = st;
                    this.scrollMessage.emit(this.scrollDown);
                }
            );

            // run inside angular
            // this.ngZone.run(() => {
            //     this.direction = dir;
            // });

        };

    }


    public passSearch(str: string) {
        this.passSearchEmitter.emit(str);
    }


    public getLatest() {
        this.latestEmitter.emit(true);
        this.searchBar.resetKeyword();

    }


    public showdebug() {
        this.debug = this.debug ? false : true;
    }

    public unchangedurl() {
        this.router.navigateByUrl('/content', {skipLocationChange: true});
    }


    public listLayout: any = {
        rowHeight: '3rem',
        gutterSize: '1rem'
    };


    public myLayout: any = {
        logoRow: 2,
        logoCol: 3,
        titleRow: 2,
        titleCol: 3
    };

    public logoClass: any;
    public titleClass: any;

    private changeToMobile(isMobile) {

        if (isMobile) {
            this.isMobile = true;
            this.listLayout.rowHeight = '2.6rem';
            this.listLayout.gutterSize = '0.2rem';

            this.myLayout.logoCol = 3;
            this.myLayout.titleCol = 3;

            this.logoClass = {
                'for-mobile': true,
                'for-other': false,
            };

            this.titleClass = {
                'for-mobile2': true,
                'for-other2': false,
            };
        }
        else {
            this.isMobile = false;

            this.listLayout.rowHeight = '3rem';
            this.listLayout.gutterSize = '1rem';

            this.myLayout.logoCol = 3;
            this.myLayout.titleCol = 3;
            this.logoClass = {
                'for-mobile': false,
                'for-other': true,
            };

            this.titleClass = {
                'for-mobile2': false,
                'for-other2': true,
            };
        }
    }


    ngOnInit() {

        const isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');
        this.breakpointObserver.observe([
            Breakpoints.HandsetPortrait
            // Breakpoints.HandsetLandscape,
        ]).subscribe(result => {

            //  console.log(result);

            if (result.matches) {

                this.changeToMobile(true);


            } else {
                this.changeToMobile(false);
            }
        });


    }

}



