import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ViewPortService} from '../../../api/view-port.service';

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

    @Input()
    isMobile: boolean;

    @Output()
    emmitSearch: EventEmitter<string> = new EventEmitter();

    @Output()
    userLogon: EventEmitter<any> = new EventEmitter();

    public searchString: string;


    // constructor(  private  viewport: ViewPortService  ) {
    // }


    constructor() {
    }


    ngOnInit() {

    }

    public lauchSearch() {
        if (this.searchString) {
            this.emmitSearch.emit(this.searchString);
        }

    }



    public detectViewPort() {

    }


    public resetKeyword() {
        this.searchString = '';
    }


    public checkString(): boolean {

        if (this.searchString) {
            return this.searchString.length > 5 ? true : false;
        }

    }


    // public detectViewPort() {
    //
    //     this.viewport.viewPort().subscribe(
    //         result => {
    //             // console.log(val.matches);
    //
    //             if (result.matches) {
    //                 console.log('yes eyes yese');
    //
    //                 return true;
    //
    //
    //             } else {
    //                 return false;
    //             }
    //
    //         }
    //     );
    //
    // }


}

