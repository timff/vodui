import {Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {FilterUnitObj, VideoFilter} from '../../lib/class';


export interface VideoItem {
    id: string;
    filename: string;
    aircraftType: string;
    aircraftModel: string;
    year: string;
    airth: string;
    releaseDate: string;
}


@Component({
    selector: 'app-item-filter',
    templateUrl: './item-filter.component.html',
    styleUrls: ['./item-filter.component.scss']
})
export class ItemFilterComponent implements OnInit {

    @Output()
    filterItemOnClick: EventEmitter<FilterUnitObj> = new EventEmitter();

    @Output()
    closeFilterItemOnClick: EventEmitter<FilterUnitObj> = new EventEmitter();


    @Output()
    orderDate: EventEmitter<boolean> = new EventEmitter();


    public filterGroup: VideoFilter[];

    public isFilterBoxOpen: boolean = false;

    public openFilter() {
        this.isFilterBoxOpen = !this.isFilterBoxOpen;
    }

    @HostListener('document:keydown', ['$event'])
    public keyboardOpenFilter(event: KeyboardEvent): void {
        //  console.log(event.key);
        event.stopPropagation();
        if (event.key === 'Shift') {
            this.openFilter();
        }
    }

    constructor(private breakpoint: BreakpointObserver) {
    }

    isShowMobileMenu: boolean = false;

    showMobileMenu() {
        this.isShowMobileMenu = this.isShowMobileMenu ? false : true;
    }


    public isHandSet: boolean;


    private viewPortCheck() {
        const isSmallScreen = this.breakpoint.isMatched('(max-width: 599px)');

        this.breakpoint.observe([
            Breakpoints.HandsetPortrait

        ]).subscribe(result => {
            // console.log(result);
            if (result.matches) {
                console.warn('item-filter-component-->Detect MOBILE view port');
                this.isHandSet = true;
            } else {
                //  console.warn("Detect MOBILE view port")
                this.isHandSet = false;

            }
        });
    }


    public updateFilter(obj) {
        if (obj) {

            this.filterGroup = obj;
        }

    }


    private relevant: boolean = true;

    public relevanceOrder() {

        this.orderDate.emit(this.relevant);
        this.relevant = !this.relevant;
    }


    public uploadDateOrder() {

    }


    ngOnInit() {

        this.viewPortCheck();

        // this.filterGroup = [
        //     {
        //         name: 'vendor', options: ['vendor a', 'vendor b', 'vendor c', 'vendor d'],
        //     },
        //     {
        //         name: 'fireName', options: ['fire Name1', 'fire Name2', 'fire Name3'],
        //     },
        //     {
        //         name: 'aircraftModel', options: ['FB200', 'model2', 'model2', 'model2', 'model3'],
        //     }
        // ];

    }


    public getChildPickEvent(filterUnitObj: FilterUnitObj) {


        if (filterUnitObj) {

            if (this.isHandSet) {
                this.showMobileMenu();
            } else {
                this.openFilter();
            }


            this.filterItemOnClick.emit(filterUnitObj);
            //  console.log('getChildPickEvent emitting:' );
        }

    }

    public getChildCloseEvent(closefilterItem: FilterUnitObj) {

        if (this.isHandSet) {
            this.showMobileMenu();
        } else {
            this.openFilter();
        }

        this.closeFilterItemOnClick.emit(closefilterItem);
        //   console.warn('getChildCloseEvent emitting-->');
    }

}


