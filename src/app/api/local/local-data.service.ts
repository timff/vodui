import {InMemoryDbService} from 'angular-in-memory-web-api';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const videos = [
            {
                'id': '001',
                'filename': 'blue mountain fire',
                'aircraftType': 'D700',
                'aircraftModel': 'mode s10',
                'year': '2001',
                'airth': '2323',
                'releaseDate': '22/03/2013'
            }, {
                'id': '003',
                'filename': 'mountain fire',
                'aircraftType': 'craft I29',
                'aircraftModel': 'mode D700',
                'year': '2014',
                'airth': '2323',
                'releaseDate': '02/03/2018'
            }, {
                'id': '005',
                'filename': 'northwest fire',
                'aircraftType': 'D700',
                'aircraftModel': 'mode 223',
                'year': '2018',
                'airth': '2323',
                'releaseDate': '02/05/2018'
            }, {
                'id': '007',
                'filename': 'southwest fire',
                'aircraftType': 'craft I29',
                'aircraftModel': 'mode D700 223',
                'year': '2011',
                'airth': '2323',
                'releaseDate': '12/03/2016'
            }, {
                'id': '003',
                'filename': 'mountain fire',
                'aircraftType': 'craft I29',
                'aircraftModel': 'mode D700',
                'year': '2014',
                'airth': '2323',
                'releaseDate': '02/03/2018'
            }, {
                'id': '005',
                'filename': 'northwest fire',
                'aircraftType': 'D700',
                'aircraftModel': 'mode 223',
                'year': '2018',
                'airth': '2323',
                'releaseDate': '02/05/2018'
            }, {
                'id': '007',
                'filename': 'southwest fire',
                'aircraftType': 'craft 33I29',
                'aircraftModel': 'mode D700 223',
                'year': '2011',
                'airth': '2323',
                'releaseDate': '12/03/2016'
            }, {
                'id': '003',
                'filename': 'mountain fire',
                'aircraftType': 'craft I29',
                'aircraftModel': 'mode D700',
                'year': '2014',
                'airth': '2323',
                'releaseDate': '02/03/2018'
            }, {
                'id': '005',
                'filename': 'northwest fire',
                'aircraftType': 'D700',
                'aircraftModel': 'mode 223',
                'year': '2018',
                'airth': '2323',
                'releaseDate': '02/05/2018'
            }, {
                'id': '007',
                'filename': 'southwest fire',
                'aircraftType': 'craft I29',
                'aircraftModel': 'mode D700 223',
                'year': '2011',
                'airth': '2323',
                'releaseDate': '12/03/2016'
            }
        ];
        return {videos};
    }


}


