import { Injectable } from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Observable} from 'rxjs/internal/Observable';
import {VideoData} from '../interface/videos';
import {HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ViewPortService {

  constructor(   private breakpointObserver: BreakpointObserver) { }



     public   viewPort():Observable<any> {

    //  const isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');


     return   this.breakpointObserver.observe([
          Breakpoints.HandsetPortrait
          // Breakpoints.HandsetLandscape,
      ])


  }




    // postVideos(): Observable<VideoData> {
    //     let tokenStr: string = 'Bearer ' + this.auth.accessToken;
    //     let httpOptions2 = {
    //         headers: new HttpHeaders({
    //             'Authorization': tokenStr
    //         })
    //     };
    //     return this.http.get<VideoData>(this.urlConfig.api + 'latest', this.newToken());
    //     // .pipe(
    //     //     catchError(this.handleError('postVideos'))
    //     // );
    // }
    //
    //





}






