import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Video, VideoItem, GuidUrl, VideoData} from '../interface/videos';
import {HttpHeaders} from '@angular/common/http';
import {AuthJsService} from './auth/auth-js.service';
import {catchError} from 'rxjs/operators';

import {HandleError, HttpErrorHandler} from './http-error-handler.service';

import {setting} from '../../config/app-setting';
import {pipe} from 'rxjs/internal-compatibility';


export interface ApiEndPoint {
    api: string;
}


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'rfs-sydney-nsw',
    })
};


const httpOptionsJson = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};




@Injectable({
    providedIn: 'root'
})


export class VideosService {

    private urlConfig: ApiEndPoint;

    private handleError: HandleError;

    constructor(
        @Inject('UrlConfig') private urlConfig2,
        private http: HttpClient,
        private  auth: AuthJsService,
        httpErrorHandler: HttpErrorHandler) {
        this.handleError = httpErrorHandler.createHandleError('videos.service.ts');

        this.urlConfig = setting.apiEndPoint;

    }


    getVideos(): Observable<VideoItem[]> {
        return this.http.get<VideoItem[]>(`${this.urlConfig2.mock_url}/videos`, httpOptions);
    }


    getVideos2(): Observable<VideoItem[]> {
        //   return this.http.get<any>(`${this.urlConfig2.mock_url2}/getvideos` ,httpOptionsJson);


        let str = {'keywords': 'tony'};
        return this.http.post<any>(`${this.urlConfig2.mock_url2}/postvideos`, str, httpOptionsJson);
    }


    private newToken() {
        let tokenStr: string = 'Bearer ' + this.auth.accessToken;
        let newtoken = {
            headers: new HttpHeaders({
                'Authorization': tokenStr
            })
        };
        return newtoken;
    }


    postVideos(): Observable<VideoData> {
        let tokenStr: string = 'Bearer ' + this.auth.accessToken;
        let httpOptions2 = {
            headers: new HttpHeaders({
                'Authorization': tokenStr
            })
        };
        return this.http.get<VideoData>(this.urlConfig.api + 'latest', this.newToken());
        // .pipe(
        //        //  pipe is working
        //     catchError(this.handleError('postVideos'))
        // );

    }


    newLatestVideos(): Observable<any> {
        let tokenStr: string = 'Bearer ' + this.auth.accessToken;
        let httpOptions2 = {
            headers: new HttpHeaders({
                'Authorization': tokenStr
            })
        };
        return this.http.get<any>(this.urlConfig.api + 'category', this.newToken());
        // .pipe(
        //     catchError(this.handleError('postVideos'))
        // );
    }


    videoGetUrl(str: string): Observable<GuidUrl> {
        return this.http.get<any>(str, this.newToken());
    }


    getPreview(str: string): Observable<any> {
        return this.http.get<any>(this.urlConfig.api + 'preview/'+ str, this.newToken());
    }


    doSearch(keyWord: string): Observable<any> {

        let searchStr = {'keywords': keyWord};
        return this.http.post<any>(this.urlConfig.api + 'search', searchStr, this.newToken());
    }




}


