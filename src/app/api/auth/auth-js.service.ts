import {Injectable} from '@angular/core';
import * as auth0 from 'auth0-js';
import {Router} from '@angular/router';
import {setting} from '../../../config/app-setting';
import {interval} from 'rxjs/internal/observable/interval';

(window as any).global = window;

@Injectable({
    providedIn: 'root'
})
export class AuthJsService {

    // Create Auth0 web auth instance
    auth0 = new auth0.WebAuth({
        clientID: setting.auth.clientID,
        domain: setting.auth.domain,
        responseType: 'token',
        redirectUri: setting.auth.redirect,
        audience: setting.auth.audience,
        scope: setting.auth.scope
    });

    // Store authentication data
    expiresAt: number;
    userProfile: any;
    accessToken: string;
    authenticated: boolean;
    isConnected: boolean;

    constructor(private router: Router) {
        this.getAccessToken();

    }

//buildAuthorizeUrl
    getLoginUrl() {

        let myurl = this.auth0.client.buildAuthorizeUrl({
            clientID: 'D9QY5Y3LVpnGhjsVf2YCilHcApnDcjtT', // string
            responseType: 'token id_token', // code
            redirectUri: 'http://localhost:4111/callback',
            nonce: 'YOUR_NONCE'

        });

        console.log(myurl);

    }


    apiLogin() {
        // this.auth0.client.login({
        //   realm: 'tests',
        //   username: 'romabug@yahoo.com.au',
        //   password: 'Romabug331',
        //   scope: 'openid profile',
        //   audience: 'urn:test'
        // });
        this.getLoginUrl();

    }


    login() {
        // Auth0 authorize request
        console.warn('@@@@trying to Deng lu');
        this.auth0.authorize();
    }


    listenToAuth() {
        const testToken = interval(800);
        const subscribe = testToken.subscribe(() => {
            this.checkLoginIn();

            subscribe.unsubscribe();
        });
    }


    //old handleLoginCallback
    checkLoginIn() {

        this.auth0.parseHash((err, authResult) => {

            if (authResult && authResult.accessToken) {
                window.location.hash = '';
                this.getUserInfo(authResult);

            } else if (err) {
                console.error(`handleLoginCallback Error: ${err.error} and redirect`);
                // this.router.navigate(['/contact']);
            }
            else {
                // console.warn(`pls login with user and pass`);
                this.login();
            }
            //  this.router.navigate(['/']);
        });
    }


    handleLoginCallback() {

        if (this.isAuthenticated()) {
            console.log('sesstion not expired... ');

        }
        else {
            this.auth0.parseHash((err, authResult) => {

                if (authResult && authResult.accessToken) {
                    window.location.hash = '';
                    this.getUserInfo(authResult);

                    console.warn('## LOGIN STATUS ');

                    this.router.navigate(['/home']);

                } else if (err) {
                    console.error(`handleLoginCallback Error: ${err.error} and redirect`);

                    //   this.router.navigate(['/home']);
                }
                else {
                    // console.warn(`pls login with user and pass`);
                    this.login();
                }
                //  this.router.navigate(['/']);
            });
        }
    }


    reSendParseHash() {
        this.auth0.parseHash((err, authResult) => {
            if (authResult && authResult.accessToken) {
                window.location.hash = '';
                this.getUserInfo(authResult);
            } else if (err) {
                console.error(`handleLoginCallback Error: ${err.error} and redirect`);
                alert(err.error);
                //  this.router.navigate(['/contact']);
            }
        });
    }


    checkLocalInfo() {
        // console.log( localStorage.getItem())
    }


    getAccessToken() {
        this.isConnected = false;
        this.auth0.checkSession({}, (err, authResult) => {
            if (authResult && authResult.accessToken) {
                this.getUserInfo(authResult);
                this.isConnected = true;
            }
        });
    }

    getUserInfo(authResult) {
        // Use access token to retrieve user's profile and set session
        this.auth0.client.userInfo(authResult.accessToken, (err, profile) => {
            if (profile) {
                this._setSession(authResult, profile);
                let expiresTime: any = this.expiresAt.toString();

                localStorage.setItem('access_token', this.accessToken);
                localStorage.setItem('expires_at', expiresTime);

                // console.log('PROFILES-->');
                // console.log(profile);

                //  console.log(' accessToken -->');
                console.log(this.accessToken);

                console.log(new Date(this.expiresAt));

            }
        });
    }


    private _setSession(authResult, profile) {
        // Save authentication data and update login status subject

        localStorage.setItem('isLoggedIn', 'true');

        this.expiresAt = authResult.expiresIn * 1000 + Date.now();
        this.accessToken = authResult.accessToken;
        this.userProfile = profile;
        this.authenticated = true;
    }

    logout() {

        this.auth0.logout({
            returnTo: setting.logoutRedirect,
            clientID: setting.auth.clientID
        });
        localStorage.removeItem('isLoggedIn');
        localStorage.removeItem('access_token');
        // localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');
        //

    }


    get isLoggedIn(): boolean {
        // Check if current date is before token
        // expiration and user is signed in locally
        return Date.now() < this.expiresAt && this.authenticated;
    }


    get __isAuthenticated(): boolean {
        return this.authenticated;

    }


    public isAuthenticated(): boolean {
        // Check whether the current time is past the
        // Access Token's expiry time
        const expiresAt = JSON.parse(localStorage.getItem('expires_at') || '{}');
        return new Date().getTime() < expiresAt;
    }


}
