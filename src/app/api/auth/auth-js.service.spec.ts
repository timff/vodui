import { TestBed } from '@angular/core/testing';

import { AuthJsService } from './auth-js.service';

describe('AuthJsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthJsService = TestBed.get(AuthJsService);
    expect(service).toBeTruthy();
  });
});
