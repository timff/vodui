import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';



@Component({
    selector: 'app-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

    constructor() {
        //   this.getVideos();
    }

    /** autocomplish */
    myControl = new FormControl();
    options: string[] = ['One', 'Two', 'Three', 'amy', 'aak', 'boom li',
        'clin tom', 'david lim', 'Doll Cai', 'ellim chen', 'emsslldi you', 'eweigth nmae',
        'Elympic tai', 'email hou', 'ecdim slky', 'Evkkk, qing', 'eiikka ckla', 'eysksk ccc', 'ennksk, aakk', 'eiuuajk kks kk'];

    filteredOptions: Observable<string[]>;

    ngOnInit() {
        this.filteredOptions = this.myControl.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.options.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    }



}
