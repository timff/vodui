import {Component, ElementRef, OnInit} from '@angular/core';
import {CdkScrollable, ScrollDispatcher} from '@angular/cdk/scrolling';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    constructor(private scrollDispatcher: ScrollDispatcher,
                private elementRef: ElementRef) {
    }


    ngOnInit() {

        //new method for the scrolling..OK
        //   this.scrollDispatcher.scrolled()
        //       .subscribe(event=> console.log('you have scrolled'))


        console.log(this.scrollDispatcher.scrollContainers);
        this.scrollDispatcher.scrolled(10000).subscribe((scrollable: CdkScrollable) => {
            console.log(' it is scrolling, it came from...');
            // console.log(scrollable.getElementRef());
        });

// for ancestorScrolled
        // this.scrollDispatcher.ancestorScrolled(this.elementRef, 10).subscribe((scrollable: CdkScrollable) => {
        //     console.log(scrollable.getElementRef());
        // });


    }


}
