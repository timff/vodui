import {Directive, HostListener, Output, EventEmitter} from '@angular/core';

@Directive({
    selector: '[appScrollTracker]'
})
export class ScrollTrackerDirective {

    constructor() {
    }

    @Output() scrolled = new EventEmitter<any>();

    @HostListener('scroll', ['$event'])
    onScroll(event) {

        let tracker = event.target;
        let endReached = false;
        let limit = tracker.scrollHeight - tracker.clientHeight;

        console.log(event.target.scrollTop, limit);
        if (event.target.scrollTop === limit) {
            //alert('end reached');
            endReached = true;
        }

        this.scrolled.emit({
            pos: event.target.scrollTop,
            endReached
        });
    }

}
