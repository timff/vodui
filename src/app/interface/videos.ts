export interface GuidUrl {
    hlsUrl: string;
}


export interface VideoItem {
    guid: string;
    fireName: string;
    fileName: string;
    aircraftType: string;
    aircraftModel: string;
    vendor: string;
    duration: string;
    snapshotUrl: string;
    thumbnailUrl: string;
    date: string;
    year?: string;
    month?: string;
    time?: number;
}


export interface VideoData {
    indexes: any;
    videos: VideoItem[]
}



export interface Video {
    id: number;
    title: string;
    upload_date: string;
    video_size?: number;
    area_code?: string;
    status: string;
}


export interface T_Video {
    id: string;
    aircraftModel: string;
    aircraftType: string;
    filename: string;
    guid: string;
    hlsUrl: string;
    month: string;
    year: string;
}

