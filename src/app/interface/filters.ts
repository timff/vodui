export interface FilterUnitObj {
    name: string;
    option: string;
}

export interface VideoFilter {
    name: string;
    options: string[];
}

