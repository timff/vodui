import {Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {timer} from 'rxjs';
import {FilterUnitObj} from './lib/class';
import {Meta} from '@angular/platform-browser';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Router} from '@angular/router';
import {AuthJsService} from './api/auth/auth-js.service';
import {createUrlResolverWithoutPackagePrefix} from '@angular/compiler';
import {interval} from 'rxjs/internal/observable/interval';
// import * as myjs  from '../assets/js/js.js';

import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';



@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [Location, {provide: LocationStrategy, useClass: PathLocationStrategy}]

})


export class AppComponent implements OnInit {


    @ViewChild('viewSearchResult')
    public viewSearchResult;

    @ViewChild('videoPlayer')
    public videoPlayer;

    @ViewChild('itemFilter')
    private itemFilter;




    constructor(
        private meta: Meta,
        private breakpointObserver: BreakpointObserver,
        public login: AuthJsService,
        private location: Location,

    ) {  }


    private thisViewPort = 410;

    @HostListener('window:resize', ['$event'])
    private onResize(event?) {
        // this.thisViewPort = window.innerWidth;
        return window.innerWidth;
    }

    breakPointCheck() {
        //  const isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');

        this.breakpointObserver.observe([
            Breakpoints.HandsetPortrait
            // Breakpoints.HandsetLandscape,
        ]).subscribe(result => {

            console.log(result);

            if (result.matches) {
                console.log(result);
                this.thisViewPort = 414;
            }
        });
    }


    updateViewPort() {
        let viewport = this.meta.getTag('name=viewport');
        let content = `width=${this.thisViewPort}`;

        this.meta.addTag({name: 'viewport', content: content});
        this.meta.addTag({name: 'author', content: 'hanchangcai@gmail.com'});
        console.log('set viewport  ---' + content);


    }


    handleScroll(event) {

        console.log(event);

       this.videoPlayer.setScrollState(event);

    }




    checkViewPort() {
        console.log(this.thisViewPort);
    }

    ngOnDestroy() {
        // this.login.logout();
    }


    setPath() {
        console.log(this.location.path());
        this.location.go('index');

    }


    ngOnInit() {
        //   this.login.listenToAuth();
        this.login.handleLoginCallback();


    }


    checkLoginStatus() {
        let testToken = interval(1200);
        let subscribe = testToken.subscribe(() => {

            if (this.login.authenticated) {
                console.warn('TOKEN WIN');

            } else {
                console.warn('TOKEN NULL');
            }

            subscribe.unsubscribe();


        });

    }


    latestVideos(arg: boolean) {

        this.videoPlayer.closeVideoArea();

        if (arg) {
            this.viewSearchResult.refreshLatest();
        }


    }


    showKeyword(keyword: string) {
        this.viewSearchResult.launchSearch(keyword);
    }


    private allFilterObjs: FilterUnitObj[] = [];

    public collectFilterObj(filterUnitObj: FilterUnitObj) {

        this.allFilterObjs.push(filterUnitObj);

        let tmpFilter: any = this.allFilterObjs.filter(value => value.name !== filterUnitObj.name);
        tmpFilter.push(filterUnitObj);

        this.allFilterObjs = tmpFilter;


        // #### when the below view child function run, it block the close item function
        this.viewSearchResult.handleMutiFilter(this.allFilterObjs);


    }


    public launchVideo(obj: any) {
        if (obj) {

            this.videoPlayer.initVideo(obj);
            this.videoPlayer.popUp();
            console.log(obj);
        }
    }


    public initFilterGroup(obj: any) {

        this.itemFilter.updateFilter(obj);

    }


    public sortByDate(event) {
        this.viewSearchResult.sortVideoList(event);
    }


    public updateFilterObj(filterUnitObj: FilterUnitObj) {

        this.allFilterObjs = this.allFilterObjs.filter(value => value.name !== filterUnitObj.name);


        const nullFilter = [{name: 'empty', option: 'empty'}];

        if (this.allFilterObjs.length === 0) {

            this.viewSearchResult.handleMutiFilter(nullFilter);
        } else {
            this.viewSearchResult.handleMutiFilter(this.allFilterObjs);
        }


    }





    tryApiLogin() {
        this.login.apiLogin();
    }


    runLogIn() {
        this.login.login();

    }

    runLogOut() {
        this.login.logout();
    }


}



