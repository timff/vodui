import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {SearchComponent} from './search/search.component';
import {SearchResultComponent} from './list/search-result/search-result.component';
import {MaterialComponent} from './directive/material/material.component';
import {LoginComponent} from './login/login/login.component';
import {TableComponent} from './list/table/table.component';
 import {CallbackComponent} from './auth/callback/callback.component';
import {AppComponent} from './app.component';


// { path: '', component: HomeComponent },
// { path: 'callback', component: CallbackComponent },
// { path: '**', redirectTo: '' }


const routes: Routes = [
    {
        path: '',
        component: AppComponent,
    },

    {
        path: 'video-search', component: SearchComponent,
    },
    {
        path: 'video-result', component: SearchResultComponent,
    },


    //
    // {
    //     path: 'material', component: MaterialComponent,
    // }
    // ,
    // {
    //     path: '**', component: SearchResultComponent,
    // },

    {path: '**', redirectTo: ''}

];


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}



