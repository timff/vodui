"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var core_1 = require("@angular/core");
var vg_player_1 = require("./vg-player");
var vg_api_1 = require("../services/vg-api");
var vg_fullscreen_api_1 = require("../services/vg-fullscreen-api");
describe('Videogular Player', function () {
    var player;
    var ref;
    var api;
    var fsAPI;
    var controlsHidden;
    beforeEach(function () {
        ref = {
            nativeElement: {
                querySelectorAll: function () {
                    return [{}];
                }
            }
        };
        controlsHidden = {
            isHidden: {
                subscribe: function () { }
            }
        };
        api = new vg_api_1.VgAPI();
        fsAPI = new vg_fullscreen_api_1.VgFullscreenAPI();
        player = new vg_player_1.VgPlayer(ref, api, fsAPI, controlsHidden);
    });
    it('Should handle native fullscreen', function () {
        fsAPI.nativeFullscreen = true;
        player.onChangeFullscreen(true);
        expect(player.isFullscreen).toBeFalsy();
    });
    it('Should handle emulated fullscreen enabled', function () {
        fsAPI.nativeFullscreen = false;
        player.onChangeFullscreen(true);
        expect(player.isFullscreen).toBeTruthy();
        expect(player.zIndex).toBe('1');
    });
    it('Should handle emulated fullscreen enabled', function () {
        fsAPI.nativeFullscreen = false;
        player.onChangeFullscreen(false);
        expect(player.isFullscreen).toBeFalsy();
        expect(player.zIndex).toBe('auto');
    });
});
describe('Videogular Player', function () {
    var builder;
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [VgPlayerTest, vg_player_1.VgPlayer]
        });
    });
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.compileComponents();
    }));
    it('Should create a VgPlayer component', testing_1.async(function () {
        var fixture = testing_1.TestBed.createComponent(VgPlayerTest);
        fixture.detectChanges();
        var compiled = fixture.debugElement.nativeElement;
        var video = compiled.querySelector('video');
        expect(video.controls).toBe(true);
    }));
});
var VgPlayerTest = /** @class */ (function () {
    function VgPlayerTest() {
    }
    VgPlayerTest.decorators = [
        { type: core_1.Component, args: [{
                    template: "\n        <vg-player>\n            <video vg-media id=\"singleVideo\" preload=\"auto\" controls>\n                <source src=\"http://static.videogular.com/assets/videos/videogular.mp4\" type=\"video/mp4\">\n                <source src=\"http://static.videogular.com/assets/videos/videogular.ogg\" type=\"video/ogg\">\n                <source src=\"http://static.videogular.com/assets/videos/videogular.webm\" type=\"video/webm\">\n            </video>\n        </vg-player>\n    ",
                    providers: [vg_api_1.VgAPI]
                },] },
    ];
    return VgPlayerTest;
}());

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmctcGxheWVyLnNwZWMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2Zy1wbGF5ZXIuc3BlYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLGlEQUE2RDtBQUM3RCxzQ0FBd0M7QUFDeEMseUNBQXFDO0FBRXJDLDZDQUF5QztBQUN6QyxtRUFBOEQ7QUFJOUQsUUFBUSxDQUFDLG1CQUFtQixFQUFFO0lBQzFCLElBQUksTUFBZSxDQUFDO0lBQ3BCLElBQUksR0FBYyxDQUFDO0lBQ25CLElBQUksR0FBUyxDQUFDO0lBQ2QsSUFBSSxLQUFxQixDQUFDO0lBQzFCLElBQUksY0FBK0IsQ0FBQztJQUVwQyxVQUFVLENBQUM7UUFDUCxHQUFHLEdBQUc7WUFDRixhQUFhLEVBQUU7Z0JBQ1gsZ0JBQWdCLEVBQUU7b0JBQ2QsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2hCLENBQUM7YUFDSjtTQUNKLENBQUM7UUFFRixjQUFjLEdBQUc7WUFDYixRQUFRLEVBQUU7Z0JBQ04sU0FBUyxFQUFFLGNBQU8sQ0FBQzthQUN0QjtTQUNnQixDQUFDO1FBRXRCLEdBQUcsR0FBRyxJQUFJLGNBQUssRUFBRSxDQUFDO1FBQ2xCLEtBQUssR0FBRyxJQUFJLG1DQUFlLEVBQUUsQ0FBQztRQUM5QixNQUFNLEdBQUcsSUFBSSxvQkFBUSxDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLGNBQWMsQ0FBQyxDQUFDO0lBQzNELENBQUMsQ0FBQyxDQUFDO0lBRUgsRUFBRSxDQUFDLGlDQUFpQyxFQUFFO1FBQ2xDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFFOUIsTUFBTSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWhDLE1BQU0sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDNUMsQ0FBQyxDQUFDLENBQUM7SUFFSCxFQUFFLENBQUMsMkNBQTJDLEVBQUU7UUFDNUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUUvQixNQUFNLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFaEMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN6QyxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNwQyxDQUFDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQywyQ0FBMkMsRUFBRTtRQUM1QyxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBRS9CLE1BQU0sQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVqQyxNQUFNLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3hDLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUM7QUFFSCxRQUFRLENBQUMsbUJBQW1CLEVBQUU7SUFDMUIsSUFBSSxPQUFPLENBQUM7SUFFWixVQUFVLENBQUM7UUFDUCxpQkFBTyxDQUFDLHNCQUFzQixDQUFDO1lBQzNCLFlBQVksRUFBRSxDQUFDLFlBQVksRUFBRSxvQkFBUSxDQUFDO1NBQ3pDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBRUgsVUFBVSxDQUFDLGVBQUssQ0FBQztRQUNiLGlCQUFPLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUNoQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRUosRUFBRSxDQUFDLG9DQUFvQyxFQUNuQyxlQUFLLENBQUM7UUFDRixJQUFJLE9BQU8sR0FBRyxpQkFBTyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNwRCxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDeEIsSUFBSSxRQUFRLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7UUFDbEQsSUFBSSxLQUFLLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUU1QyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN0QyxDQUFDLENBQUMsQ0FDTCxDQUFDO0FBQ04sQ0FBQyxDQUFDLENBQUM7QUFFSDtJQUFBO0lBWW9CLENBQUM7O2dCQVpwQixnQkFBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxtZUFRVDtvQkFDRCxTQUFTLEVBQUUsQ0FBQyxjQUFLLENBQUM7aUJBQ3JCOztJQUNtQixtQkFBQztDQUFBLEFBWnJCLElBWXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHthc3luYywgaW5qZWN0LCBUZXN0QmVkfSBmcm9tIFwiQGFuZ3VsYXIvY29yZS90ZXN0aW5nXCI7XG5pbXBvcnQge0NvbXBvbmVudH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7VmdQbGF5ZXJ9IGZyb20gXCIuL3ZnLXBsYXllclwiO1xuaW1wb3J0IHtWZ01lZGlhfSBmcm9tIFwiLi4vdmctbWVkaWEvdmctbWVkaWFcIjtcbmltcG9ydCB7VmdBUEl9IGZyb20gXCIuLi9zZXJ2aWNlcy92Zy1hcGlcIjtcbmltcG9ydCB7VmdGdWxsc2NyZWVuQVBJfSBmcm9tIFwiLi4vc2VydmljZXMvdmctZnVsbHNjcmVlbi1hcGlcIjtcbmltcG9ydCB7RWxlbWVudFJlZn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFZnQ29udHJvbHNIaWRkZW4gfSBmcm9tICcuLi9zZXJ2aWNlcy92Zy1jb250cm9scy1oaWRkZW4nO1xuXG5kZXNjcmliZSgnVmlkZW9ndWxhciBQbGF5ZXInLCAoKSA9PiB7XG4gICAgbGV0IHBsYXllcjpWZ1BsYXllcjtcbiAgICBsZXQgcmVmOkVsZW1lbnRSZWY7XG4gICAgbGV0IGFwaTpWZ0FQSTtcbiAgICBsZXQgZnNBUEk6VmdGdWxsc2NyZWVuQVBJO1xuICAgIGxldCBjb250cm9sc0hpZGRlbjpWZ0NvbnRyb2xzSGlkZGVuO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICAgIHJlZiA9IHtcbiAgICAgICAgICAgIG5hdGl2ZUVsZW1lbnQ6IHtcbiAgICAgICAgICAgICAgICBxdWVyeVNlbGVjdG9yQWxsOiAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbe31dO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcblxuICAgICAgICBjb250cm9sc0hpZGRlbiA9IHtcbiAgICAgICAgICAgIGlzSGlkZGVuOiB7XG4gICAgICAgICAgICAgICAgc3Vic2NyaWJlOiAoKSA9PiB7fVxuICAgICAgICAgICAgfVxuICAgICAgICB9IGFzIFZnQ29udHJvbHNIaWRkZW47XG5cbiAgICAgICAgYXBpID0gbmV3IFZnQVBJKCk7XG4gICAgICAgIGZzQVBJID0gbmV3IFZnRnVsbHNjcmVlbkFQSSgpO1xuICAgICAgICBwbGF5ZXIgPSBuZXcgVmdQbGF5ZXIocmVmLCBhcGksIGZzQVBJLCBjb250cm9sc0hpZGRlbik7XG4gICAgfSk7XG5cbiAgICBpdCgnU2hvdWxkIGhhbmRsZSBuYXRpdmUgZnVsbHNjcmVlbicsICgpID0+IHtcbiAgICAgICAgZnNBUEkubmF0aXZlRnVsbHNjcmVlbiA9IHRydWU7XG5cbiAgICAgICAgcGxheWVyLm9uQ2hhbmdlRnVsbHNjcmVlbih0cnVlKTtcblxuICAgICAgICBleHBlY3QocGxheWVyLmlzRnVsbHNjcmVlbikudG9CZUZhbHN5KCk7XG4gICAgfSk7XG5cbiAgICBpdCgnU2hvdWxkIGhhbmRsZSBlbXVsYXRlZCBmdWxsc2NyZWVuIGVuYWJsZWQnLCAoKSA9PiB7XG4gICAgICAgIGZzQVBJLm5hdGl2ZUZ1bGxzY3JlZW4gPSBmYWxzZTtcblxuICAgICAgICBwbGF5ZXIub25DaGFuZ2VGdWxsc2NyZWVuKHRydWUpO1xuXG4gICAgICAgIGV4cGVjdChwbGF5ZXIuaXNGdWxsc2NyZWVuKS50b0JlVHJ1dGh5KCk7XG4gICAgICAgIGV4cGVjdChwbGF5ZXIuekluZGV4KS50b0JlKCcxJyk7XG4gICAgfSk7XG5cbiAgICBpdCgnU2hvdWxkIGhhbmRsZSBlbXVsYXRlZCBmdWxsc2NyZWVuIGVuYWJsZWQnLCAoKSA9PiB7XG4gICAgICAgIGZzQVBJLm5hdGl2ZUZ1bGxzY3JlZW4gPSBmYWxzZTtcblxuICAgICAgICBwbGF5ZXIub25DaGFuZ2VGdWxsc2NyZWVuKGZhbHNlKTtcblxuICAgICAgICBleHBlY3QocGxheWVyLmlzRnVsbHNjcmVlbikudG9CZUZhbHN5KCk7XG4gICAgICAgIGV4cGVjdChwbGF5ZXIuekluZGV4KS50b0JlKCdhdXRvJyk7XG4gICAgfSk7XG59KTtcblxuZGVzY3JpYmUoJ1ZpZGVvZ3VsYXIgUGxheWVyJywgKCkgPT4ge1xuICAgIGxldCBidWlsZGVyO1xuXG4gICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICAgIFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgICAgICAgICBkZWNsYXJhdGlvbnM6IFtWZ1BsYXllclRlc3QsIFZnUGxheWVyXVxuICAgICAgICB9KTtcbiAgICB9KTtcblxuICAgIGJlZm9yZUVhY2goYXN5bmMoKCkgPT4ge1xuICAgICAgICBUZXN0QmVkLmNvbXBpbGVDb21wb25lbnRzKCk7XG4gICAgfSkpO1xuXG4gICAgaXQoJ1Nob3VsZCBjcmVhdGUgYSBWZ1BsYXllciBjb21wb25lbnQnLFxuICAgICAgICBhc3luYygoKSA9PiB7XG4gICAgICAgICAgICBsZXQgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KFZnUGxheWVyVGVzdCk7XG4gICAgICAgICAgICBmaXh0dXJlLmRldGVjdENoYW5nZXMoKTtcbiAgICAgICAgICAgIGxldCBjb21waWxlZCA9IGZpeHR1cmUuZGVidWdFbGVtZW50Lm5hdGl2ZUVsZW1lbnQ7XG4gICAgICAgICAgICBsZXQgdmlkZW8gPSBjb21waWxlZC5xdWVyeVNlbGVjdG9yKCd2aWRlbycpO1xuXG4gICAgICAgICAgICBleHBlY3QodmlkZW8uY29udHJvbHMpLnRvQmUodHJ1ZSk7XG4gICAgICAgIH0pXG4gICAgKTtcbn0pO1xuXG5AQ29tcG9uZW50KHtcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8dmctcGxheWVyPlxuICAgICAgICAgICAgPHZpZGVvIHZnLW1lZGlhIGlkPVwic2luZ2xlVmlkZW9cIiBwcmVsb2FkPVwiYXV0b1wiIGNvbnRyb2xzPlxuICAgICAgICAgICAgICAgIDxzb3VyY2Ugc3JjPVwiaHR0cDovL3N0YXRpYy52aWRlb2d1bGFyLmNvbS9hc3NldHMvdmlkZW9zL3ZpZGVvZ3VsYXIubXA0XCIgdHlwZT1cInZpZGVvL21wNFwiPlxuICAgICAgICAgICAgICAgIDxzb3VyY2Ugc3JjPVwiaHR0cDovL3N0YXRpYy52aWRlb2d1bGFyLmNvbS9hc3NldHMvdmlkZW9zL3ZpZGVvZ3VsYXIub2dnXCIgdHlwZT1cInZpZGVvL29nZ1wiPlxuICAgICAgICAgICAgICAgIDxzb3VyY2Ugc3JjPVwiaHR0cDovL3N0YXRpYy52aWRlb2d1bGFyLmNvbS9hc3NldHMvdmlkZW9zL3ZpZGVvZ3VsYXIud2VibVwiIHR5cGU9XCJ2aWRlby93ZWJtXCI+XG4gICAgICAgICAgICA8L3ZpZGVvPlxuICAgICAgICA8L3ZnLXBsYXllcj5cbiAgICBgLFxuICAgIHByb3ZpZGVyczogW1ZnQVBJXVxufSlcbmNsYXNzIFZnUGxheWVyVGVzdCB7fVxuIl19