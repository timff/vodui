"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var VgStates = /** @class */ (function () {
    function VgStates() {
    }
    VgStates.VG_ENDED = 'ended';
    VgStates.VG_PAUSED = 'paused';
    VgStates.VG_PLAYING = 'playing';
    VgStates.VG_LOADING = 'waiting';
    VgStates.decorators = [
        { type: core_1.Injectable },
    ];
    return VgStates;
}());
exports.VgStates = VgStates;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmctc3RhdGVzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmctc3RhdGVzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlDO0FBRXpDO0lBQUE7SUFNQSxDQUFDO0lBSlUsaUJBQVEsR0FBVyxPQUFPLENBQUM7SUFDM0Isa0JBQVMsR0FBVyxRQUFRLENBQUM7SUFDN0IsbUJBQVUsR0FBVyxTQUFTLENBQUM7SUFDL0IsbUJBQVUsR0FBVyxTQUFTLENBQUM7O2dCQUx6QyxpQkFBVTs7SUFNWCxlQUFDO0NBQUEsQUFORCxJQU1DO0FBTFksNEJBQVEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgVmdTdGF0ZXMge1xuICAgIHN0YXRpYyBWR19FTkRFRDogc3RyaW5nID0gJ2VuZGVkJztcbiAgICBzdGF0aWMgVkdfUEFVU0VEOiBzdHJpbmcgPSAncGF1c2VkJztcbiAgICBzdGF0aWMgVkdfUExBWUlORzogc3RyaW5nID0gJ3BsYXlpbmcnO1xuICAgIHN0YXRpYyBWR19MT0FESU5HOiBzdHJpbmcgPSAnd2FpdGluZyc7XG59XG4iXX0=