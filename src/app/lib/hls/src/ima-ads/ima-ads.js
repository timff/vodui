"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var vg_ima_ads_1 = require("./vg-ima-ads");
var VgImaAdsModule = /** @class */ (function () {
    function VgImaAdsModule() {
    }
    VgImaAdsModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [common_1.CommonModule],
                    declarations: [
                        vg_ima_ads_1.VgImaAds
                    ],
                    exports: [
                        vg_ima_ads_1.VgImaAds
                    ]
                },] },
    ];
    return VgImaAdsModule;
}());
exports.VgImaAdsModule = VgImaAdsModule;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1hLWFkcy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImltYS1hZHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxzQ0FBOEM7QUFDOUMsMENBQStDO0FBRS9DLDJDQUFzQztBQUV0QztJQUFBO0lBUzZCLENBQUM7O2dCQVQ3QixlQUFRLFNBQUM7b0JBQ04sT0FBTyxFQUFFLENBQUUscUJBQVksQ0FBRTtvQkFDekIsWUFBWSxFQUFFO3dCQUNWLHFCQUFRO3FCQUNYO29CQUNELE9BQU8sRUFBRTt3QkFDTCxxQkFBUTtxQkFDWDtpQkFDSjs7SUFDNEIscUJBQUM7Q0FBQSxBQVQ5QixJQVM4QjtBQUFqQix3Q0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gICAgICBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5cbmltcG9ydCB7VmdJbWFBZHN9IGZyb20gJy4vdmctaW1hLWFkcyc7XG5cbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogWyBDb21tb25Nb2R1bGUgXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtcbiAgICAgICAgVmdJbWFBZHNcbiAgICBdLFxuICAgIGV4cG9ydHM6IFtcbiAgICAgICAgVmdJbWFBZHNcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIFZnSW1hQWRzTW9kdWxlIHt9XG4iXX0=