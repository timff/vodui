"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var vg_api_1 = require("../../core/services/vg-api");
var VgHLS = /** @class */ (function () {
    function VgHLS(ref, API) {
        this.ref = ref;
        this.API = API;
        this.onGetBitrates = new core_1.EventEmitter();
        this.subscriptions = [];
    }
    VgHLS.prototype.ngOnInit = function () {
        var _this = this;
        if (this.API.isPlayerReady) {
            this.onPlayerReady();
        }
        else {
            this.subscriptions.push(this.API.playerReadyEvent.subscribe(function () { return _this.onPlayerReady(); }));
        }
    };
    VgHLS.prototype.onPlayerReady = function () {
        var _this = this;
        this.crossorigin = this.ref.nativeElement.getAttribute('crossorigin');
        this.preload = this.ref.nativeElement.getAttribute('preload') !== 'none';
        this.vgFor = this.ref.nativeElement.getAttribute('vgFor');
        this.target = this.API.getMediaById(this.vgFor);
        this.config = {
            autoStartLoad: this.preload
        };
        if (this.crossorigin === 'use-credentials') {
            this.config.xhrSetup = function (xhr, url) {
                // Send cookies
                xhr.withCredentials = true;
            };
        }
        this.createPlayer();
        if (!this.preload) {
            this.subscriptions.push(this.API.subscriptions.play.subscribe(function () {
                if (_this.hls) {
                    _this.hls.startLoad(0);
                }
            }));
        }
    };
    VgHLS.prototype.ngOnChanges = function (changes) {
        if (changes['vgHls'] && changes['vgHls'].currentValue) {
            this.createPlayer();
        }
        else {
            this.destroyPlayer();
        }
    };
    VgHLS.prototype.createPlayer = function () {
        var _this = this;
        if (this.hls) {
            this.destroyPlayer();
        }
        // It's a HLS source
        if (this.vgHls && this.vgHls.indexOf('.m3u8') > -1 && Hls.isSupported()) {
            var video = this.ref.nativeElement;
            this.hls = new Hls(this.config);
            this.hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
                var videoList = [];
                videoList.push({
                    qualityIndex: 0,
                    width: 0,
                    height: 0,
                    bitrate: 0,
                    mediaType: 'video',
                    label: 'AUTO'
                });
                data.levels.forEach(function (item, index) {
                    videoList.push({
                        qualityIndex: ++index,
                        width: item.width,
                        height: item.height,
                        bitrate: item.bitrate,
                        mediaType: 'video',
                        label:  item.width + "x" + item.height
                    });
                });
                _this.onGetBitrates.emit(videoList);
            });
            this.hls.loadSource(this.vgHls);
            this.hls.attachMedia(video);
        }
        else {
            if (this.target && !!this.target.pause) {
                this.target.pause();
                this.target.seekTime(0);
                this.ref.nativeElement.src = this.vgHls;
            }
        }
    };
    VgHLS.prototype.setBitrate = function (bitrate) {
        if (this.hls) {
            this.hls.nextLevel = bitrate.qualityIndex - 1;
        }
    };
    VgHLS.prototype.destroyPlayer = function () {
        if (this.hls) {
            this.hls.destroy();
            this.hls = null;
        }
    };
    VgHLS.prototype.ngOnDestroy = function () {
        this.subscriptions.forEach(function (s) { return s.unsubscribe(); });
        this.destroyPlayer();
    };
    VgHLS.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[vgHls]',
                    exportAs: 'vgHls'
                },] },
    ];
    /** @nocollapse */
    VgHLS.ctorParameters = function () { return [
        { type: core_1.ElementRef },
        { type: vg_api_1.VgAPI }
    ]; };
    VgHLS.propDecorators = {
        vgHls: [{ type: core_1.Input }],
        onGetBitrates: [{ type: core_1.Output }]
    };
    return VgHLS;
}());
exports.VgHLS = VgHLS;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmctaGxzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmctaGxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBVXVCO0FBQ3ZCLHFEQUFtRDtBQU9uRDtJQWtCSSxlQUFvQixHQUFjLEVBQVMsR0FBUztRQUFoQyxRQUFHLEdBQUgsR0FBRyxDQUFXO1FBQVMsUUFBRyxHQUFILEdBQUcsQ0FBTTtRQVgxQyxrQkFBYSxHQUFrQyxJQUFJLG1CQUFZLEVBQUUsQ0FBQztRQVM1RSxrQkFBYSxHQUFtQixFQUFFLENBQUM7SUFFb0IsQ0FBQztJQUV4RCx3QkFBUSxHQUFSO1FBQUEsaUJBT0M7UUFORyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDekIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLENBQUM7UUFDRCxJQUFJLENBQUMsQ0FBQztZQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsYUFBYSxFQUFFLEVBQXBCLENBQW9CLENBQUMsQ0FBQyxDQUFDO1FBQzdGLENBQUM7SUFDTCxDQUFDO0lBRUQsNkJBQWEsR0FBYjtRQUFBLGlCQThCQztRQTdCRyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsS0FBSyxNQUFNLENBQUM7UUFDekUsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFaEQsSUFBSSxDQUFDLE1BQU0sR0FBZTtZQUN0QixhQUFhLEVBQUUsSUFBSSxDQUFDLE9BQU87U0FDOUIsQ0FBQztRQUVGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFHLFVBQUMsR0FBRyxFQUFFLEdBQUc7Z0JBQzVCLGVBQWU7Z0JBQ2YsR0FBRyxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7WUFDL0IsQ0FBQyxDQUFDO1FBQ04sQ0FBQztRQUVELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVwQixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1lBQ2hCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUNqQztnQkFDSSxFQUFFLENBQUMsQ0FBQyxLQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDWCxLQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDMUIsQ0FBQztZQUNMLENBQUMsQ0FDSixDQUNKLENBQUM7UUFDTixDQUFDO0lBQ0wsQ0FBQztJQUVELDJCQUFXLEdBQVgsVUFBWSxPQUFxQjtRQUM3QixFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3hCLENBQUM7UUFDRCxJQUFJLENBQUMsQ0FBQztZQUNGLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN6QixDQUFDO0lBQ0wsQ0FBQztJQUVELDRCQUFZLEdBQVo7UUFBQSxpQkFnREM7UUEvQ0csRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDWCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDekIsQ0FBQztRQUVELG9CQUFvQjtRQUNwQixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdEUsSUFBSSxLQUFLLEdBQW9CLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDO1lBRXBELElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUNQLEdBQUcsQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUMxQixVQUFDLEtBQUssRUFBRSxJQUFJO2dCQUNSLElBQU0sU0FBUyxHQUFHLEVBQUUsQ0FBQztnQkFFckIsU0FBUyxDQUFDLElBQUksQ0FBQztvQkFDWCxZQUFZLEVBQUUsQ0FBQztvQkFDZixLQUFLLEVBQUUsQ0FBQztvQkFDUixNQUFNLEVBQUUsQ0FBQztvQkFDVCxPQUFPLEVBQUUsQ0FBQztvQkFDVixTQUFTLEVBQUUsT0FBTztvQkFDbEIsS0FBSyxFQUFFLE1BQU07aUJBQ2hCLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUksRUFBRSxLQUFLO29CQUM1QixTQUFTLENBQUMsSUFBSSxDQUFDO3dCQUNYLFlBQVksRUFBRSxFQUFFLEtBQUs7d0JBQ3JCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzt3QkFDakIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO3dCQUNuQixPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU87d0JBQ3JCLFNBQVMsRUFBRSxPQUFPO3dCQUNsQixLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUk7cUJBQ25CLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQztnQkFFSCxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN2QyxDQUFDLENBQ0osQ0FBQztZQUNGLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoQyxDQUFDO1FBQ0QsSUFBSSxDQUFDLENBQUM7WUFDRixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN4QixJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUM1QyxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRCwwQkFBVSxHQUFWLFVBQVcsT0FBc0I7UUFDN0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDWCxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztRQUNsRCxDQUFDO0lBQ0wsQ0FBQztJQUVELDZCQUFhLEdBQWI7UUFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNYLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7UUFDcEIsQ0FBQztJQUNMLENBQUM7SUFFRCwyQkFBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQWYsQ0FBZSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7O2dCQXhJSixnQkFBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxTQUFTO29CQUNuQixRQUFRLEVBQUUsT0FBTztpQkFDcEI7Ozs7Z0JBbkJHLGlCQUFVO2dCQVNMLGNBQUs7Ozt3QkFZVCxZQUFLO2dDQUVMLGFBQU07O0lBa0lYLFlBQUM7Q0FBQSxBQXpJRCxJQXlJQztBQXJJWSxzQkFBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XG4gICAgRGlyZWN0aXZlLFxuICAgIEVsZW1lbnRSZWYsXG4gICAgSW5wdXQsXG4gICAgU2ltcGxlQ2hhbmdlcyxcbiAgICBPbkNoYW5nZXMsXG4gICAgT25EZXN0cm95LFxuICAgIE9uSW5pdCxcbiAgICBPdXRwdXQsXG4gICAgRXZlbnRFbWl0dGVyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBWZ0FQSSB9IGZyb20gXCIuLi8uLi9jb3JlL3NlcnZpY2VzL3ZnLWFwaVwiO1xuaW1wb3J0IHsgSUhMU0NvbmZpZyB9IGZyb20gJy4vaGxzLWNvbmZpZyc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEJpdHJhdGVPcHRpb24gfSBmcm9tICcuLi8uLi9jb3JlL2NvcmUnO1xuXG5kZWNsYXJlIGxldCBIbHM7XG5cbkBEaXJlY3RpdmUoe1xuICAgIHNlbGVjdG9yOiAnW3ZnSGxzXScsXG4gICAgZXhwb3J0QXM6ICd2Z0hscydcbn0pXG5leHBvcnQgY2xhc3MgVmdITFMgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcywgT25EZXN0cm95IHtcbiAgICBASW5wdXQoKSB2Z0hsczpzdHJpbmc7XG5cbiAgICBAT3V0cHV0KCkgb25HZXRCaXRyYXRlczogRXZlbnRFbWl0dGVyPEJpdHJhdGVPcHRpb25bXT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICB2Z0Zvcjogc3RyaW5nO1xuICAgIHRhcmdldDogYW55O1xuICAgIGhsczogYW55O1xuICAgIHByZWxvYWQ6IGJvb2xlYW47XG4gICAgY3Jvc3NvcmlnaW46IHN0cmluZztcbiAgICBjb25maWc6IElITFNDb25maWc7XG5cbiAgICBzdWJzY3JpcHRpb25zOiBTdWJzY3JpcHRpb25bXSA9IFtdO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByZWY6RWxlbWVudFJlZiwgcHVibGljIEFQSTpWZ0FQSSkge31cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBpZiAodGhpcy5BUEkuaXNQbGF5ZXJSZWFkeSkge1xuICAgICAgICAgICAgdGhpcy5vblBsYXllclJlYWR5KCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaCh0aGlzLkFQSS5wbGF5ZXJSZWFkeUV2ZW50LnN1YnNjcmliZSgoKSA9PiB0aGlzLm9uUGxheWVyUmVhZHkoKSkpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25QbGF5ZXJSZWFkeSgpIHtcbiAgICAgICAgdGhpcy5jcm9zc29yaWdpbiA9IHRoaXMucmVmLm5hdGl2ZUVsZW1lbnQuZ2V0QXR0cmlidXRlKCdjcm9zc29yaWdpbicpO1xuICAgICAgICB0aGlzLnByZWxvYWQgPSB0aGlzLnJlZi5uYXRpdmVFbGVtZW50LmdldEF0dHJpYnV0ZSgncHJlbG9hZCcpICE9PSAnbm9uZSc7XG4gICAgICAgIHRoaXMudmdGb3IgPSB0aGlzLnJlZi5uYXRpdmVFbGVtZW50LmdldEF0dHJpYnV0ZSgndmdGb3InKTtcbiAgICAgICAgdGhpcy50YXJnZXQgPSB0aGlzLkFQSS5nZXRNZWRpYUJ5SWQodGhpcy52Z0Zvcik7XG5cbiAgICAgICAgdGhpcy5jb25maWcgPSA8SUhMU0NvbmZpZz57XG4gICAgICAgICAgICBhdXRvU3RhcnRMb2FkOiB0aGlzLnByZWxvYWRcbiAgICAgICAgfTtcblxuICAgICAgICBpZiAodGhpcy5jcm9zc29yaWdpbiA9PT0gJ3VzZS1jcmVkZW50aWFscycpIHtcbiAgICAgICAgICAgIHRoaXMuY29uZmlnLnhoclNldHVwID0gKHhociwgdXJsKSA9PiB7XG4gICAgICAgICAgICAgICAgLy8gU2VuZCBjb29raWVzXG4gICAgICAgICAgICAgICAgeGhyLndpdGhDcmVkZW50aWFscyA9IHRydWU7XG4gICAgICAgICAgICB9O1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5jcmVhdGVQbGF5ZXIoKTtcblxuICAgICAgICBpZiAoIXRoaXMucHJlbG9hZCkge1xuICAgICAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2goXG4gICAgICAgICAgICAgICAgdGhpcy5BUEkuc3Vic2NyaXB0aW9ucy5wbGF5LnN1YnNjcmliZShcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaGxzKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5obHMuc3RhcnRMb2FkKDApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgKVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6U2ltcGxlQ2hhbmdlcykge1xuICAgICAgICBpZiAoY2hhbmdlc1sndmdIbHMnXSAmJiBjaGFuZ2VzWyd2Z0hscyddLmN1cnJlbnRWYWx1ZSkge1xuICAgICAgICAgICAgdGhpcy5jcmVhdGVQbGF5ZXIoKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZGVzdHJveVBsYXllcigpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgY3JlYXRlUGxheWVyKCkge1xuICAgICAgICBpZiAodGhpcy5obHMpIHtcbiAgICAgICAgICAgIHRoaXMuZGVzdHJveVBsYXllcigpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gSXQncyBhIEhMUyBzb3VyY2VcbiAgICAgICAgaWYgKHRoaXMudmdIbHMgJiYgdGhpcy52Z0hscy5pbmRleE9mKCcubTN1OCcpID4gLTEgJiYgSGxzLmlzU3VwcG9ydGVkKCkpIHtcbiAgICAgICAgICAgIGxldCB2aWRlbzpIVE1MVmlkZW9FbGVtZW50ID0gdGhpcy5yZWYubmF0aXZlRWxlbWVudDtcblxuICAgICAgICAgICAgdGhpcy5obHMgPSBuZXcgSGxzKHRoaXMuY29uZmlnKTtcbiAgICAgICAgICAgIHRoaXMuaGxzLm9uKFxuICAgICAgICAgICAgICAgIEhscy5FdmVudHMuTUFOSUZFU1RfUEFSU0VELFxuICAgICAgICAgICAgICAgIChldmVudCwgZGF0YSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zdCB2aWRlb0xpc3QgPSBbXTtcblxuICAgICAgICAgICAgICAgICAgICB2aWRlb0xpc3QucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICBxdWFsaXR5SW5kZXg6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGJpdHJhdGU6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICBtZWRpYVR5cGU6ICd2aWRlbycsXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogJ0FVVE8nXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGRhdGEubGV2ZWxzLmZvckVhY2goKGl0ZW0sIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2aWRlb0xpc3QucHVzaCh7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVhbGl0eUluZGV4OiArK2luZGV4LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHdpZHRoOiBpdGVtLndpZHRoLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlaWdodDogaXRlbS5oZWlnaHQsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYml0cmF0ZTogaXRlbS5iaXRyYXRlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lZGlhVHlwZTogJ3ZpZGVvJyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogaXRlbS5uYW1lXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkdldEJpdHJhdGVzLmVtaXQodmlkZW9MaXN0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgICAgICAgICAgdGhpcy5obHMubG9hZFNvdXJjZSh0aGlzLnZnSGxzKTtcbiAgICAgICAgICAgIHRoaXMuaGxzLmF0dGFjaE1lZGlhKHZpZGVvKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnRhcmdldCAmJiAhIXRoaXMudGFyZ2V0LnBhdXNlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy50YXJnZXQucGF1c2UoKTtcbiAgICAgICAgICAgICAgICB0aGlzLnRhcmdldC5zZWVrVGltZSgwKTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlZi5uYXRpdmVFbGVtZW50LnNyYyA9IHRoaXMudmdIbHM7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzZXRCaXRyYXRlKGJpdHJhdGU6IEJpdHJhdGVPcHRpb24pIHtcbiAgICAgICAgaWYgKHRoaXMuaGxzKSB7XG4gICAgICAgICAgICB0aGlzLmhscy5uZXh0TGV2ZWwgPSBiaXRyYXRlLnF1YWxpdHlJbmRleCAtIDE7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkZXN0cm95UGxheWVyKCkge1xuICAgICAgICBpZiAodGhpcy5obHMpIHtcbiAgICAgICAgICAgIHRoaXMuaGxzLmRlc3Ryb3koKTtcbiAgICAgICAgICAgIHRoaXMuaGxzID0gbnVsbDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaChzID0+IHMudW5zdWJzY3JpYmUoKSk7XG4gICAgICAgIHRoaXMuZGVzdHJveVBsYXllcigpO1xuICAgIH1cbn1cbiJdfQ==