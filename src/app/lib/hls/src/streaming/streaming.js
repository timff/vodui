"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var vg_dash_1 = require("./vg-dash/vg-dash");
var vg_hls_1 = require("./vg-hls/vg-hls");
var VgStreamingModule = /** @class */ (function () {
    function VgStreamingModule() {
    }
    VgStreamingModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [common_1.CommonModule],
                    declarations: [
                        vg_dash_1.VgDASH, vg_hls_1.VgHLS
                    ],
                    exports: [
                        vg_dash_1.VgDASH, vg_hls_1.VgHLS
                    ]
                },] },
    ];
    return VgStreamingModule;
}());
exports.VgStreamingModule = VgStreamingModule;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RyZWFtaW5nLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsic3RyZWFtaW5nLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQThDO0FBQzlDLDBDQUErQztBQUMvQyw2Q0FBMkM7QUFDM0MsMENBQXdDO0FBUXhDO0lBQUE7SUFTZ0MsQ0FBQzs7Z0JBVGhDLGVBQVEsU0FBQztvQkFDTixPQUFPLEVBQUUsQ0FBRSxxQkFBWSxDQUFFO29CQUN6QixZQUFZLEVBQUU7d0JBQ1YsZ0JBQU0sRUFBRSxjQUFLO3FCQUNoQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ0wsZ0JBQU0sRUFBRSxjQUFLO3FCQUNoQjtpQkFDSjs7SUFDK0Isd0JBQUM7Q0FBQSxBQVRqQyxJQVNpQztBQUFwQiw4Q0FBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9ICAgICAgZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgVmdEQVNIIH0gZnJvbSBcIi4vdmctZGFzaC92Zy1kYXNoXCI7XG5pbXBvcnQgeyBWZ0hMUyB9IGZyb20gXCIuL3ZnLWhscy92Zy1obHNcIjtcblxuZXhwb3J0IGludGVyZmFjZSBJRFJNTGljZW5zZVNlcnZlciB7XG4gICAgW2luZGV4OiBzdHJpbmddOiB7XG4gICAgICAgIHNlcnZlclVSTDogc3RyaW5nO1xuICAgIH1cbn1cblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbIENvbW1vbk1vZHVsZSBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBWZ0RBU0gsIFZnSExTXG4gICAgXSxcbiAgICBleHBvcnRzOiBbXG4gICAgICAgIFZnREFTSCwgVmdITFNcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIFZnU3RyZWFtaW5nTW9kdWxlIHt9XG4iXX0=