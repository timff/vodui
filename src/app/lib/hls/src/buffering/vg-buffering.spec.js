"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vg_buffering_1 = require("./vg-buffering");
var vg_api_1 = require("../core/services/vg-api");
describe('Buffering', function () {
    var vgBuffering;
    var ref;
    var api;
    beforeEach(function () {
        ref = {
            nativeElement: {
                getAttribute: function (name) {
                    return name;
                }
            }
        };
        api = new vg_api_1.VgAPI();
        vgBuffering = new vg_buffering_1.VgBuffering(ref, api);
    });
    describe('onPlayerReady', function () {
        it('should subscribe to bufferDetected media events', function () {
            spyOn(api, 'getMediaById').and.returnValue({
                subscriptions: {
                    bufferDetected: { subscribe: jasmine.createSpy('bufferDetected') }
                }
            });
            vgBuffering.onPlayerReady();
            expect(vgBuffering.target.subscriptions.bufferDetected.subscribe).toHaveBeenCalled();
        });
    });
    describe('isBuffering', function () {
        it('should show if buffer is detected', function () {
            vgBuffering.onUpdateBuffer(true);
            expect(vgBuffering.isBuffering).toBe(true);
        });
        it('should hide if buffer is not detected', function () {
            vgBuffering.onUpdateBuffer(false);
            expect(vgBuffering.isBuffering).toBe(false);
        });
    });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmctYnVmZmVyaW5nLnNwZWMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJ2Zy1idWZmZXJpbmcuc3BlYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLCtDQUEyQztBQUMzQyxrREFBOEM7QUFLOUMsUUFBUSxDQUFDLFdBQVcsRUFBRTtJQUNsQixJQUFJLFdBQXVCLENBQUM7SUFDNUIsSUFBSSxHQUFjLENBQUM7SUFDbkIsSUFBSSxHQUFTLENBQUM7SUFFZCxVQUFVLENBQUM7UUFDUCxHQUFHLEdBQUc7WUFDRixhQUFhLEVBQUU7Z0JBQ1gsWUFBWSxFQUFFLFVBQUMsSUFBSTtvQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNoQixDQUFDO2FBQ0o7U0FDSixDQUFDO1FBRUYsR0FBRyxHQUFHLElBQUksY0FBSyxFQUFFLENBQUM7UUFDbEIsV0FBVyxHQUFHLElBQUksMEJBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDNUMsQ0FBQyxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsZUFBZSxFQUFFO1FBQ3RCLEVBQUUsQ0FBQyxpREFBaUQsRUFBRTtZQUNsRCxLQUFLLENBQUMsR0FBRyxFQUFFLGNBQWMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUM7Z0JBQ3ZDLGFBQWEsRUFBRTtvQkFDWCxjQUFjLEVBQUUsRUFBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFO2lCQUNwRTthQUNKLENBQUMsQ0FBQztZQUNILFdBQVcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUM1QixNQUFNLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDekYsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQyxhQUFhLEVBQUU7UUFDcEIsRUFBRSxDQUFDLG1DQUFtQyxFQUFFO1lBQ3BDLFdBQVcsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0MsQ0FBQyxDQUFDLENBQUM7UUFDSCxFQUFFLENBQUMsdUNBQXVDLEVBQUU7WUFDeEMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsQyxNQUFNLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1ZnQnVmZmVyaW5nfSBmcm9tIFwiLi92Zy1idWZmZXJpbmdcIjtcbmltcG9ydCB7VmdBUEl9IGZyb20gXCIuLi9jb3JlL3NlcnZpY2VzL3ZnLWFwaVwiO1xuaW1wb3J0IHtJUGxheWFibGV9IGZyb20gXCIuLi9jb3JlL3ZnLW1lZGlhL2ktcGxheWFibGVcIjtcbmltcG9ydCB7RWxlbWVudFJlZn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFZnU3RhdGVzIH0gZnJvbSBcIi4uL2NvcmUvc3RhdGVzL3ZnLXN0YXRlc1wiO1xuXG5kZXNjcmliZSgnQnVmZmVyaW5nJywgKCkgPT4ge1xuICAgIGxldCB2Z0J1ZmZlcmluZzpWZ0J1ZmZlcmluZztcbiAgICBsZXQgcmVmOkVsZW1lbnRSZWY7XG4gICAgbGV0IGFwaTpWZ0FQSTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgICByZWYgPSB7XG4gICAgICAgICAgICBuYXRpdmVFbGVtZW50OiB7XG4gICAgICAgICAgICAgICAgZ2V0QXR0cmlidXRlOiAobmFtZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmFtZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgYXBpID0gbmV3IFZnQVBJKCk7XG4gICAgICAgIHZnQnVmZmVyaW5nID0gbmV3IFZnQnVmZmVyaW5nKHJlZiwgYXBpKTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdvblBsYXllclJlYWR5JywgKCk9PntcbiAgICAgICAgaXQoJ3Nob3VsZCBzdWJzY3JpYmUgdG8gYnVmZmVyRGV0ZWN0ZWQgbWVkaWEgZXZlbnRzJywgKCk9PntcbiAgICAgICAgICAgIHNweU9uKGFwaSwgJ2dldE1lZGlhQnlJZCcpLmFuZC5yZXR1cm5WYWx1ZSh7XG4gICAgICAgICAgICAgICAgc3Vic2NyaXB0aW9uczoge1xuICAgICAgICAgICAgICAgICAgICBidWZmZXJEZXRlY3RlZDoge3N1YnNjcmliZTogamFzbWluZS5jcmVhdGVTcHkoJ2J1ZmZlckRldGVjdGVkJykgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgdmdCdWZmZXJpbmcub25QbGF5ZXJSZWFkeSgpO1xuICAgICAgICAgICAgZXhwZWN0KHZnQnVmZmVyaW5nLnRhcmdldC5zdWJzY3JpcHRpb25zLmJ1ZmZlckRldGVjdGVkLnN1YnNjcmliZSkudG9IYXZlQmVlbkNhbGxlZCgpO1xuICAgICAgICB9KTtcbiAgICB9KTtcblxuICAgIGRlc2NyaWJlKCdpc0J1ZmZlcmluZycsICgpPT4ge1xuICAgICAgICBpdCgnc2hvdWxkIHNob3cgaWYgYnVmZmVyIGlzIGRldGVjdGVkJywgKCkgPT4ge1xuICAgICAgICAgICAgdmdCdWZmZXJpbmcub25VcGRhdGVCdWZmZXIodHJ1ZSk7XG4gICAgICAgICAgICBleHBlY3QodmdCdWZmZXJpbmcuaXNCdWZmZXJpbmcpLnRvQmUodHJ1ZSk7XG4gICAgICAgIH0pO1xuICAgICAgICBpdCgnc2hvdWxkIGhpZGUgaWYgYnVmZmVyIGlzIG5vdCBkZXRlY3RlZCcsICgpID0+IHtcbiAgICAgICAgICAgIHZnQnVmZmVyaW5nLm9uVXBkYXRlQnVmZmVyKGZhbHNlKTtcbiAgICAgICAgICAgIGV4cGVjdCh2Z0J1ZmZlcmluZy5pc0J1ZmZlcmluZykudG9CZShmYWxzZSk7XG4gICAgICAgIH0pO1xuICAgIH0pO1xufSk7XG4iXX0=