"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vg_quality_selector_1 = require("./vg-quality-selector");
var vg_api_1 = require("../../core/services/vg-api");
describe('Quality Selector control', function () {
    var vgQualitySelector;
    beforeEach(function () {
        var ref = {
            nativeElement: {
                getAttribute: function (name) {
                    return name;
                }
            }
        };
        vgQualitySelector = new vg_quality_selector_1.VgQualitySelector(ref, new vg_api_1.VgAPI());
    });
    describe('onPlayerReady', function () {
    });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmctcXVhbGl0eS1zZWxlY3Rvci5zcGVjLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmctcXVhbGl0eS1zZWxlY3Rvci5zcGVjLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsNkRBQTBEO0FBQzFELHFEQUFtRDtBQUduRCxRQUFRLENBQUMsMEJBQTBCLEVBQUU7SUFDakMsSUFBSSxpQkFBb0MsQ0FBQztJQUV6QyxVQUFVLENBQUM7UUFDUCxJQUFNLEdBQUcsR0FBZTtZQUNwQixhQUFhLEVBQUU7Z0JBQ1gsWUFBWSxFQUFFLFVBQUMsSUFBSTtvQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNoQixDQUFDO2FBQ0o7U0FDSixDQUFDO1FBQ0YsaUJBQWlCLEdBQUcsSUFBSSx1Q0FBaUIsQ0FBQyxHQUFHLEVBQUUsSUFBSSxjQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ2hFLENBQUMsQ0FBQyxDQUFDO0lBRUgsUUFBUSxDQUFDLGVBQWUsRUFBRTtJQUUxQixDQUFDLENBQUMsQ0FBQztBQUNQLENBQUMsQ0FBQyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgVmdRdWFsaXR5U2VsZWN0b3IgfSBmcm9tIFwiLi92Zy1xdWFsaXR5LXNlbGVjdG9yXCI7XG5pbXBvcnQgeyBWZ0FQSSB9IGZyb20gXCIuLi8uLi9jb3JlL3NlcnZpY2VzL3ZnLWFwaVwiO1xuaW1wb3J0IHsgRWxlbWVudFJlZiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbmRlc2NyaWJlKCdRdWFsaXR5IFNlbGVjdG9yIGNvbnRyb2wnLCAoKSA9PiB7XG4gICAgbGV0IHZnUXVhbGl0eVNlbGVjdG9yOiBWZ1F1YWxpdHlTZWxlY3RvcjtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgICBjb25zdCByZWY6IEVsZW1lbnRSZWYgPSB7XG4gICAgICAgICAgICBuYXRpdmVFbGVtZW50OiB7XG4gICAgICAgICAgICAgICAgZ2V0QXR0cmlidXRlOiAobmFtZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmFtZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHZnUXVhbGl0eVNlbGVjdG9yID0gbmV3IFZnUXVhbGl0eVNlbGVjdG9yKHJlZiwgbmV3IFZnQVBJKCkpO1xuICAgIH0pO1xuXG4gICAgZGVzY3JpYmUoJ29uUGxheWVyUmVhZHknLCAoKSA9PiB7XG5cbiAgICB9KTtcbn0pO1xuIl19