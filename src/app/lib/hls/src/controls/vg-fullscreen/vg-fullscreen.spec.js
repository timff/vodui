"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var vg_fullscreen_1 = require("./vg-fullscreen");
var vg_api_1 = require("../../core/services/vg-api");
var vg_fullscreen_api_1 = require("../../core/services/vg-fullscreen-api");
describe('Videogular Player', function () {
    var fullscreen;
    var ref;
    var api;
    var fsAPI;
    beforeEach(function () {
        ref = {
            nativeElement: {
                getAttribute: function (name) {
                    return name;
                }
            }
        };
        api = new vg_api_1.VgAPI();
        fsAPI = new vg_fullscreen_api_1.VgFullscreenAPI();
        fullscreen = new vg_fullscreen_1.VgFullscreen(ref, api, fsAPI);
    });
    it('Should get media by id on init', function () {
        spyOn(api, 'getMediaById').and.callFake(function () { });
        fullscreen.vgFor = 'test';
        fullscreen.onPlayerReady();
        expect(api.getMediaById).toHaveBeenCalledWith('test');
    });
    describe('onClick', function () {
        beforeEach(function () {
            spyOn(fsAPI, 'toggleFullscreen');
        });
        it('Should call toggleFullscreen with null param if target is API', function () {
            fullscreen.target = api;
            fullscreen.onClick();
            expect(fsAPI.toggleFullscreen).toHaveBeenCalledWith(null);
        });
        it('Should call toggleFullscreen with target param if target', function () {
            fullscreen.target = 'test';
            fullscreen.onClick();
            expect(fsAPI.toggleFullscreen).toHaveBeenCalledWith('test');
        });
    });
});

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmctZnVsbHNjcmVlbi5zcGVjLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsidmctZnVsbHNjcmVlbi5zcGVjLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsaURBQTZDO0FBQzdDLHFEQUFpRDtBQUVqRCwyRUFBc0U7QUFFdEUsUUFBUSxDQUFDLG1CQUFtQixFQUFFO0lBQzFCLElBQUksVUFBd0IsQ0FBQztJQUM3QixJQUFJLEdBQWMsQ0FBQztJQUNuQixJQUFJLEdBQVMsQ0FBQztJQUNkLElBQUksS0FBcUIsQ0FBQztJQUUxQixVQUFVLENBQUM7UUFDUCxHQUFHLEdBQUc7WUFDRixhQUFhLEVBQUU7Z0JBQ1gsWUFBWSxFQUFFLFVBQUMsSUFBSTtvQkFDZixNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUNoQixDQUFDO2FBQ0o7U0FDSixDQUFDO1FBRUYsR0FBRyxHQUFHLElBQUksY0FBSyxFQUFFLENBQUM7UUFDbEIsS0FBSyxHQUFHLElBQUksbUNBQWUsRUFBRSxDQUFDO1FBQzlCLFVBQVUsR0FBRyxJQUFJLDRCQUFZLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNuRCxDQUFDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxnQ0FBZ0MsRUFBRTtRQUNqQyxLQUFLLENBQUMsR0FBRyxFQUFFLGNBQWMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsY0FBUSxDQUFDLENBQUMsQ0FBQztRQUVuRCxVQUFVLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztRQUMxQixVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFM0IsTUFBTSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxRCxDQUFDLENBQUMsQ0FBQztJQUVILFFBQVEsQ0FBQyxTQUFTLEVBQUU7UUFDaEIsVUFBVSxDQUFDO1lBQ1AsS0FBSyxDQUFDLEtBQUssRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3JDLENBQUMsQ0FBQyxDQUFDO1FBRUgsRUFBRSxDQUFDLCtEQUErRCxFQUFFO1lBQ2hFLFVBQVUsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1lBRXhCLFVBQVUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztZQUVyQixNQUFNLENBQUMsS0FBSyxDQUFDLGdCQUFnQixDQUFDLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDOUQsQ0FBQyxDQUFDLENBQUM7UUFFSCxFQUFFLENBQUMsMERBQTBELEVBQUU7WUFDM0QsVUFBVSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7WUFFM0IsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBRXJCLE1BQU0sQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1ZnRnVsbHNjcmVlbn0gZnJvbSBcIi4vdmctZnVsbHNjcmVlblwiO1xuaW1wb3J0IHtWZ0FQSX0gZnJvbSBcIi4uLy4uL2NvcmUvc2VydmljZXMvdmctYXBpXCI7XG5pbXBvcnQge0VsZW1lbnRSZWZ9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQge1ZnRnVsbHNjcmVlbkFQSX0gZnJvbSBcIi4uLy4uL2NvcmUvc2VydmljZXMvdmctZnVsbHNjcmVlbi1hcGlcIjtcblxuZGVzY3JpYmUoJ1ZpZGVvZ3VsYXIgUGxheWVyJywgKCkgPT4ge1xuICAgIGxldCBmdWxsc2NyZWVuOiBWZ0Z1bGxzY3JlZW47XG4gICAgbGV0IHJlZjpFbGVtZW50UmVmO1xuICAgIGxldCBhcGk6VmdBUEk7XG4gICAgbGV0IGZzQVBJOlZnRnVsbHNjcmVlbkFQSTtcblxuICAgIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgICAgICByZWYgPSB7XG4gICAgICAgICAgICBuYXRpdmVFbGVtZW50OiB7XG4gICAgICAgICAgICAgICAgZ2V0QXR0cmlidXRlOiAobmFtZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmFtZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG5cbiAgICAgICAgYXBpID0gbmV3IFZnQVBJKCk7XG4gICAgICAgIGZzQVBJID0gbmV3IFZnRnVsbHNjcmVlbkFQSSgpO1xuICAgICAgICBmdWxsc2NyZWVuID0gbmV3IFZnRnVsbHNjcmVlbihyZWYsIGFwaSwgZnNBUEkpO1xuICAgIH0pO1xuXG4gICAgaXQoJ1Nob3VsZCBnZXQgbWVkaWEgYnkgaWQgb24gaW5pdCcsICgpID0+IHtcbiAgICAgICAgc3B5T24oYXBpLCAnZ2V0TWVkaWFCeUlkJykuYW5kLmNhbGxGYWtlKCgpID0+IHsgfSk7XG5cbiAgICAgICAgZnVsbHNjcmVlbi52Z0ZvciA9ICd0ZXN0JztcbiAgICAgICAgZnVsbHNjcmVlbi5vblBsYXllclJlYWR5KCk7XG5cbiAgICAgICAgZXhwZWN0KGFwaS5nZXRNZWRpYUJ5SWQpLnRvSGF2ZUJlZW5DYWxsZWRXaXRoKCd0ZXN0Jyk7XG4gICAgfSk7XG5cbiAgICBkZXNjcmliZSgnb25DbGljaycsICgpID0+IHtcbiAgICAgICAgYmVmb3JlRWFjaCgoKSA9PiB7XG4gICAgICAgICAgICBzcHlPbihmc0FQSSwgJ3RvZ2dsZUZ1bGxzY3JlZW4nKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaXQoJ1Nob3VsZCBjYWxsIHRvZ2dsZUZ1bGxzY3JlZW4gd2l0aCBudWxsIHBhcmFtIGlmIHRhcmdldCBpcyBBUEknLCAoKSA9PiB7XG4gICAgICAgICAgICBmdWxsc2NyZWVuLnRhcmdldCA9IGFwaTtcblxuICAgICAgICAgICAgZnVsbHNjcmVlbi5vbkNsaWNrKCk7XG5cbiAgICAgICAgICAgIGV4cGVjdChmc0FQSS50b2dnbGVGdWxsc2NyZWVuKS50b0hhdmVCZWVuQ2FsbGVkV2l0aChudWxsKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgaXQoJ1Nob3VsZCBjYWxsIHRvZ2dsZUZ1bGxzY3JlZW4gd2l0aCB0YXJnZXQgcGFyYW0gaWYgdGFyZ2V0JywgKCkgPT4ge1xuICAgICAgICAgICAgZnVsbHNjcmVlbi50YXJnZXQgPSAndGVzdCc7XG5cbiAgICAgICAgICAgIGZ1bGxzY3JlZW4ub25DbGljaygpO1xuXG4gICAgICAgICAgICBleHBlY3QoZnNBUEkudG9nZ2xlRnVsbHNjcmVlbikudG9IYXZlQmVlbkNhbGxlZFdpdGgoJ3Rlc3QnKTtcbiAgICAgICAgfSk7XG4gICAgfSk7XG59KTtcbiJdfQ==