"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var vg_overlay_play_1 = require("./vg-overlay-play");
var VgOverlayPlayModule = /** @class */ (function () {
    function VgOverlayPlayModule() {
    }
    VgOverlayPlayModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [common_1.CommonModule],
                    declarations: [
                        vg_overlay_play_1.VgOverlayPlay
                    ],
                    exports: [
                        vg_overlay_play_1.VgOverlayPlay
                    ]
                },] },
    ];
    return VgOverlayPlayModule;
}());
exports.VgOverlayPlayModule = VgOverlayPlayModule;

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3ZlcmxheS1wbGF5LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsib3ZlcmxheS1wbGF5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQThDO0FBQzlDLDBDQUErQztBQUUvQyxxREFBZ0Q7QUFFaEQ7SUFBQTtJQVNrQyxDQUFDOztnQkFUbEMsZUFBUSxTQUFDO29CQUNOLE9BQU8sRUFBRSxDQUFFLHFCQUFZLENBQUU7b0JBQ3pCLFlBQVksRUFBRTt3QkFDViwrQkFBYTtxQkFDaEI7b0JBQ0QsT0FBTyxFQUFFO3dCQUNMLCtCQUFhO3FCQUNoQjtpQkFDSjs7SUFDaUMsMEJBQUM7Q0FBQSxBQVRuQyxJQVNtQztBQUF0QixrREFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9ICAgICAgZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5pbXBvcnQge1ZnT3ZlcmxheVBsYXl9IGZyb20gJy4vdmctb3ZlcmxheS1wbGF5JztcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbIENvbW1vbk1vZHVsZSBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBWZ092ZXJsYXlQbGF5XG4gICAgXSxcbiAgICBleHBvcnRzOiBbXG4gICAgICAgIFZnT3ZlcmxheVBsYXlcbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIFZnT3ZlcmxheVBsYXlNb2R1bGUge31cbiJdfQ==