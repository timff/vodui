import {Component, OnInit} from '@angular/core';
import {Sort} from '@angular/material';

export interface Dessert {
    calories: number;
    carbs: number;
    fat: number;
    name: string;
    protein: number;
}


@Component({
    selector: 'app-table',
    templateUrl: './table.component.html',
    styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

    desserts: Dessert[] = [
        {name: 'Frozen yogurt', calories: 159, fat: 6, carbs: 24, protein: 4},
        {name: 'Ice cream sandwich', calories: 237, fat: 9, carbs: 37, protein: 4},
        {name: 'Eclair', calories: 262, fat: 16, carbs: 24, protein: 6},
        {name: 'Cupcake', calories: 305, fat: 4, carbs: 67, protein: 4},
        {name: 'Cupcake', calories: 105, fat: 4, carbs: 43, protein: 6},
        {name: 'Gingerbread', calories: 356, fat: 16, carbs: 49, protein: 4},
    ];


    desserts2: any[] = [
        {name: 'tony cai', age: 24},
        {name: 'jingwei tang', age: 44},
    ];


    constructor() {
        this.sortedData = this.desserts.slice();
    }

    ngOnInit() {
        this.arrayHandle();

    };


    arrayHandle() {
        let concat_result = this.desserts.concat(this.desserts2);
        let tmp_1: any = [{width: 333, height: 999}, {width: 111, height: 222}];
        concat_result.push(tmp_1);
        // concat and slice  dont change the original array
        // delete 1st:  concat_result.shift();  delete last:  concat_result.pop();
        //  revere sorting:  concat_result.reverse()   get the range array: concat_result.slice(4, 8)
      //  console.log(concat_result);
        //  let slice_result:any = concat_result.slice(4, 8);   console.warn (slice_result );
        //sorting  let sortArry = concat_result.sort();   console.log(  sortArry );

        //find  return the first element
        //  var find_result = concat_result.find(x => x.name == 'Cupcake').name;
        //  console.warn(find_result);

        var filter_result = concat_result.filter(x => x.name == 'Cupcake');
        var filter_result2 = filter_result.filter(x => x.carbs == 43);
        //console.log(filter_result);
        //console.log(filter_result2);
    }




    sortedData: Dessert[];

    sortData(sort: Sort) {
        const data = this.desserts.slice();
        if (!sort.active || sort.direction === '') {
            this.sortedData = data;
            return;
        }

        this.sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'name':
                    return compare(a.name, b.name, isAsc);
                case 'calories':
                    return compare(a.calories, b.calories, isAsc);
                case 'fat':
                    return compare(a.fat, b.fat, isAsc);
                case 'carbs':
                    return compare(a.carbs, b.carbs, isAsc);
                case 'protein':
                    return compare(a.protein, b.protein, isAsc);
                default:
                    return 0;
            }
        });
    }


}


function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}