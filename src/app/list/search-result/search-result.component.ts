import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
// import { VgAPI } from 'videogular2/core';
import {Platform} from '@angular/cdk/platform';

@Component({
    selector: 'app-search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.scss']
})
export class SearchResultComponent implements OnInit {

    constructor(private breakpointObserver: BreakpointObserver,
                public platform: Platform) {
    }

    @Output()
    public pushVideo: EventEmitter<any> = new EventEmitter();

    @Output()
    public popFilter: EventEmitter<any> = new EventEmitter();


    @ViewChild('viewVideoCard')
    public viewVideoCard;


    handleMutiFilter(filterObj) {

        this.viewVideoCard.mutiFilterObjPrehandle(filterObj);

    }



    sortVideoList(event) {

        this.viewVideoCard.sortingByDate(event);

    }


    handleFilterInfo(obj: any) {

        this.popFilter.emit(obj);

    }


    handleVideoInfo(message: any) {
        //    console.log('search-result--> handleVideoInfo():' + message);
        this.pushVideo.emit(message);
    }


    ngOnInit() {

        const isSmallScreen = this.breakpointObserver.isMatched('(max-width: 599px)');

        this.breakpointObserver.observe([
            Breakpoints.HandsetPortrait

        ]).subscribe(result => {
            // console.log(result);
            if (result.matches) {
                //      console.warn("Detect MOBILE view port")
            } else {
                //  console.warn("Detect MOBILE view port")
            }
        });
    }


    launchSearch(str: string) {
        this.viewVideoCard.handleSearch(str);
    }


    refreshLatest() {
        this.viewVideoCard.latestVideos();
    }


}
